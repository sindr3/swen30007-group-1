// Utility methods
if (!String.prototype.trim) {
    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g,'');
    }
}
 
 
// Triggered using a POST request, this function sends a push notification to any user who hasn't been active in the last 24 hours.
Parse.Cloud.define("notifyInactiveUsers", function(request, response) {
    var today = new Date();
    var yesterday = new Date();
    yesterday.setDate ((today.getDate() - 1));
    yesterday.setHours (yesterday.getHours() + 10);
    console.log ("notifyInactiveUsers triggered for users inactive since " + yesterday.toString());
 
    var userQuery = new Parse.Query (Parse.User);
    userQuery.lessThan ('lastActive', yesterday);
 
    userQuery.find ({
        success: function (results) {
            var pushQuery = new Parse.Query (Parse.Installation);
            pushQuery.containedIn ('user', results);
 
            Parse.Push.send ({
                where: pushQuery,
                data: {
                    alert: "You haven't been active in the last 24 hours."
                }
            }, {
                success: function() {
                    console.log ("notifyInactiveUsers completed: " + results.length + " users found inactive.");
                    response.success ("");
                },
                error: function(error) {
                    throw "notifyInactiveUsers had an error";
                }
            })
        },
 
        error: function (error) {
            throw "Got an error " + error.code + " : " + error.message;
        }
    })
});
 
// Sends a notification to a person when someone requests to be their friend.
Parse.Cloud.afterSave ("friendRequest", function (request) {
    // Find devices owned by user being added
    var pushQuery = new Parse.Query (Parse.Installation);
    pushQuery.equalTo ('user', request.object.get('toUser'));
 
    // Get the user who sent the request
    var userQuery = new Parse.Query (Parse.User);
    userQuery.get (request.object.get('fromUser').id,
    {
        success: function(result) {
            // Name of person doing the friend requesting
            var fromName = result.get ('firstName') + " " + result.get ('lastName');
 
            Parse.Push.send ({
                where: pushQuery,
                data: {
                    alert: fromName + " added you as a friend!",
                    badge: "Increment",
                    type: "friendRequest"
                }
            }, {
                success: function() {
                    // Push succeeded
                },
                error: function(error) {
                    throw "Got an error: " + error.code + " : " + error.message;
                }
            });
        },
        error: function(error) {
            throw "afterSave FriendRequest got an error: " + error.code + " : " + error.message;
        }
    });
});
 
// Notify person when someone accepts their friend request
Parse.Cloud.afterSave ("friendLink", function (request) {
    // Find devices owned by user being added
    var pushQuery = new Parse.Query (Parse.Installation);
    pushQuery.equalTo ('user', request.object.get('personTwo'));
 
    // Get the user who sent the request
    var userQuery = new Parse.Query (Parse.User);
    userQuery.get (request.object.get('personOne').id,
    {
        success: function(result) {
            // Name of person doing the friend requesting
            var fromName = result.get ('firstName') + " " + result.get ('lastName');
 
            Parse.Push.send ({
                where: pushQuery,
                data: {
                    alert: fromName + " accepted your friend request.",
                    badge: "Increment",
                    type: "friendLink"
                }
            }, {
                success: function() {
                    // Push succeeded
                },
                error: function(error) {
                    throw "Got an error: " + error.code + " : " + error.message;
                }
            });
        },
        error: function(error) {
            throw "afterSave FriendRequest got an error: " + error.code + " : " + error.message;
        }
    });
});
 
// Notifies the recipient of a message that someone has sent a message to them
Parse.Cloud.afterSave ("Message", function (request) {
    // Find devices owned by user being added
    var pushQuery = new Parse.Query (Parse.Installation);
    pushQuery.equalTo ('user', request.object.get('toUser'));
 
    // Get the user who sent the request
    var userQuery = new Parse.Query (Parse.User);
    userQuery.get (request.object.get('fromUser').id,
    {
        success: function(result) {
            // Name of person doing the friend requesting
            var fromName = result.get ('firstName') + " " + result.get ('lastName');
 
            Parse.Push.send ({
                where: pushQuery,
                data: {
                    alert: fromName + " sent you a message.",
                    badge: "Increment",
                    type: "Message",
                    userID: request.object.get('fromUser').id
                }
            }, {
                success: function() {
                    // Push succeeded
                },
                error: function(error) {
                    throw "Got an error: " + error.code + " : " + error.message;
                }
            });
        },
        error: function(error) {
            throw "afterSave Message got an error: " + error.code + " : " + error.message;
        }
    });
});
 
// Notifies the carers of an AP that the AP has sent and emergency help request
Parse.Cloud.afterSave ("EmergencyRequest", function (request) {
    // Get the user who sent the request
    var userQuery = new Parse.Query (Parse.User);
    userQuery.get (request.object.get('EmergencyUser').id,
    {
        success: function(assistedPerson) {
            // Find devices owned by carers of the AP
            var linkQuery = new Parse.Query ("carerLink");
            linkQuery.equalTo ('assistedPerson', request.object.get('EmergencyUser'));
            linkQuery.include ('carer');
 
            linkQuery.find({
                success: function(links) {
                    // Find who to send to
                    var _ = require ("underscore");
                    var carers = _.map (links, function (link) {
                        var carer = new Parse.Object ('User');
                        carer.id = link.get ('carer').id;
                        return carer;
                    });
                    var pushQuery = new Parse.Query (Parse.Installation);
                    pushQuery.containedIn ('user', carers);
 
                    // Name of person doing the friend requesting
                    var fromName = assistedPerson.get ('firstName') + " " + assistedPerson.get ('lastName');
                     
                    Parse.Push.send ({
                        where: pushQuery,
                        data: {
                            alert: fromName + " requires emergency assistance!",
                            badge: "Increment",
                            objID: request.object.id,
                            type: "EmergencyRequest",
                            userID: request.object.get('EmergencyUser').id
                        }
                    }, {
                        success: function() {
                            // Push succeeded
                        },
                        error: function(error) {
                            throw "Error sending push: " + error.code + " : " + error.message;
                        }
                    });
                },
 
                error: function(error) {
                    throw "Error finding carers: " + error.code + " : " + error.message;
                }
            });
        },
        error: function(error) {
            throw "Error getting AP sending emergency request: " + error.code + " : " + error.message;
        }
    });
});
 
// Notifies the carers of an AP that the AP has cancelled an emergency request
Parse.Cloud.afterDelete ("EmergencyRequest", function (request) {
    // Get the user who sent the request
    var userQuery = new Parse.Query (Parse.User);
    userQuery.get (request.object.get('EmergencyUser').id,
    {
        success: function(assistedPerson) {
            // Find devices owned by carers of the AP
            var linkQuery = new Parse.Query ("carerLink");
            linkQuery.equalTo ('assistedPerson', request.object.get('EmergencyUser'));
            linkQuery.include ('carer');
 
            linkQuery.find({
                success: function(links) {
                    // Find who to send to
                    var _ = require ("underscore");
                    var carers = _.map (links, function (link) {
                        var carer = new Parse.Object ('User');
                        carer.id = link.get ('carer').id;
                        return carer;
                    });
                    var pushQuery = new Parse.Query (Parse.Installation);
                    pushQuery.containedIn ('user', carers);
 
                    // Name of person doing the friend requesting
                    var fromName = assistedPerson.get ('firstName') + " " + assistedPerson.get ('lastName');
                     
                    Parse.Push.send ({
                        where: pushQuery,
                        data: {
                            alert: fromName + " no longers requires emergency assistance.",
                            badge: "Increment",
                            type: "EmergencyCancel",
                            userID: request.object.get('EmergencyUser').id
                        }
                    }, {
                        success: function() {
                            // Push succeeded
                        },
                        error: function(error) {
                            throw "Error sending push: " + error.code + " : " + error.message;
                        }
                    });
                },
 
                error: function(error) {
                    throw "Error finding carers: " + error.code + " : " + error.message;
                }
            });
        },
        error: function(error) {
            throw "Error getting AP sending emergency request: " + error.code + " : " + error.message;
        }
    });
});
 
// Notifies a carer that their AP has requested help for something
Parse.Cloud.afterSave ("carerRequest", function (request) {
    // Get the user who sent the request
    var userQuery = new Parse.Query (Parse.User);
    userQuery.get (request.object.get('assistedPerson').id,
    {
        success: function(ap) {
            // Name of person doing the friend requesting, and the request itself
            var fromName = ap.get ('firstName') + " " + ap.get ('lastName');
            var requestType = request.object.get('type').trim().toLowerCase();
 
            // Find devices owned by carer
            var pushQuery = new Parse.Query (Parse.Installation);
            pushQuery.equalTo ('user', request.object.get('carer'));
 
            Parse.Push.send ({
                where: pushQuery,
                data: {
                    alert: fromName + " requests assistance with " + requestType + ".",
                    badge: "Increment",
                    type: "carerRequest",
                    userID: request.object.get('assistedPerson').id
                }
            }, {
                success: function() {
                    // Push succeeded
                },
                error: function(error) {
                    throw "Got an error: " + error.code + " : " + error.message;
                }
            });
        },
        error: function(error) {
            throw "afterSave carerRequest got an error: " + error.code + " : " + error.message;
        }
    });
});

// Only keep one social activity
Parse.Cloud.beforeSave ("socialActivity", function (request, response) {
    // Get the user who sent the request
    var activityQuery = new Parse.Query ("socialActivity");
    activityQuery.equalTo ('user', request.object.get('user'));

    activityQuery.find({
        success: function(objects) {
            for (var i = 0; i < objects.length; i++) {
                objects[i].destroy({});
            }

            response.success ();
        },

        error: function(error) {
            response.error ("Could not clear old activity: " + error.message);
        }
    });
});