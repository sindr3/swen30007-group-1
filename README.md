#SWEN30007 - Group 1
##Project Oldbook

###Using the Application

iOS project for Software Systems Project, targetting **iOS 7** running on **iPads, and 4" iPhone and iPod Touch devices**.
Requires a minimum of a camera and internet connection to function.

See the software requirements document at this address for further details

docs/Software Requirements.pdf

******
###Build Instructions

Building the project should be straightforward if you have **xCode 5**, which is available free on the Mac App Store.

Once you have xCode installed, simply **add this repository and clone out the project**, which should configure xCode correctly. You can then proceed to run the project on the iOS simulator, or if you have a developer account with provisioning profiles, install it on your own device locally.

Note: you may need to remove and re-add Parse.framework from the list of linked libraries and frameworks, to make the application compile.

******
###Using the Application

Three users have been registered for use. Their passwords are the same as the usernames:

- tim: A carer user
- iain: A social user
- aidan: An assisted user

Tim is the registered carer of Aidan, and Iain and Aidan are friends.

******
###Running Tests

In order to run the tests, you must open OldBook.xcworkspace, not OldBook.xcodeproj. Some of the tests test our queries, and thus require an internet connection.