//
//  EmergencyViewController.m
//  OldBook
//
//  Created by Oscar Morrison on 25/09/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//
#define METERS_PER_MILE 1609.344
#import "EmergencyViewController.h"
#import <MapKit/MapKit.h>

@interface EmergencyViewController () <UIAlertViewDelegate, CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (weak, nonatomic) CLLocation *currentLocation;
@property (weak, nonatomic) IBOutlet UIButton *cancelEmergencyButton;
@end

@implementation EmergencyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Create the location manager if this object does not
    // already have one.
    if (nil == self.locationManager)
        self.locationManager = [[CLLocationManager alloc] init];
    
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    // Set a movement threshold for new events.
    self.locationManager.distanceFilter = 500;
    
    [self.locationManager startUpdatingLocation];
}

- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    self.currentLocation = [locations lastObject];
    
    CLLocationCoordinate2D zoomLocation = self.currentLocation.coordinate;
    //Update map
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
    [_mapView setRegion:viewRegion animated:YES];
    
    return;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)emergencyCancelled:(id)sender{
    UIAlertView *areYouSureAlert = [[UIAlertView alloc] initWithTitle:@"Cancel Emergency"
                                                              message:@"Are You Sure You Want To Cancel?"
                                                             delegate:self
                                                    cancelButtonTitle:@"NO"
                                                    otherButtonTitles:@"YES",nil
                                    
                                    ];
    [areYouSureAlert show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex == [alertView cancelButtonIndex]){
        return;
    } else {
        // Send cancel notification
        [self performSegueWithIdentifier:@"CANCELEMERGENCY" sender:self];
    }
}


@end
