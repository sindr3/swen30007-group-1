//
// OBAssistedUser.h
// OldBook
//
// Created by Mat on 17/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "OBAssistedUser.h"
#import "OBUser.h"
#import "OBCarer.h"
#import "OBEmergencyRequest.h"
#import "OBCarerRequest.h"

@interface OBAssistedUser : OBUser

//The users carers as a mutable array (so we can add to them)
@property (strong, nonatomic) OBCarer *carer;
//The carers requests available
@property (strong,nonatomic) NSMutableArray *carerRequestsAvailable;
//Requests sent to a carer;
@property (strong,nonatomic) NSMutableArray *carerRequestsSent;
//The emergency request (if active)
@property (strong, nonatomic) OBEmergencyRequest *emergencyRequest;

//Function to set up the assisted user
+ (OBAssistedUser *)getCurrentUser;
- (id) initWithMockObjectsMailbox:(OBParseQueryManager *) qm andUser:(PFUser *)user andDetails:(OBContact *)details andFriendlist:(NSMutableArray *)fl andMessages:(NSMutableDictionary *)inbox andCarer:(OBCarer *)carer andCarerRequests:(NSMutableArray *)requests;

//The functions for getting the carers
- (void) retrieveCarerWithCallback:(void(^)(OBCarer *carer, NSError *error))callback;
//Functions for sedning carer requests
- (void) sendCarerRequest:(NSInteger)requestIndex to:(OBCarer *)carer;
//Check request functions available for a carer
- (void) getCarerRequestsFor:(OBCarer *)carer withCallback:(void (^)(NSArray *carerRequests, NSError *error))completionBlock;
- (void) getRequestsSentWithCallback:(void(^)(NSArray *requestSent, NSError *error))completionBlock;

//Functions for emergency requests
- (void)sendEmergencyRequest;
- (void)cancelEmergencyRequest;
- (void)etaForCarerInBackgroundWithBlock:(void (^)(MKETAResponse *response, NSError *error))block;
- (void)locationForCarerInBackgroundWithBlock:(void (^)(CLLocationCoordinate2D *location, NSError *error))block;

@end
