//
// OBMessage.m
// OldBook
//
// Created by Mat on 14/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBMessage.h"

@implementation OBMessage
+ (OBMessage *) createAndSendTextMessageWith:(NSString *)textContents toUser:(PFUser *)toUser fromUser:(PFUser *)fromUser withProgress:(void (^)(int percentdone))showProgress
{
  //Create the new message
  OBMessage *newMessage = [OBMessage new];
  
  //Set local copies
  newMessage.toUser = toUser;
  newMessage.fromUser = fromUser;
  newMessage.messageType = @"text";
  newMessage.read=NO;
  
  NSData *textData = [textContents dataUsingEncoding:NSUTF8StringEncoding];
  PFFile *textFile = [PFFile fileWithData:textData];
  PFObject *serverSave = [PFObject objectWithClassName:@"Message"];
  [serverSave setObject:toUser forKey:@"toUser"];
  [serverSave setObject:fromUser forKey:@"fromUser"];
  [serverSave setObject:@"text" forKey:@"type"];
  [serverSave setObject:[NSNumber numberWithBool:NO] forKey:@"hasBeenRead"];
  
  //Now that the file is saved set it to the message and save.
  [serverSave setObject:textFile forKey:@"messageContents"];
  [serverSave saveInBackground];

  newMessage.objectID = [serverSave objectId];
  newMessage.serverObject = serverSave;
  newMessage.messageContents = textData;
  newMessage.sent = [NSDate date];
  newMessage.outgoing = true;

  
  return newMessage;
}

+ (OBMessage *) createAndSendTextMessageWithSYNCH:(NSString *)textContents toUser:(PFUser *)toUser fromUser:(PFUser *)fromUser withProgress:(void (^)(int percentdone))showProgress
{
  OBMessage *newMessage = [OBMessage new];
  PFObject *serverSave = [PFObject objectWithClassName:@"Message"];
  
  //Set local copies
  newMessage.toUser = toUser;
  newMessage.fromUser = fromUser;
  newMessage.messageType = @"text";
  [serverSave setObject:toUser forKey:@"toUser"];
  [serverSave setObject:fromUser forKey:@"fromUser"];
  [serverSave setObject:@"text" forKey:@"type"];
  [serverSave setObject:[NSNumber numberWithBool:NO] forKey:@"hasBeenRead"];
  newMessage.read=NO;
  
  NSData *textData = [textContents dataUsingEncoding:NSUTF8StringEncoding];
  PFFile *textFile = [PFFile fileWithData:textData];

  [textFile save];
  newMessage.messageContents = textData;
  [serverSave setObject:textFile forKey:@"messageContents"];
  [serverSave save];
  newMessage.objectID = [serverSave objectId];
  newMessage.serverObject = serverSave;
  newMessage.sent = [NSDate date];
  newMessage.outgoing = true;

  
  return newMessage;
}


+ (OBMessage *) createAndSendPictureMessageWith:(UIImage *)image andToUser:(PFUser *)toUser andFromUser:(PFUser *)fromUser withProgress:(void (^)(int percentdone))showProgress
{
  OBMessage *newMessage = [OBMessage new];
  PFObject *serverSave = [PFObject objectWithClassName:@"Message"];
  
  //Set local copies
  newMessage.toUser = toUser;
  newMessage.fromUser = fromUser;
  [serverSave setObject:toUser forKey:@"toUser"];
  [serverSave setObject:fromUser forKey:@"fromUser"];
  [serverSave setObject:@"pic" forKey:@"type"];
  [serverSave setObject:[NSNumber numberWithBool:NO] forKey:@"hasBeenRead"];
  newMessage.read=NO;

  NSData *imageData = UIImageJPEGRepresentation(image, 1.0f);
  PFFile *contents = [PFFile fileWithData:imageData];
  [serverSave setObject:contents forKey:@"messageContents"];
  [serverSave saveInBackground];
  
  newMessage.messageContents = imageData;
  newMessage.objectID = [serverSave objectId];
  newMessage.serverObject = serverSave;
  newMessage.sent = [NSDate date];
  newMessage.outgoing = true;

  return newMessage;
}

+ (OBMessage *) createAndSendPictureMessageWithSYNCH:(UIImage *)image andToUser:(PFUser *)toUser andFromUser:(PFUser *)fromUser withProgress:(void (^)(int percentdone))showProgress
{
  OBMessage *newMessage = [OBMessage new];
  PFObject *serverSave = [PFObject objectWithClassName:@"Message"];
  
  //Set local copies
  newMessage.toUser = toUser;
  newMessage.fromUser = fromUser;
  [serverSave setObject:toUser forKey:@"toUser"];
  [serverSave setObject:fromUser forKey:@"fromUser"];
  [serverSave setObject:@"pic" forKey:@"type"];
  [serverSave setObject:[NSNumber numberWithBool:YES] forKey:@"hasBeenRead"];
  newMessage.read=NO;

  NSData *imageData = UIImageJPEGRepresentation(image, 1.0f);
  PFFile *contents = [PFFile fileWithData:imageData];
  [contents save];
  newMessage.messageContents = imageData;
  [serverSave setObject:contents forKey:@"messageContents"];
  [serverSave save];
  newMessage.objectID = [serverSave objectId];
  newMessage.serverObject = serverSave;

  newMessage.sent = [NSDate date];
  newMessage.outgoing = true;
  
  return newMessage;
}


+ (OBMessage *) saveMessageFromServer:(PFObject *)serverMessage
{
  [serverMessage fetchIfNeeded];
  OBMessage *message = [OBMessage new];
  message.toUser = [serverMessage objectForKey:@"toUser"];
  message.fromUser = [serverMessage objectForKey:@"fromUser"];
  message.messageType = [serverMessage objectForKey:@"type"];
  message.read = [[serverMessage objectForKey:@"hasBeenRead"] boolValue];

  //Downlaod message contents
  PFFile *messageContents = [serverMessage objectForKey:@"messageContents"];
  message.messageContents = [messageContents getData];  
  message.objectID = [serverMessage objectId];
  message.sent = serverMessage.createdAt;
  
  //Check who sent it.
  if([message.toUser.objectId isEqualToString:[PFUser currentUser].objectId]){
    message.outgoing=NO;
  } else {
    message.outgoing=YES;
  }

  return message;
}

+ (OBMessage *) saveMessageFromServerSYNCH:(PFObject *)serverMessage
{
  [serverMessage fetch];
  OBMessage *message = [OBMessage new];
  message.toUser = [serverMessage objectForKey:@"toUser"];
  [message.toUser fetch];
  message.fromUser = [serverMessage objectForKey:@"fromUser"];
  [message.fromUser fetch];
  message.messageType = [serverMessage objectForKey:@"type"];
  message.read = [[serverMessage objectForKey:@"hasBeenRead"] boolValue];
  message.serverObject = serverMessage;
  //Downlaod message contents
  PFFile *messageContents = [serverMessage objectForKey:@"messageContents"];
  message.messageContents = [messageContents getData];
  message.objectID = [serverMessage objectId];
  message.sent = serverMessage.createdAt;

  //Check who sent it.
  if([message.toUser.objectId isEqualToString:[PFUser currentUser].objectId]){
    message.outgoing=NO;
      } else {
    message.outgoing=YES;
  }

  return message;
}


- (void) deleteMessageFromServer
{
  [self.serverObject deleteInBackground];
}

- (void) deleteMessageFromServerSYNCH
{
  [self.serverObject delete];
}


@end
