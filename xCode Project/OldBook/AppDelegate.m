//
// AppDelegate.m
// OldBook
//
// Created by Mat on 7/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <Parse/Parse.h>
#import "AppDelegate.h"
#import "TestFlight.h"
#import "OBNotificationHandler.h"

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

static AppDelegate *delegate = nil;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	// Override handling of uncaught exceptions
	NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
	
	//Set up parse connection
	[Parse setApplicationId:@"mzhUVsf2I5Dxb9T4ptFtpIFWOXS20f5qWxJJPOnR"
				  clientKey:@"vec4TZnMwQyuZzpof6cd08Sv63tzL2SQlTOKfxpc"];
	
	// Register for push notifications
	[application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge|
	 UIRemoteNotificationTypeAlert|
	 UIRemoteNotificationTypeSound];
	
	// TestFlight registration
	[TestFlight setDeviceIdentifier:[[PFInstallation currentInstallation] deviceToken]];
	[TestFlight takeOff:@"57b88d89-f919-449f-9729-58414d7d4450"];
	
	//Create the window
	self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
	
	//Load the device storyboard
	UIViewController *viewController = nil;
	
	if([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
		// iPhone
		self.storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:nil];
	} else {
		// iPad
		self.storyboard = [UIStoryboard storyboardWithName:@"Storyboard-iPad" bundle:nil];
	}
	
	if([[PFUser currentUser] isAuthenticated]){
		NSString *userType = [[PFUser currentUser] objectForKey:@"type"];
		//Log in to the correct area
		if([userType isEqualToString:@"SOCIAL"]){
			viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ROOTSOCIALVIEW"];
		}else if([userType isEqualToString:@"ASSISTED"]){
			viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ROOTAPVIEW"];
		}else if([userType isEqualToString:@"CARER"]){
			viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ROOTCARERVIEW"];
		}
	} else {
		viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LOGINVIEW"];
	}
    
	delegate = self;
	
	self.window.rootViewController = viewController;
	[self.window makeKeyAndVisible];
	
	return YES;
}

void uncaughtExceptionHandler(NSException *exception) {
    NSLog(@"CRASH: %@", exception);
    NSLog(@"Stack Trace: %@", [exception callStackSymbols]);
    // Internal error reporting
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	PFInstallation *currentInstallation = [PFInstallation currentInstallation];
	if (currentInstallation.badge != 0) {
		currentInstallation.badge = 0;
		[currentInstallation saveEventually];
	}
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Saves changes in the application's managed object context before the application terminates.
	[self saveContext];
}

- (void)saveContext
{
	NSError *error = nil;
	NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
	if (managedObjectContext != nil) {
		if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
			// Replace this implementation with code to handle the error appropriately.
			// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
			abort();
		}
	}
}

#pragma mark - Parse push notification node

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
	// Store the deviceToken in the current installation and save it to Parse.
	PFInstallation *currentInstallation = [PFInstallation currentInstallation];
	[currentInstallation setDeviceTokenFromData:deviceToken];
	[currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
	[OBNotificationHandler handleNotificationWithinApp:userInfo inApp:self.window.rootViewController andStoryboard:self.storyboard];
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
	if (_managedObjectContext != nil) {
		return _managedObjectContext;
	}
	
	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if (coordinator != nil) {
		_managedObjectContext = [[NSManagedObjectContext alloc] init];
		[_managedObjectContext setPersistentStoreCoordinator:coordinator];
	}
	return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
	if (_managedObjectModel != nil) {
		return _managedObjectModel;
	}
	NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"OldBook" withExtension:@"momd"];
	_managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
	return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
	if (_persistentStoreCoordinator != nil) {
		return _persistentStoreCoordinator;
	}
	
	NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"OldBook.sqlite"];
	
	NSError *error = nil;
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
	if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
		/*
		 Replace this implementation with code to handle the error appropriately.
		 
		 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
		 
		 Typical reasons for an error here include:
		 * The persistent store is not accessible;
		 * The schema for the persistent store is incompatible with current managed object model.
		 Check the error message to determine what the actual problem was.
		 
		 
		 If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
		 
		 If you encounter schema incompatibility errors during development, you can reduce their frequency by:
		 * Simply deleting the existing store:
		 [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
		 
		 * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
		 @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
		 
		 Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
		 
		 */
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
	}
	
	return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
	return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

// Methods for view management
+ (AppDelegate *) getCurrentAppDelegate
{
    return delegate;
}

- (void) presentStoryBoardView:(NSString *) title
{
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:title];
    if(viewController){
        [self.window.rootViewController presentViewController:viewController animated:YES completion:nil];
    }
}

- (void) changeRootViewController:(NSString *) title
{
    UIViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:title];
    if(viewController){
        self.window.rootViewController = viewController;
        [self.window makeKeyAndVisible];
    }
}


@end
