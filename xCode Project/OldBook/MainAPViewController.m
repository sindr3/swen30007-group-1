//
// MainAPViewController.m
// OldBook
//
// Created by Mat on 8/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "MainAPViewController.h"
#import "RequestsViewController.h"
#import "OBCameraViewControllerHelper.h"

@interface MainAPViewController () <UIAlertViewDelegate>
@property (strong, nonatomic) UIAlertView *logOutAlert;
@property (strong, nonatomic) UIAlertView *areYouSureAlert;
@property (assign, nonatomic) CGPoint highWelcome;
@property (assign, nonatomic) CGPoint lowName;
@property (weak, nonatomic) IBOutlet UILabel *userFirstName;
@property (weak, nonatomic) IBOutlet UILabel *welcomeBackLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (weak, nonatomic) IBOutlet UIButton *friendRequestButton;
@property (strong, nonatomic) OBCameraViewControllerHelper *cameraHelper;

@end

@implementation MainAPViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.currentUser = [OBAssistedUser getCurrentUser];
	self.highWelcome = CGPointMake(200, 138);
	self.lowName = CGPointMake(200, 172);
	[self.friendRequestButton setHidden:YES];
	self.cameraHelper = [[OBCameraViewControllerHelper alloc] initWithImageDest:self.userImage inView:self withNotifier:@"assistedProfilePicutre"];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(updateProfilePic)
												 name:@"assistedProfilePicutre"
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(newFriendRequests)
												 name:@"newFriendRequest"
											   object:nil];
	
	
}

- (void) updateProfilePic {
	[self.currentUser setProfilePicture:self.cameraHelper.profilePicture showProgress:YES];
}

- (void) viewDidLayoutSubviews {
	if([self.userFirstName.text length]!=0){
		[self.welcomeBackLabel setCenter:self.highWelcome];
		[self.userFirstName setCenter:self.lowName];
	}
}

- (void) viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	//Set the userName logo
	if([self.userFirstName.text length]==0){
		OBContact *contact = self.currentUser.userDetails;
		
		[self.userFirstName setAlpha:0];
		[self.userFirstName setText:[contact firstName]];
		[UIView animateWithDuration:0.5 animations:^{
			[self.welcomeBackLabel setCenter:self.highWelcome];
			[self.userFirstName setCenter:self.lowName];
			[self.userFirstName setAlpha:1];
		}];
		
		[UIView animateWithDuration:1 animations:^{
			[self.userImage setAlpha:0];
		}];
		[self.userImage setImage:[contact profilePicture]];
		[UIView animateWithDuration:0.2 animations:^{
			[self.userImage setAlpha:1];
		}];
		
	}
	// check for friend request button
	[[self friendRequestButton] setHidden:![self.currentUser userHasFriendRequests]];
}

- (void) newFriendRequests {
	[[self friendRequestButton] setHidden:NO];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

-(IBAction)logout:(id)sender{
	self.logOutAlert = [[UIAlertView alloc] initWithTitle:@"Log Out?"
												  message:@"Are you sure you want to log out?"
												 delegate:self
										cancelButtonTitle:@"NO"
										otherButtonTitles:@"YES",
						nil];
	[self.logOutAlert show];
	return;
}

- (IBAction)emergencyInitiated:(id)sender{
	self.areYouSureAlert = [[UIAlertView alloc] initWithTitle:@"Emergency"
													  message:@"Do You Need Emergency Assistance?"
													 delegate:self
											cancelButtonTitle:@"NO"
											otherButtonTitles:@"YES",
							nil];
	[self.areYouSureAlert show];
}

- (IBAction)changeProfilePic:(id)sender {
	[self.cameraHelper selectPhoto:sender];
}

//This needs to be sent out of this class into the OBAssistedUser
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if(buttonIndex != [alertView cancelButtonIndex]){
		if(alertView==self.logOutAlert){
			[OBAssistedUser logOut];
			[self performSegueWithIdentifier:@"BACKTOLOGIN" sender:self];
			
		} else {
			[self.currentUser sendEmergencyRequest];
			[self performSegueWithIdentifier:@"createEmergency" sender:self];
		}
		return;
	} else {
		[self resignFirstResponder];
	}
	
}

- (BOOL) checkForFriendRequests {
	return [[self currentUser] userHasFriendRequests];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if([segue.identifier isEqualToString:@"CARERREQUESTS"]){
		RequestsViewController *view = (RequestsViewController *) [segue destinationViewController];
		[view setCurrentUser:[self currentUser]];
		[view setCurrentCarer:[[self currentUser] carer]];
	}
}




@end
