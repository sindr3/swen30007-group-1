//
//  ApDefaultViewController.m
//  OldBook
//
//  Created by Sindre on 16/10/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import "ApDefaultViewController.h"
#import "OBCameraViewControllerHelper.h"
#import "AppDelegate.h"
#import "OBAssistedUser.h"

@interface ApDefaultViewController () <UIImagePickerControllerDelegate>
@property(nonatomic, strong) OBCameraViewControllerHelper* cameraHelper;

@property(strong, nonatomic) UIAlertView* logOutAlert;

@property(strong, nonatomic) OBAssistedUser* user;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UIImageView* userImage;

@property(strong, nonatomic) OBCarer* carer;
@property (strong, nonatomic) IBOutlet UILabel *carerNameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *carerImage;

@end

@implementation ApDefaultViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.user = [OBAssistedUser getCurrentUser];
    self.carer = [self.user carer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateProfilePic)
                                                 name:@"socialProfilePic"
                                               object:nil];
    
    self.cameraHelper = [[OBCameraViewControllerHelper alloc] initWithImageDest:self.userImage inView:self.splitViewController withNotifier:@"socialProfilePic"];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [self fillCarerDetails];
    [self fillUserDetails];
    
    [self.user retrieveCarerWithCallback:^(OBCarer *carer, NSError *error) {
        if (!error) {
            self.carer = carer;
            [self fillCarerDetails];
        }
    }];

}

- (void)fillUserDetails
{
    self.userNameLabel.text = [self.user.userDetails getFullName];
    self.userImage.image = [self.user.userDetails profilePicture];
}

- (void)fillCarerDetails
{
    self.carerNameLabel.text = [self.carer getFullName];
    if ([self.carer profilePicture] == nil) {
        self.carerImage.image = [UIImage imageNamed:@"defaultImage.png"];
    } else {
        self.carerImage.image = [self.carer profilePicture];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Profile Picture Methods
- (void)updateProfilePic
{
    [[OBUser getCurrentUser] setProfilePicture:[self.cameraHelper profilePicture] showProgress:YES];
}

- (void)changeProfilePic:(id)sender
{
    [self.cameraHelper selectPhoto:sender];
}

- (IBAction)changePhotoButtonTapped:(id)sender
{
    [self changeProfilePic:sender];
}

- (IBAction)userImageTapped:(id)sender
{
    [self changeProfilePic:sender];
}

#pragma mark Log Out Methods
- (IBAction)logOutButtonPressed:(id)sender
{
    self.logOutAlert = [[UIAlertView alloc] initWithTitle:@"Log Out?"
                                                  message:@"Are you sure you want to log out?"
                                                 delegate:self
                                        cancelButtonTitle:@"NO"
                                        otherButtonTitles:@"YES", nil];
    [self.logOutAlert show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == self.logOutAlert) {
        if (buttonIndex != [alertView cancelButtonIndex]) {
            [OBUser logOut];
            AppDelegate* app = [AppDelegate getCurrentAppDelegate];
            [app presentStoryBoardView:@"LOGINVIEW"];
        } else {
            [self resignFirstResponder];
        }
    } else {
        [self resignFirstResponder];
    }
}
@end
