//
// CarerClientOverViewViewController.m
// OldBook
//
// Created by Mat on 9/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "CarerClientOverViewViewController.h"
#import "OBContactDetailViewViewController.h"
#import "NextOfKinViewController.h"
#import "MedicalDetailsViewController.h"
#import "CheckInViewController.h"
#import "ClientRequestViewController.h"

@interface CarerClientOverViewViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *clientImage;
@property (weak, nonatomic) IBOutlet UILabel *clientName;
@property (weak, nonatomic) IBOutlet UILabel *clientLastSeen;

@end

@implementation CarerClientOverViewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  [self.clientImage setImage:[self.client profilePicture]];
  [self.clientName setText:[self.client getFullName]];
  [self.clientLastSeen setText:[self.client getLastSeenString]];
}

- (void) viewWillAppear:(BOOL)animated {
  //Calculate last known date
  NSDate *date = self.client.lastUpdated;
  [date description];
}

- (IBAction)nextOFKinPressed:(id)sender
{
  [self performSegueWithIdentifier:@"toNextOfKin" sender:self];
}

- (IBAction)requestsPressed:(id)sender
{
  [self performSegueWithIdentifier:@"toClientRequests" sender:self];
}

- (IBAction)medicalNotesPressed:(id)sender
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
													message:@"I'm afraid I can't do that dave."
												   delegate:nil
										  cancelButtonTitle:@"Ok..."
										  otherButtonTitles:nil];
	[alert show];																																																
	
//  [self performSegueWithIdentifier:@"toMedicalDetails" sender:self];
}

- (IBAction)checkInPressed:(id)sender
{
  [self performSegueWithIdentifier:@"toCheckIn" sender:self];
}

- (IBAction)messagesPressed:(id)sender
{
  [self performSegueWithIdentifier:@"toMessageDetails" sender:self];
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  if([segue.identifier isEqualToString:@"toMessageDetails"]){
    OBContactDetailViewViewController *destination = (OBContactDetailViewViewController *) [segue destinationViewController];
    destination.friendUser = self.client;
	  destination.currentUser = [OBCarerUser getCurrentCarerUser];
  } else if ([segue.identifier isEqualToString:@"toNextOfKin"]) {
    NextOfKinViewController *destination = (NextOfKinViewController *) [segue destinationViewController];
    destination.client = self.client;
  } else if ([segue.identifier isEqualToString:@"toClientRequests"]) {
    ClientRequestViewController *destination = (ClientRequestViewController *) [segue destinationViewController];
    destination.client = self.client;
  } else if ([segue.identifier isEqualToString:@"toMedicalDetails"]) {
    MedicalDetailsViewController *destination = (MedicalDetailsViewController *) [segue destinationViewController];
    destination.client = self.client;
  } else if ([segue.identifier isEqualToString:@"toCheckIn"]) {
    CheckInViewController *destination = (CheckInViewController *) [segue destinationViewController];
    destination.client = self.client;
  }
}

@end
