//
//  CarerClientRequestsViewController
//  OldBook
//
//  Created by Oscar Morrison on 14/10/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBAssistedContact.h"
#import "OBCarerUser.h"

@interface CarerClientRequestsViewController : UIViewController
@property (strong, nonatomic) OBAssistedContact *client;
@property (strong, nonatomic) NSArray *requests;
@property (strong, nonatomic) OBCarerUser *user;

@end
