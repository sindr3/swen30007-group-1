//
// LogInViewController.m
// OldBook
//
// Created by Mat on 7/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBUser.h"
#import "LogInViewController.h"
#import "SVProgressHUD.h"
#import <QuartzCore/QuartzCore.h>

#define LOGINKEY "LOGIN"
#define KEYBOARDMOVE 200


@interface LogInViewController () <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (assign, nonatomic) CGPoint originalMiddle;
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@end

@implementation LogInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    //
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  [self.loginButton setEnabled:NO];
  [self.passwordField setReturnKeyType:UIReturnKeyDefault];
  self.originalMiddle = self.view.center;
  
  //Gesture recognizer for dismissing keyboards.
  UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTappedBackground:)];
  tapGesture.cancelsTouchesInView = NO;
  [self.scrollView addGestureRecognizer:tapGesture];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  
}

- (void) didTappedBackground:(UITapGestureRecognizer *)sender{
  if(self.passwordField.isFirstResponder){
    [self.passwordField resignFirstResponder];
  }else if(self.usernameField.isFirstResponder){
    [self.usernameField resignFirstResponder];
  }
  [self hideKeyboardAnimation];
}


- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
}

#pragma mark - UITextFieldDelegate methods
- (BOOL) textFieldShouldReturn:(UITextField *)textField {
  if(textField==self.usernameField){
    [self.passwordField becomeFirstResponder];
    [self checkLoginValid:nil];
  } else {
    [textField resignFirstResponder];
    [self hideKeyboardAnimation];
    [self checkLoginValid:nil];
    if([self.loginButton isEnabled]){
      [self logInPressed:textField];
    }
  }
  return YES;
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
  CGPoint newCenter = CGPointMake(0, KEYBOARDMOVE/4);
  [self.scrollView setContentOffset:newCenter animated:YES];
  return YES;
}

- (IBAction)logInPressed: (id) sender{
  [SVProgressHUD showWithStatus:@"Logging In"];
  self.password = [self passwordField].text;
  self.username = [self usernameField].text;
  if([SVProgressHUD isVisible]){
    if([self.username length]!=0 && [self.password length]!=0){
      
      [OBUser logInWithUserNameInBackground:self.username andPassword:self.password onCompletion:^(NSError *error) {
        [SVProgressHUD dismiss];
        [self.passwordField setText:nil];
        if(!error){
          [self logInToView];
        } else {
          [SVProgressHUD dismiss];
          NSString *errorMessage = [[error userInfo] objectForKey:@"error"];
          [[[UIAlertView alloc] initWithTitle:@"Log In Error"
                        message:errorMessage
                        delegate:nil
                   cancelButtonTitle:@"OK"
                   otherButtonTitles:nil] show];
          
        }
        
      }];
    }
    
    if ([self.username length]==0){
      [self.usernameField setPlaceholder:@"Enter username and try again"];
    }
    
    if ([self.password length]==0){
      [self.passwordField setPlaceholder:@"Enter password and try again"];
    }
  }
  
  
  return;
}

- (void) logInToView {
  OBUser *currentUser = [OBUser getCurrentUser];
  NSString *userType = [currentUser.currentParseUser objectForKey:@"type"];
  //Log in to the correct area
  if([userType isEqualToString:@"SOCIAL"]){
    [self performSegueWithIdentifier:@"SocialUserLogin" sender: self];
  }else if([userType isEqualToString:@"ASSISTED"]){
    [self performSegueWithIdentifier:@"AssistedPersonLogin" sender: self];
  }else if([userType isEqualToString:@"CARER"]){
    [self performSegueWithIdentifier:@"CarerLogin" sender: self];
  }
  return;
}


- (IBAction)checkLoginValid:(id)sender {
  if([[self usernameField].text length]!=0){
    [self.passwordField setReturnKeyType:UIReturnKeyGo];
  }
  if([[self passwordField].text length]!=0 && [[self usernameField].text length]!=0 ) {
    [self.loginButton setEnabled:YES];
  } else {
    [self.loginButton setEnabled:NO];
    [self.passwordField setReturnKeyType:UIReturnKeyDefault];
    
  }
}

- (void) hideKeyboardAnimation {
  [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

@end
