//
// SignUpViewController.m
// OldBook
//
// Created by Mat on 8/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "SignUpViewController.h"
#import "FinishSigningUpViewController.h"

//Key points to scroll to
#define BIRTHDAYPOINT 60
#define FIRSTNAMEPOINT 60
#define LASTNAMEPOINT 60

@interface SignUpViewController () <UIAlertViewDelegate, UITextFieldDelegate>

@property (strong, nonatomic) UIAlertView *cancelAlert;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *doneButton;

@property (weak, nonatomic) IBOutlet UITextField *firstNameField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameField;
@property (weak, nonatomic) IBOutlet UITextField *birthdayField;

//Date Picker Stuff
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (assign, nonatomic) CGPoint birthdayUp;
@property (assign, nonatomic) CGPoint birthdayDown;

//Scrolling stuff
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

- (IBAction)continueButtonPressed:(id)sender;
- (IBAction)viewShouldDismiss:(id)sender;
- (IBAction)cancelButtonPressed:(id)sender;
- (IBAction)checkAllFieldsValid:(id)sender;

@end


@implementation SignUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  [self checkAllFieldsValid:self];
  
  // Set up the date picker
  NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:[NSDate date]];
  [components setYear:components.year - 50];
  [components setMonth:1];
  [components setDay:1];
  NSDate *defaultDate = [[NSCalendar currentCalendar] dateFromComponents:components];
  
  self.datePicker = [[UIDatePicker alloc] init];
  [self.datePicker setDatePickerMode:UIDatePickerModeDate];
  [self.datePicker addTarget:self action:@selector(updateDate:) forControlEvents:UIControlEventValueChanged];
  [self.datePicker setMaximumDate:[NSDate date]];
  [self.datePicker setDate:defaultDate];
  [self.birthdayField setInputView:self.datePicker];
  [self.doneButton setEnabled:NO];
  
  //Gesture recognizer for dismissing keyboards.
  UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTappedBackground:)];
  tapGesture.cancelsTouchesInView = NO;
  [self.scrollView addGestureRecognizer:tapGesture];
}

- (void) didTappedBackground:(UITapGestureRecognizer *)sender{
  [self.view endEditing:YES];
  [self hideKeyboardAnimation];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void) hideKeyboardAnimation {
  [self.scrollView setContentOffset: CGPointMake(0, 0) animated:YES];
  return;
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
  if(textField == self.firstNameField){
    [self.scrollView setContentOffset: CGPointMake(0, FIRSTNAMEPOINT) animated:YES];
  }else if(textField ==self.lastNameField){
    [self.scrollView setContentOffset: CGPointMake(0, LASTNAMEPOINT) animated:YES];
  }else if(textField == self.birthdayField){
    [self.scrollView setContentOffset: CGPointMake(0, BIRTHDAYPOINT) animated:YES];
  }
  return YES;
}

- (IBAction)viewShouldDismiss:(id)sender {
  [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelButtonPressed:(id)sender {
  self.cancelAlert = [[UIAlertView alloc]
            initWithTitle:@"Cancel Sign Up"
            message:@"Are you sure you want to cancel?"
            delegate:self
            cancelButtonTitle:@"No"
            otherButtonTitles:@"Yes", nil];
  [[self cancelAlert] show];
  
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
  if([segue.identifier isEqualToString:@"continueSignUp"]){
    FinishSigningUpViewController *controller = (FinishSigningUpViewController *)segue.destinationViewController;
    
    controller.firstName = [_firstNameField text];
    controller.lastName = [_lastNameField text];
    controller.birthday = [NSDate date];
  }
}

#pragma mark - UIAlertViewDelegate methods
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if (alertView == [self cancelAlert]) {
    if (buttonIndex == [alertView cancelButtonIndex]) {
      [self resignFirstResponder];
    } else {
      [self viewShouldDismiss:alertView];
    }
  }
}

#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  if (textField==[self firstNameField]) {
    [[self lastNameField] becomeFirstResponder];
  } else if (textField == [self lastNameField]) {
    [[self birthdayField] becomeFirstResponder];
  } else if (textField == [self birthdayField]) {
    [[self birthdayField] resignFirstResponder];
    [self hideKeyboardAnimation];
    [self.doneButton setEnabled:NO];
    
    if ([self allFieldsCompleted]) {
      [self proceedWithSignup:textField];
    }
  }
  
  return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  if (textField == [self birthdayField]) {
    return NO;
  } else {
    return YES;
  }
}

- (void)proceedWithSignup:(id)sender {
  [self performSegueWithIdentifier:@"continueSignUp" sender:self];
}

- (IBAction)checkAllFieldsValid:(id)sender {
  if ([self allFieldsCompleted]) {
    [self.doneButton setEnabled:YES];
  } else {
    [self.doneButton setEnabled:NO];
  }
}

- (BOOL)allFieldsCompleted {
  BOOL done = [self.firstNameField.text length] != 0 & [self.lastNameField.text length] != 0 & [self.birthdayField.text length] != 0;
  return done;
}

- (IBAction)continueButtonPressed:(id)sender {
  [self.view endEditing:YES];
  [self proceedWithSignup:sender];
}

- (void)dateSelected:(id)sender {
  [self.birthdayField resignFirstResponder];
  [self hideKeyboardAnimation];
}

- (void)updateDate:(id)sender {
  NSString *date = [NSDateFormatter localizedStringFromDate:[_datePicker date]
                          dateStyle:NSDateFormatterLongStyle
                          timeStyle:NSDateFormatterNoStyle];
  [self.birthdayField setText:date];
  [self checkAllFieldsValid:sender];
}

@end
