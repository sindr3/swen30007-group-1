//
// OBSearchQueryManager.m
// OldBook
//
// Created by Mat on 15/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBSearchQueryManager.h"

@implementation OBSearchQueryManager

static OBSearchQueryManager *manager=nil;

// Returns the singleton method for the searchQueryManager
+(id) getSearchQueryManager
{
  if(manager==nil){
    manager = [OBSearchQueryManager new];
  }
  return manager;
}

#pragma mark - String Validity checking methods
// Method for checking validity of an email address, from:
// http://stackoverflow.com/questions/3139619/check-that-an-email-address-is-valid-on-ios
+ (BOOL) NSStringIsValidEmail:(NSString *)checkString
{
  BOOL stricterFilter = YES;
  NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
  NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
  NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
  NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
  return [emailTest evaluateWithObject:checkString];
}

//Method for checking
+ (BOOL) NSStringIsValidPhoneNumber:(NSString *)checkString
{
  NSString *filterString = @"[0-9+ ]*";
  NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", filterString];
  return [phoneTest evaluateWithObject:checkString];
}

//Method for checking if a name is valid
+ (BOOL) NSStringIsValidName:(NSString *)checkString
{
  NSString *filterString = @"[A-Za-z]*";
  NSPredicate *nameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", filterString];
  return [nameTest evaluateWithObject:checkString];
  
}

-(void) makeSearchQuery:(NSString *) searchString whereKey:(NSString *)searchKey withCompletion:( void (^)(NSMutableArray *searchResults,NSError *error))completionBlock
{
  NSString *search = [searchString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
  PFQuery *notCarer = [PFUser query];
  [notCarer whereKey:@"type" notEqualTo:@"CARER"];
  PFQuery *allUsers = [PFUser query];
  [allUsers whereKey:searchKey equalTo:search];
  [allUsers whereKey:@"type" matchesKey:@"type" inQuery:notCarer];
  OBUser *currentUser = [OBUser getCurrentUser];
  [allUsers whereKey:@"objectId" notEqualTo:currentUser.currentParseUser.objectId];
  self.results = allUsers;
  
  // Get the obejcts from parse
  [self.results findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
    
    //Return users as OBContacts;
    NSMutableArray *searchResults = [NSMutableArray arrayWithArray:[OBParseQueryManager contactsFromUsers:objects]];
    completionBlock(searchResults, error);
  }];
}

-(void) makeSearchQueryWithoutKeyFor:(NSString *)searchString withCompletion:( void(^)(NSMutableArray *searchResults, NSError *error))completionBlock
{
  if([OBSearchQueryManager NSStringIsValidEmail:searchString]){
    [self makeSearchQuery:searchString whereKey:@"email" withCompletion:^(NSMutableArray *searchResults, NSError *error) {
      completionBlock(searchResults,error);
    }];
  } else if ([OBSearchQueryManager NSStringIsValidName:searchString]){
    [self makeSearchQuery:searchString whereKey:@"firstName" withCompletion:^(NSMutableArray *searchResults, NSError *error) {
      completionBlock(searchResults,error);
    }];
  } else if ([OBSearchQueryManager NSStringIsValidPhoneNumber:searchString]){
    [self makeSearchQuery:searchString whereKey:@"phoneNumber" withCompletion:^(NSMutableArray *searchResults, NSError *error) {
      completionBlock(searchResults,error);
    }];
  }
}


@end
