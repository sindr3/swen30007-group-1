//
//  CarerRequestsViewController.m
//  OldBook
//
//  Created by Sindre on 7/10/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//
#import "CarerRequestsViewController.h"

@interface CarerRequestsViewController () <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIAlertView* logOutAlert;

//OBUser

//activity array
@property (strong, nonatomic) NSArray *recentActivity;
//table view
@property (weak, nonatomic) IBOutlet UITableView *activityTableView;
@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;


@end

@implementation CarerRequestsViewController



- (void) viewDidLoad {
	[super viewDidLoad];
	self.carer = [OBCarerUser getCurrentCarerUser];
    [self.activityTableView setHidden:YES];
    [self.activityIndicator startAnimating];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshActivity)
                                                 name:@"loadedRecentActivity"
                                               object:nil];
    
    
}



- (void) refreshActivity {
    
     self.recentActivity = self.carer.clientUpdates;
    [self.activityTableView setHidden:NO];
    [self.activityTableView reloadData];
    [self.activityIndicator hidesWhenStopped];
    [self.activityIndicator stopAnimating];
    [self.loadingView setHidden:YES];
}


- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.recentActivity count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ACTIVITYCELL" forIndexPath:indexPath];
    OBRecentAcitvity *activity = [self.recentActivity objectAtIndex:indexPath.row];
    
    //Now fill in the details
    [cell.imageView setImage:activity.contact.profilePicture];
    [cell.textLabel setText:activity.contact.firstName];
    [cell.detailTextLabel setText:activity.type];
    [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18]];
    [cell.detailTextLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12]];
    
    return cell;
}





#pragma mark - log out functions

// Log out functions
- (IBAction)logOutButtonPressed:(id)sender
{
    self.logOutAlert = [[UIAlertView alloc] initWithTitle:@"Log Out?"
                                                  message:@"Are you sure you want to log out?"
                                                 delegate:self
                                        cancelButtonTitle:@"NO"
                                        otherButtonTitles:@"YES",
                        nil];
    [self.logOutAlert show];
    return;
}

//This needs to be sent out of this class into the OBAssistedUser
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex != [alertView cancelButtonIndex]){
        if(alertView==self.logOutAlert){
            [OBUser logOut];
            AppDelegate* app = [AppDelegate getCurrentAppDelegate];
            [app presentStoryBoardView:@"LOGINVIEW"];
        }
        return;
    } else {
        [self resignFirstResponder];
    }
}

@end
