//
// PeopleYouKnowViewController.m
// OldBook
//
// Created by Mat on 9/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "PeopleYouKnowViewController.h"

@interface PeopleYouKnowViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@end

@implementation PeopleYouKnowViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (int) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
  return 0;
}

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  return nil;
}

-(IBAction) addSelectedFriend:(id)sender {
  return;
}

@end
