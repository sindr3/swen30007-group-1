//
//  EmergencyViewController.h
//  OldBook
//
//  Created by Oscar Morrison on 25/09/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmergencyViewController : UIViewController
- (IBAction)emergencyCancelled:(id)sender;

@end
