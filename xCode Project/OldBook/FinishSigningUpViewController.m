//
// FinishSigningUpViewController.m
// OldBook
//
// Created by Mat on 9/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <Parse/Parse.h>
#import "FinishSigningUpViewController.h"
#import "SVProgressHUD.h"
#import "UIImage-Extensions.h"
#import "OBUser.h"
#import "OBSearchQueryManager.h"
#import "OBCameraViewControllerHelper.h"

@interface FinishSigningUpViewController () <UIImagePickerControllerDelegate, UITextFieldDelegate, UIActionSheetDelegate>

////Contversion macros
//CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;};
//CGFloat RadiansToDegrees(CGFloat radians) {return radians * 180/M_PI;};
//

//For scrolling up and down with keyboard
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (assign, nonatomic) CGPoint scrollDefault;
@property (assign, nonatomic) CGPoint scrollTo;

//For accessing UIElements
@property (weak, nonatomic) IBOutlet UIImageView *imageToPick;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *usernameField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *createButton;

//Our camer view controller helper
@property (strong, nonatomic) OBCameraViewControllerHelper *cameraView;

- (IBAction)createButtonPressed:(id)sender;

@end

@implementation FinishSigningUpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Extra init stuff
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  
  self.scrollTo = CGPointMake(0, 60);
  self.scrollDefault = CGPointMake(0, 0);
  self.cameraView = [[OBCameraViewControllerHelper alloc] initWithImageDest:self.imageToPick inView:self withNotifier:@"finishSignUpPic"];
  //Gesture recognizer for dismissing keyboards.
  UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTappedBackground:)];
  tapGesture.cancelsTouchesInView = NO;
  [self.scrollView addGestureRecognizer:tapGesture];
}

- (void) didTappedBackground:(UITapGestureRecognizer *)sender{
  [self.view endEditing:YES];
  [self scrollViewDown];
}


- (void)viewDidAppear:(BOOL)animated {
  [self checkAllFieldsValid:self];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)createButtonPressed:(id)sender {
  [self.view endEditing:YES];
  [self signUpUser];
}

- (void)scrollViewDown {
  [self.scrollView setContentOffset:self.scrollDefault animated:YES];
}

- (void)scrollViewUp {
  [self.scrollView setContentOffset:self.scrollTo animated:YES];
}

#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
  [self scrollViewUp];
  return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  if (textField==[self emailField]) {
    [[self usernameField] becomeFirstResponder];
    
  } else if (textField == [self usernameField]) {
    [[self passwordField] becomeFirstResponder];
    
  } else if (textField == [self passwordField]) {
    [[self passwordField] resignFirstResponder];
    [self scrollViewDown];
    [self signUpUser];
  }
  
  return YES;
}

- (IBAction)checkAllFieldsValid:(id)sender {
  if ([self allFieldsCompleted]) {
    [_createButton setEnabled:YES];
  } else {
    [_createButton setEnabled:NO];
  }
}

- (BOOL)allFieldsCompleted {
  BOOL done = [self.usernameField.text length] != 0 & [self.emailField.text length]
  != 0 & [self.passwordField.text length] != 0;
  return done;
}


- (IBAction)signUpUser {
  if (![self allFieldsCompleted]) {
    return;
  }
  if (![OBSearchQueryManager NSStringIsValidEmail:[_emailField text]]) {
    UIAlertView *invalidEmailAlert = [[UIAlertView alloc] initWithTitle:@"Invalid Email"
                                  message:@"The email address you entered is invalid."
                                  delegate:nil
                             cancelButtonTitle:@"OK"
                             otherButtonTitles:nil];
    [invalidEmailAlert show];
    return;
  }
  [SVProgressHUD showWithStatus:@"Creating Account"];
  //Create the OBUser
  OBUser *newUser = [OBUser createTempUserFor:self.usernameField.text andPassword:self.passwordField.text andEmail:self.emailField.text andBirthday:self.birthday andFirstName:self.firstName andLastName:self.lastName];
  
  [newUser createNewUserWithProfilePicture:[self.cameraView profilePicture] andCompletion:^(BOOL succeeded, NSError *error) {
    if(!error){
      [SVProgressHUD dismiss];
      //Everything went smoothly, so now we can go into the next screen
      [self performSegueWithIdentifier:@"finishedSignUp" sender:self];
    } else {
      [SVProgressHUD dismiss];
      NSString *errorMessage = [[error userInfo] objectForKey:@"error"];
      UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error"
                                 message:errorMessage
                                delegate:nil
                            cancelButtonTitle:@"OK"
                            otherButtonTitles:nil];
      [errorAlert show];
      
    }
  }];
}


- (IBAction)selectPhoto:(id)sender {
  [self.cameraView selectPhoto:sender];
}

@end
