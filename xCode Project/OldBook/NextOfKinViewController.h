//
// NextOfKinViewController.h
// OldBook
//
// Created by Mat on 9/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBAssistedContact.h"

@interface NextOfKinViewController : UIViewController

@property (strong,nonatomic) OBAssistedContact *client;

@end
