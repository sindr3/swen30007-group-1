//
// OBContactDetailViewViewController.h
// OldBook
//
// Created by Mat on 7/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSMessagesViewController.h"
#import <Parse/Parse.h>
#import "OBMessageInbox.h"
#import "SVProgressHUD.h"
#import "OBContact.h"
#import "OBUser.h"
#import "AGPhotoBrowserView.h"

@interface OBContactDetailViewViewController : JSMessagesViewController <JSMessagesViewDelegate, JSMessagesViewDataSource, AGPhotoBrowserDelegate, AGPhotoBrowserDataSource>

@property (strong, nonatomic) OBMessageInbox *inbox;
@property (nonatomic) OBContact *friendUser;
@property (nonatomic) OBUser *currentUser;
@property (strong, nonatomic) UIImage *selectedImage;
@property (assign, nonatomic) BOOL finishedLoading;

-(void) newMessage;
-(void) loadMessageInboxForUser;

@end
