//
// ResetPasswordViewController.m
// OldBook
//
// Created by Mat on 9/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import <Parse/Parse.h>

#define KEYBOARDMOVE 200

@interface ResetPasswordViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (assign, nonatomic) CGPoint originalMiddle;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;

@end

@implementation ResetPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  [self.resetButton setEnabled:NO];
  self.originalMiddle = self.view.center;
  UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTappedBackground:)];
  tapGesture.cancelsTouchesInView = NO;
  [self.scrollView addGestureRecognizer:tapGesture];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (void) didTappedBackground:(UITapGestureRecognizer *)sender{
  if(self.emailField.isFirstResponder){
    [self.emailField resignFirstResponder];
  }
  [self hideKeyboardAnimation];
}

- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
  CGPoint newCenter = CGPointMake(0, KEYBOARDMOVE/4);
  [self.scrollView setContentOffset:newCenter animated:YES];
  return YES;
}

- (void) hideKeyboardAnimation {
  [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (IBAction) checkUserValid:(id)sender {
  if([self.emailField.text length]!=0){
    [self.resetButton setEnabled:YES];
  } else {
    [self.resetButton setEnabled:NO];
  }
}

- (IBAction)resetPassword:(id)sender {
  [PFUser requestPasswordResetForEmailInBackground:[self emailField].text block:^(BOOL succeeded, NSError *error) {
    if(!succeeded){
      if(error.code==125){
        UIAlertView *failedResetRequest = [[UIAlertView alloc] initWithTitle:@"Reset Failed"
                                       message:@"We could not find an account with that email address."
                                      delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [failedResetRequest show];
      }
      self.emailField.text=nil;
      [self checkUserValid:nil];
    } else {
      UIAlertView *resetSent = [[UIAlertView alloc] initWithTitle:@"Reset Sent"
                                message:@"A password reset email has been sent, please check your inbox."
                                delegate:nil
                           cancelButtonTitle:@"OK"
                           otherButtonTitles:nil];
      [resetSent show];
      [self performSegueWithIdentifier:@"RETURNTOLOGIN" sender:self];
    }
  }];
}

@end
