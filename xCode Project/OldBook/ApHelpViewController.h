//
//  ApHelpViewController.h
//  OldBook
//
//  Created by Sindre on 9/10/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApNavViewController.h"

@interface ApHelpViewController : UIViewController
@property (strong, nonatomic) ApNavViewController* masterNav;

@end
