//
//  WelcomeViewController.m
//  OldBook
//
//  Created by Sindre on 9/10/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import "WelcomeViewController.h"
#import "OBCarerUser.h"

@interface WelcomeViewController ()

@property (strong, nonatomic) UIAlertView* logOutAlert;
@property (strong, nonatomic) OBCarer* user;
@end

@implementation WelcomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.user = [OBCarerUser getCurrentUser];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)logOutButtonPressed:(id)sender {
    self.logOutAlert = [[UIAlertView alloc] initWithTitle:@"Log Out?"
                                                  message:@"Are you sure you want to log out?"
                                                 delegate:self
                                        cancelButtonTitle:@"NO"
                                        otherButtonTitles:@"YES",
                        nil];
    [self.logOutAlert show];
    return;
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(buttonIndex != [alertView cancelButtonIndex]){
        if(alertView == self.logOutAlert){
            [OBCarerUser logOut];
            AppDelegate* app = [AppDelegate getCurrentAppDelegate];
            [app presentStoryBoardView:@"LOGINVIEW"];
        }
        return;
    } else {
        [self resignFirstResponder];
    }
}

@end
