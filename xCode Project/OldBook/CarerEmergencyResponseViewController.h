//
// CarerEmergencyResponseViewController.h
// OldBook
//
// Created by Mat on 9/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBEmergencyRequest.h"
#import "OBCarerUser.h"

@interface CarerEmergencyResponseViewController : UIViewController

//The emergency request for our given user.
@property (strong, nonatomic) OBEmergencyRequest *request;
//The caerer user
@property (strong, nonatomic) OBCarerUser *carer;

@end
