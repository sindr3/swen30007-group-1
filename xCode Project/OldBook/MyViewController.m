//
//  MyViewController.m
//  OldBook
//
//  Created by Sindre on 30/09/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import "MyViewController.h"
//#import "MySplitViewController.h"
#import "OBUser.h"
#import "AppDelegate.h"


@interface MyViewController ()
@property (strong, nonatomic) UIAlertView *logOutAlert;

@end

@implementation MyViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)logOutButtonPressed:(id)sender
{
    self.logOutAlert = [[UIAlertView alloc] initWithTitle:@"Log Out?"
                                                  message:@"Are you sure you want to log out?"
                                                 delegate:self
                                        cancelButtonTitle:@"NO"
                                        otherButtonTitles:@"YES", nil];
    [self.logOutAlert show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == self.logOutAlert) {
        if (buttonIndex != [alertView cancelButtonIndex]) {
            [OBUser logOut];
            AppDelegate* app = [AppDelegate getCurrentAppDelegate];
            [app presentStoryBoardView:@"LOGINVIEW"];
        } else {
            [self resignFirstResponder];
        }
    } else {
        [self resignFirstResponder];
    }
}

@end
