//
// ClientDetailsViewController-iPad.m
// OldBook
//
//  Created by Sindre on 30/09/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import "MySplitViewController.h"
#import "OBUser.h"
#import "AppDelegate.h"
#import "LoginViewController-iPad.h"

@interface MySplitViewController () <UISplitViewControllerDelegate>

@end

@implementation MySplitViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}


- (BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation{
    return YES;
}

@end
