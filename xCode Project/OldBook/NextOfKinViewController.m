//
// NextOfKinViewController.m
// OldBook
//
// Created by Mat on 9/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "NextOfKinViewController.h"
#import "OBEmergencyContact.h"

@interface NextOfKinViewController () <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *contactName;
@property (weak, nonatomic) IBOutlet UIImageView *contactImage;
@property (weak, nonatomic) IBOutlet UILabel *clientLastSeen;
@property (weak, nonatomic) IBOutlet UITableView *familyTableView;
@property (weak, nonatomic) IBOutlet UITableView *medicalTableView;
@property (strong, nonatomic) NSString *phone;
@end

@implementation NextOfKinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  [self.contactImage setImage:[self.client profilePicture]];
  [self.contactName setText:[self.client getFullName]];
  [self.clientLastSeen setText:[self.client getLastSeenString]];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  if(tableView == self.familyTableView){
    return self.client.nextOfKin.count;
  } else {
    return self.client.medicalContacts.count;
  }
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell;
  
  if(tableView==self.familyTableView){
    cell = [tableView dequeueReusableCellWithIdentifier:@"FAMILYCELL" forIndexPath:indexPath];
    OBEmergencyContact *contact = [self.client.nextOfKin objectAtIndex:indexPath.row];
    [cell.textLabel setText:contact.firstName];
    [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:22]];
  } else {
    cell = [tableView dequeueReusableCellWithIdentifier:@"MEDICALCELL" forIndexPath:indexPath];
    OBEmergencyContact *contact = [self.client.medicalContacts objectAtIndex:indexPath.row];
    [cell.textLabel setText:contact.firstName];
    [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:22]];
  }
  
  return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
  OBEmergencyContact *contact;
  if(tableView==self.familyTableView){
    contact = [self.client.nextOfKin objectAtIndex:indexPath.row];
  } else {
    contact = [self.client.medicalContacts objectAtIndex:indexPath.row];
  }
  NSNumber *phoneNumber = contact.contactNumber;
  self.phone = [@"telprompt://" stringByAppendingString:phoneNumber];
  NSString *message = [NSString stringWithFormat:@"Do you want to call %@?", contact.firstName];
  UIAlertView *phonePrompt = [[UIAlertView alloc] initWithTitle:@"Make A Call"
                             message:message
                            delegate:self
                        cancelButtonTitle:@"Yes"
                        otherButtonTitles:@"No", nil];
  
  [phonePrompt show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if(buttonIndex==0){
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.phone]];
  }
}

@end
