//
//  LoginViewController-iPad.m
//  OldBook
//
//  Created by Sindre on 17/08/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

//#import <QuartzCore/QuartzCore.h>

#import "LoginViewController-iPad.h"
#import "OBUser.h"
#import "MySplitViewController.h"
#import "AppDelegate.h"
#import "SVProgressHUD.h"

@interface LoginViewController_iPad () <UITextFieldDelegate>

@property(nonatomic, strong) NSString* username;
@property(nonatomic, strong) NSString* password;

@property (nonatomic, weak) IBOutlet UITextField* usernameField;
@property (nonatomic, weak) IBOutlet UITextField* passwordField;
@property (nonatomic, strong) IBOutlet UIButton* loginButton;

@end

@implementation LoginViewController_iPad

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialiseTextFieldEventListeners];
    [self.loginButton setEnabled:NO]; // Disable login button
    [self.passwordField setReturnKeyType:UIReturnKeyDefault];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initialiseTextFieldEventListeners
{
    [self.usernameField addTarget:self action:@selector(textFieldValueChanged) forControlEvents:UIControlEventEditingChanged];
    [self.passwordField addTarget:self action:@selector(textFieldValueChanged) forControlEvents:UIControlEventEditingChanged];
}

- (void)textFieldValueChanged
{
    if ( [self textFieldsNonEmpty] ) {
        [self.loginButton setEnabled:YES];
    } else {
        [self.loginButton setEnabled:NO];
    }
}

- (BOOL)textFieldsNonEmpty
{
    if([self.passwordField.text length] != 0 && [self.usernameField.text length] != 0 ) {
        return YES;
    } else {
        return NO;
    }
}

#pragma View Interactions
- (IBAction)didTappedBackground:(UITapGestureRecognizer *)sender
{
    if(self.passwordField.isFirstResponder){
        [self.passwordField resignFirstResponder];
    }else if(self.usernameField.isFirstResponder){
        [self.usernameField resignFirstResponder];
    }
}

- (IBAction)signUpButtonTapped
{
    [self performSegueWithIdentifier:@"showSignup" sender:self];
}

- (IBAction)forgotPasswordTapped
{
    [self performSegueWithIdentifier:@"showForgotPassword" sender:self];
}

- (IBAction)loginButtonTapped
{
    [self logIn];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

#pragma Login Methods
- (void)logIn {
    [SVProgressHUD showWithStatus:@"Logging in"];
    [OBUser logInWithUserNameInBackground:[self.usernameField text] andPassword:[self.passwordField text] onCompletion:^(NSError *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            [self changeUserView];
        } else {
            NSString *errorMessage = [[error userInfo] objectForKey:@"error"];
            [[[UIAlertView alloc] initWithTitle:@"Log In Error"
                                        message:errorMessage
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        }
    }];
}

- (void)changeUserView
{
    AppDelegate* app = [AppDelegate getCurrentAppDelegate];
    NSString* viewControllerName;
    
    if([OBUser getCurrentUser] ){
        NSString *userType = [OBUser getCurrentUserType];
        
        if([userType isEqualToString:@"SOCIAL"]){
            viewControllerName = @"ROOTSOCIALVIEW";
        }else if([userType isEqualToString:@"ASSISTED"]){
            viewControllerName = @"ROOTAPVIEW";
        }else if([userType isEqualToString:@"CARER"]){
            viewControllerName = @"ROOTCARERVIEW";
        }
    }
    
    [app changeRootViewController:viewControllerName];
}



#pragma Text Field Delegate Methods
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if([self.usernameField.text length] != 0){
        // Username is filled, password field can trigger login with return key
        [self.passwordField setReturnKeyType:UIReturnKeyGo];
    } else {
        [self.passwordField setReturnKeyType:UIReturnKeyDefault];
    }
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    if(textField==self.usernameField){
        [self.passwordField becomeFirstResponder];
    } else {
        [textField resignFirstResponder];
        if( [self textFieldsNonEmpty] ){
            [self logIn];
        }
    }
    return YES;
}


@end
