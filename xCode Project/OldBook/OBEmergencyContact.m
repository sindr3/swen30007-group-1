//
// OBEmergencyContact.m
// OldBook
//
// Created by Mat on 17/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBEmergencyContact.h"

@implementation OBEmergencyContact

// To create an emergency contact from PFUser
+ (OBEmergencyContact *) getEmergencyContactFrom:(PFObject *)eContact
{
  //Our emergency contacts to not neccesarily have a direct link to a user in the system, doctors for example will not be users, thus we create our emergencyContact by downloading from a PFObjet relating to the user
  
  OBEmergencyContact *contact = [OBEmergencyContact new];

  contact.firstName = [eContact objectForKey:@"firstName"];
  contact.lastName = [eContact objectForKey:@"lastName"];
  contact.relation = [eContact objectForKey:@"relation"];
  contact.contactNumber = [eContact objectForKey:@"contactNumber"];

  return contact;
}

@end
