//
// ContactSearchController.h
// OldBook
//
// Created by Mat on 7/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactSearchController : UITableViewController <UISearchBarDelegate, UISearchDisplayDelegate>

@property (strong, nonatomic) NSMutableArray *searchResults;
@property (weak, nonatomic) NSString *searchString;

@end
