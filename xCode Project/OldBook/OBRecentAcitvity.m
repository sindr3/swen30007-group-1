//
// OBRecentAcitvity.m
// OldBook
//
// Created by Mat on 19/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBRecentAcitvity.h"

@implementation OBRecentAcitvity

//Funtion to create it from friend request or from message
+ (OBRecentAcitvity *) createNewActivitytForUser:(OBContact *)user ofType:(NSString *) type
{
  OBRecentAcitvity *newActivity = [OBRecentAcitvity new];
  newActivity.contact = user;
  newActivity.type = type;
  return newActivity;
}

@end
