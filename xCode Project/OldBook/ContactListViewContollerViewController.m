
// ContactListViewContollerViewController.m
// OldBook
//
// Created by Mat on 7/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "ContactListViewContollerViewController.h"
#import "SVProgressHUD.h"
#import "OBContactDetailViewViewController.h"
#import "TSMessage.h"
#import "OBParseQueryManager.h"
#import "OBUser.h"
#import "JSBadgeView.h"

@interface ContactListViewContollerViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *friendCollectionView;
@property (weak, nonatomic) IBOutlet UIView *noFriendsView;
@property (strong, nonatomic) PFQuery *combine;
@property (strong, nonatomic) OBContact *userSelected;
@property (strong, nonatomic) OBUser *currentUser;

@end

@implementation ContactListViewContollerViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	self.currentUser = [OBUser getCurrentUser];
	[self.noFriendsView setHidden:YES];
	if(self.currentUser.friendList){
		self.friendList = self.currentUser.friendList;
		[self.friendCollectionView reloadData];
	} else {
		[self.friendCollectionView setHidden:YES];
	}
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(newFriends)
												 name:@"newFriends"
											   object:nil];
	
	UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self
																							 action:@selector(handleLongPress:)];
	[recognizer setMinimumPressDuration:0.4];
	[self.friendCollectionView addGestureRecognizer:recognizer];
}

- (void) viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	if(self.currentUser.friendList.count!=0){
		self.friendList = self.currentUser.friendList;
		[self.friendCollectionView reloadData];
	} else if (self.currentUser.friendList!=nil){
		[self.noFriendsView setHidden:NO];
	}
}

- (void) viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	if(self.friendList==nil){
		[self loadFriendList];
	}
}


- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	[SVProgressHUD dismiss];
	if([segue.identifier isEqualToString:@"toContactDetails"]){
		OBContactDetailViewViewController *destination = (OBContactDetailViewViewController *) [segue destinationViewController];
		destination.friendUser = self.userSelected;
		destination.currentUser = self.currentUser;
	}
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)longPress
{
	if(longPress.state != UIGestureRecognizerStateBegan
	   || ![self becomeFirstResponder])
		return;
	
	CGPoint p = [longPress locationInView:self.collectionView];
	
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:p];
    if (indexPath == nil){
        NSLog(@"couldn't find index path");
    } else {
        // get the cell at indexPath (the one you long pressed)
        UICollectionViewCell* cell = [self.collectionView cellForItemAtIndexPath:indexPath];
        // do stuff with the cell
    }
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
	return [self.friendList count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	return [self populateParseCell:cv andIndex:indexPath];
}


#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	OBContact *selected = [self.friendList objectAtIndex:indexPath.row];
	if(selected!=nil){
		self.userSelected = selected;
	}
	[self performSegueWithIdentifier:@"toContactDetails" sender:self];
	
}

#pragma mark – UICollectionViewDelegateFlowLayout

// Returns a constant cell size of 75,75 we can adjust this later so that we can properly change the view size of the friend list if we feel like it.
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
	return CGSizeMake(75, 85);;
}

// 3
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
	return UIEdgeInsetsMake(50, 20, 50, 20);
}

#pragma mark - Parse stuff
- (void) loadFriendList {
	[SVProgressHUD showWithStatus:@"Loading Friends"];
	[self.currentUser retrieveFriendlistWithCallback:^(NSArray *friendList, NSError *error) {
		if(!error){
			if(friendList.count!=0){
				//We have friends so reloard the view.
				self.friendList = [NSMutableArray arrayWithArray:friendList];
				[self.friendCollectionView setHidden:NO];
				[self.friendCollectionView reloadData];
				[TSMessage showNotificationInViewController:self
												  withTitle:@"Success!"
												withMessage:@"Loaded Friends"
												   withType:TSMessageNotificationTypeSuccess
											   withDuration:TSMessageNotificationDurationAutomatic
											   withCallback:^{}
												 atPosition:TSMessageNotificationPositionBottom];
			}else {
				[self.noFriendsView setHidden:NO];
			}
			[SVProgressHUD dismiss];
		} else {
			[SVProgressHUD showErrorWithStatus:@"Could Not Load Clients"];
			[SVProgressHUD dismiss];
		}
	}];
}

- (void) newFriends {
	self.friendList = [self.currentUser friendList];
	[self.friendCollectionView reloadData];
	
	[TSMessage showNotificationInViewController:self
									  withTitle:@"New friends!"
									withMessage:@"Someone accepted your friend request."
									   withType:TSMessageNotificationTypeSuccess
								   withDuration:TSMessageNotificationDurationAutomatic
								   withCallback:^{}
									 atPosition:TSMessageNotificationPositionBottom];
}

- (UICollectionViewCell *) populateParseCell:(UICollectionView *) cv andIndex:(NSIndexPath *) indexPath {
	UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"ContactCell" forIndexPath:indexPath];
	OBContact *cellUser = [self.friendList objectAtIndex:indexPath.row];
	
	//Profile picture
	UIImageView *cellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 4.0f, 73, 73)];
	cellImageView.image = cellUser.profilePicture;
	[cell.contentView addSubview:cellImageView];
	
	//Add the badge.
	JSBadgeView *nameLabel = [[JSBadgeView alloc] initWithParentView:cell.contentView alignment:JSBadgeViewAlignmentBottomCenter];
	[nameLabel setBadgeBackgroundColor:[UIColor whiteColor]];
	[nameLabel setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.2f]];
	[nameLabel setBadgeStrokeColor:[UIColor darkGrayColor]];
	[nameLabel setBadgeTextColor:[UIColor blackColor]];
	[nameLabel setBadgeText:cellUser.firstName];
	[cell.contentView addSubview:nameLabel];
	
	//Add the message unread badge.
	//  JSBadgeView *unreadLabel = [[JSBadgeView alloc] initWithParentView:cell.contentView alignment:JSBadgeViewAlignmentBottomCenter];
	//  [unreadLabel setBadgeText:[self.currentUser getUnreadMessagesFor:cellUser]];
	
	return cell;
}

@end
