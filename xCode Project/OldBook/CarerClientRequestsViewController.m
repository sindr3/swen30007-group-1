//
//  ClientRequestsViewController.m
//  OldBook
//
//  Created by Oscar Morrison on 14/10/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import "CarerClientRequestsViewController.h"
#import "OBCarerRequest.h"
#import "NSDate+Helper.h"


@interface CarerClientRequestsViewController () <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *requestsView;
@property (assign, nonatomic) NSInteger selectedIndex;
@property (weak, nonatomic) IBOutlet UILabel *requestInfo;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation CarerClientRequestsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.user = [OBCarerUser getCurrentCarerUser];
    self.requestsView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.activityIndicator startAnimating];
    
    [self.user getClientRequestsForContact:self.client withCallback:^(NSMutableArray *requests, NSError *error) {
        self.requests = [NSArray arrayWithArray:requests];
        self.activityIndicator.hidesWhenStopped = YES;
        [self.activityIndicator stopAnimating];
        [self.requestsView reloadData];
        if([self.requests count]==0)
        {
            self.requestsView.hidden = YES;
        }
    }];

}
#pragma mark - table view


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.requests count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"REQUESTSCELL" forIndexPath:indexPath];
    
    OBCarerRequest *request = [self.requests objectAtIndex:indexPath.row];
    
    [cell.textLabel setText:request.type];
    [cell.detailTextLabel setText:[NSDate stringForDisplayFromDate:request.timeSent]];
    
    return cell;
}

@end