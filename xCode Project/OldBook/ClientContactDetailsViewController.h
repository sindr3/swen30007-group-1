//
//  ClientContactDetailsViewController.h
//  OldBook
//
//  Created by Oscar Morrison on 14/10/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBAssistedContact.h"

@interface ClientContactDetailsViewController : UIViewController
@property (strong, nonatomic) OBAssistedContact *client;

@end
