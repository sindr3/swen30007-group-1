//
//  CarerRequestsViewController.h
//  OldBook
//
//  Created by Sindre on 7/10/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

#import "OBCarerUser.h"
#import "OBRecentAcitvity.h"

@interface CarerRequestsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *refreshButton;
@property (strong, nonatomic) OBCarerUser *carer;

@end
