//
// OBNotificationHandler.m
// OldBook
//
// Created by Mat on 26/09/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBNotificationHandler.h"
#import "AppDelegate.h"
#import "TSMessage.h"
#import "OBEmergencyRequest.h"
#import "OBCarerUser.h"

@implementation OBNotificationHandler

//Will handle a notification received whilst currently running.
//Posts a local notification to be handled anywhere required.
+ (void) handleNotificationWithinApp: (NSDictionary *) userInfo inApp:(UIViewController *) app andStoryboard:(UIStoryboard *) storyboard {
	//Get the string for TYPE
	NSString *type = [userInfo objectForKey:@"type"];
	if([type isEqualToString:@"Message"]){
		PFQuery *q = [PFUser query];
		NSString *userID = [userInfo objectForKey:@"userID"];
		PFUser *u = (PFUser *)[q getObjectWithId:userID];
		[[OBUser getCurrentUser] getMessageinboxForContact:[[OBContact alloc] initWithUser:u] withCallback:^(OBMessageInbox *inbox, NSError *error) {
			[inbox checkForNewMessagesWithUpdate:^(BOOL downloadedNew) {
				[[NSNotificationCenter defaultCenter] postNotificationName:@"reloadMessage" object:self];
			}];
		}];
		
	} else if([type isEqualToString:@"friendRequest"]){
		[[OBUser getCurrentUser] retrieveFriendRequestsWithCallback:^(NSArray *friendRequests, NSError *error) {
			if (!error) {
				[[NSNotificationCenter defaultCenter] postNotificationName:@"newFriendRequest" object:self];
			}
		}];
		
	} else if([type isEqualToString:@"friendLink"]){
		[[OBUser getCurrentUser] updateFriendListWithCallback:^(NSArray *friendList, NSError *error) {
			if (!error) {
				[[NSNotificationCenter defaultCenter] postNotificationName:@"newFriends" object:self];
				[PFPush handlePush:userInfo];
			}
		}];
		
	} else if([type isEqualToString:@"EmergencyRequest"]){
		NSString *currentType = [OBUser getCurrentUserType];
		if([currentType isEqualToString:@"CARER"]){
			// Need to create the OBEmergencyRequest object
			PFQuery *q = [PFQuery queryWithClassName:@"EmergencyRequest"];
			NSString *objectID = [userInfo objectForKey:@"objID"];
			[q getObjectInBackgroundWithId:objectID block:^(PFObject *object, NSError *error) {
				if (!error)
				{
					OBEmergencyRequest *req = [OBEmergencyRequest getEmergencyRequestFrom:object];
					[[OBCarerUser getCurrentCarerUser] setRequest:req];
					
					AppDelegate *delegate =[AppDelegate getCurrentAppDelegate];
					[delegate changeRootViewController:@"CAREREMERGENCY"];
				}
				
				// Show the message anyway.
				[PFPush handlePush:userInfo];
			}];
		}
			 
	} else if([type isEqualToString:@"EmergencyCancel"]){
		NSString *currentType = [OBUser getCurrentUserType];
		if([currentType isEqualToString:@"CARER"]){
			AppDelegate *delegate =[AppDelegate getCurrentAppDelegate];
			[delegate changeRootViewController:@"ROOTCARERVIEW"];
		}
	} else {
		[PFPush handlePush:userInfo];
	}
	
}

//Our method for handling opening the app from a notification. Will
//return the identifier of the view we need to swap to.
+ (NSString *) getViewFromOpenWithNotification: (NSDictionary *) notificationData inApp:(UIViewController *) app andStoryboard:(UIStoryboard *) storyboard {
	return @"HELLO";
}


@end
