//
// OBEmergencyRequest.m
// OldBook
//
// Created by Mat on 19/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBEmergencyRequest.h"
#import "OBAssistedUser.h"
#import "OBAssistedContact.h"

@implementation OBEmergencyRequest

//Function for creating emergency requests from a given contact, designed fro the
//user to make the response.
+ (OBEmergencyRequest *) makeEmergencyRequestFor:(OBAssistedUser *)contact
{
	OBEmergencyRequest *newRequest = [OBEmergencyRequest new];
	newRequest.user= contact.currentParseUser;
	
	PFObject *serverRequest = [PFObject objectWithClassName:@"EmergencyRequest"];
	[serverRequest setObject:contact.currentParseUser forKey:@"EmergencyUser"];
	newRequest.serverRequest = serverRequest;
	
	[serverRequest saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
		if (!succeeded)
			newRequest.serverRequest = nil;
	}];
	
	return newRequest;
}

//Generates an emergency request for from a server side object, to be used in conjunction
//with the push notification that gives the object id from the server
//returns an OBEmergencyRequest that is tied to a specific user.
+ (OBEmergencyRequest *) getEmergencyRequestFrom:(PFObject *)object
{
	OBEmergencyRequest *newRequest = [OBEmergencyRequest new];
	newRequest.user = [object objectForKey:@"EmergencyUser"];
	newRequest.requestingContact = [[OBAssistedContact alloc] initWithUser:newRequest.user];
	newRequest.serverRequest = object;
	
	return newRequest;
}

//Cancel emergency request
- (void) cancelRequest {
	// We need to delete the server object
	[self.serverRequest deleteInBackground];
}

@end