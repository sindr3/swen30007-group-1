//
// OBContact.m
// OldBook
//
// Created by Mat on 14/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBContact.h"
#import "OBParseQueryManager.h"

@implementation OBContact

//Initializers

//Creates a
- (id) initWithUser:(PFUser *)user
{
	[user refresh];
	self.profilePicture = [UIImage imageNamed:@"defaultimage.png"];
	//Get the profile picture form the server
	PFFile *profilePicture = [user objectForKey:@"profilePicture"];
	
	//Check the user has a profile picture otherwise set it as default
	if(profilePicture==nil){
		self.profilePicture = [UIImage imageNamed:@"defaultimage.png"];
	} else {
		if(profilePicture.isDataAvailable){
			[profilePicture getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
				self.profilePicture = [UIImage imageWithData:data];
			}];
		} else {
			//Neeed to download the data from the server
			[profilePicture getDataInBackgroundWithBlock:^(NSData *data, NSError *error) {
				if(!error){
					self.profilePicture = [UIImage imageWithData:data];
				} else {
					NSLog(@"Couldn't download profile picture for this user");
				}
			}];
		}
	}
	
	//Set the name
	self.firstName = [user objectForKey:@"firstName"];
	self.lastName = [user objectForKey:@"lastName"];
	self.emailAddress = user.email;
	//Set the user
	self.objectsUser = user;
	
	//Set the last time updated.
	OBParseQueryManager *QM = [OBParseQueryManager getQueryManager];
	[QM getLastInteractionFor:self.objectsUser withCallBack:^(NSDate *lastInteraction) {
		self.lastUpdated = lastInteraction;
	}];

	return self;
}

// Checks if the last updated time on the server is greater than the current updated time and if
// so, downloads the latest information for this user.
//
//- (void) checkForUpdate
//{
//	if(self.objectsUser!=nil){
//		NSDate *lastUpdate = [self.objectsUser objectForKey:@"updatedAt"];
//		if([self.lastUpdated timeIntervalSinceDate:lastUpdate]>0){
//			[self updateContact];
//		}
//	} else {
//		NSLog(@"Couldn't check for update, not linked to user");
//	}
//}

//Custom accessors
- (NSString *) getFullName
{
	NSString *fullName = [[NSString alloc] initWithFormat:@"%@ %@",self.firstName,self.lastName];
	return fullName;
}

//Function for logging recentActivity
- (void) newSocialInteractionWithType:(NSString *) type
{
	self.lastUpdated = [NSDate date];
	PFObject *newInteraction = [PFObject objectWithClassName:@"socialActivity"];
	[newInteraction setObject:type forKey:@"type"];
	[newInteraction setObject:self.objectsUser forKey:@"user"];
	[newInteraction saveInBackground];
}

-(NSString *) getLastSeenString
{
	NSString *lastSeen = [NSDate stringForDisplayFromDate:self.lastUpdated];
	return lastSeen;
}


@end
