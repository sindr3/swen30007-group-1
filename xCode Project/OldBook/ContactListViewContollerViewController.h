//
// ContactListViewContollerViewController.h
// OldBook
//
// Created by Mat on 7/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+JSMessagesView.h"

@interface ContactListViewContollerViewController : UIViewController

@property(nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) NSMutableArray *friendList;

@end
