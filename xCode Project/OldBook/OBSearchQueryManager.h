//
// OBSearchQueryManager.h
// OldBook
//
// Created by Mat on 15/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "OBUser.h"

@interface OBSearchQueryManager : NSObject

//A table for the latest results
@property (strong,nonatomic) PFQuery *results;
@property (strong,nonatomic) NSMutableArray *searchResults;


//Methods to instantiate and return querymanager
+(id) getSearchQueryManager;

//Methods for string matching
+ (BOOL) NSStringIsValidEmail:(NSString *)checkString;
+ (BOOL) NSStringIsValidPhoneNumber:(NSString *)checkString;
+ (BOOL) NSStringIsValidName:(NSString *)checkString;

//For exact queries
-(void) makeSearchQuery:(NSString *) searchString whereKey:(NSString *)searchKey withCompletion:( void (^)(NSMutableArray *searchResults,NSError *error))completionBlock;
-(void) makeSearchQueryWithoutKeyFor:(NSString *)searchString withCompletion:( void(^)(NSMutableArray *searchResults, NSError *error))completionBlock;


//TO DO, INEXACT SEARCH MATCHING

@end
