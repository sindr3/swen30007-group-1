//
//  ClientDetailsNavViewController.m
//  OldBook
//
//  Created by Oscar Morrison on 14/10/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import "ClientDetailsNavViewController.h"

#import "CarerClientRequestsViewController.h"
#import "ClientContactDetailsViewController.h"


@interface ClientDetailsNavViewController () <UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *clientLastSeen;
@property (weak, nonatomic) IBOutlet UIImageView *clientImage;
@property (weak, nonatomic) IBOutlet UILabel *clientName;

- (IBAction)viewRequests:(id)sender;
- (IBAction)viewMessages:(id)sender;
- (IBAction)viewContact:(id)sender;
- (IBAction)checkIn:(id)sender;

@end

@implementation ClientDetailsNavViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewDidAppear:(BOOL)animated{
    self.clientName.text = [self.client getFullName];
    self.clientLastSeen.text = [self.client getLastSeenString];
    self.clientImage.image = [self.client profilePicture];
}


#pragma mark - IBActions
- (IBAction)checkIn:(id)sender
{
    UIAlertView *checkIn = [[UIAlertView alloc]
                            initWithTitle:@"Check In"
                            message:@"Are you sure you want to check client in?"
                            delegate:self
                            cancelButtonTitle:@"Yes"
                            otherButtonTitles:@"No", nil];
    [checkIn show];
}



-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        //write code for client check int here.
        UIAlertView *confirm = [[UIAlertView alloc]
                                initWithTitle:@"You checked in"
                                message:nil
                                delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [confirm show];
    }

}

- (IBAction)viewContact:(id)sender
{
    [self performSegueWithIdentifier:@"showContact" sender:self];
}

- (IBAction)viewRequests:(id)sender
{
    [self performSegueWithIdentifier:@"showRequests" sender:self];
}

- (IBAction)viewMessages:(id)sender
{
    [self performSegueWithIdentifier:@"showMessagingView" sender:self];
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showContact"]) {
        UINavigationController *navigationController = (UINavigationController *)[segue destinationViewController];
        ClientContactDetailsViewController *destination = (ClientContactDetailsViewController *)navigationController.topViewController;
        destination.client = self.client;
    } else if ([segue.identifier isEqualToString:@"showRequests"]) {
         UINavigationController *navigationController = (UINavigationController *)[segue destinationViewController];
         CarerClientRequestsViewController *destination = (CarerClientRequestsViewController *)navigationController.topViewController;
         destination.client = self.client;
     } else if([segue.identifier isEqualToString:@"showMessagingView"]) {
         self.messagingView = (OBContactDetailViewViewController*) [segue destinationViewController];
         self.messagingView.friendUser = self.client;
         self.messagingView.currentUser = [OBCarerUser getCurrentCarerUser];
    }
}

@end