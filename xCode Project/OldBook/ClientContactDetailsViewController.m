//
//  ClientContactDetailsViewController.m
//  OldBook
//
//  Created by Oscar Morrison on 14/10/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import "ClientContactDetailsViewController.h"
#import "OBEmergencyContact.h"


@interface ClientContactDetailsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *clientName;
@property (weak, nonatomic) IBOutlet UILabel *nextOfKin;
@property (weak, nonatomic) IBOutlet UILabel *relation;
@property (weak, nonatomic) IBOutlet UILabel *contactNumber;


@end

@implementation ClientContactDetailsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

-(void)viewDidAppear:(BOOL)animated{
    self.clientName.text = self.client.getFullName;
    
    if ([self.client.nextOfKin count]>0) {


    OBEmergencyContact *contact = [self.client.nextOfKin objectAtIndex:0];
    self.nextOfKin.text = [NSString stringWithFormat:@"%@ %@", contact.firstName, contact.lastName];
    self.relation.text = contact.relation;
    self.contactNumber.text = [NSString stringWithFormat:@"%@", contact.contactNumber];
    }
    else{
        self.nextOfKin.text = @"";
        self.relation.text =  @"";
        self.contactNumber.text =  @"";
    }
}
@end
