//
//  OBLocationManager.m
//  OldBook
//
//  Created by James Fitzsimmons on 14/10/2013.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBLocationManager.h"

@implementation OBLocationManager

+ (void)updateServerLocationForLoggedInUserWithLocation:(CLLocation *)location inBackgroundWithBlock:(void (^)(BOOL, NSError *))block
{
	PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLocation:location];
	PFUser *user = [PFUser currentUser];
	[user setObject:geoPoint forKey:@"location"];
	[user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
		block(succeeded, error);
	}];
}

+ (void)updateServerLocationForLoggedInUserInBackgroundWithBlock:(void (^)(BOOL succeeded, NSError *error))block
{
	[PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
		if (error)
		{
			block(NO, error);
		}
		
		else
		{
			PFUser *user = [PFUser currentUser];
			[user setObject:geoPoint forKey:@"location"];
			[user saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
				block(succeeded, error);
			}];
		}
	}];
}

+ (void)mapItemForContact:(OBContact *)contact withBlock:(void (^)(MKMapItem *item, NSError *error))completionBlock
{
	[self clLocationForContact:contact withBlock:^(CLLocationCoordinate2D *location, NSError *error) {
		MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:*location addressDictionary:nil];
		
		// Item
		MKMapItem *item = [[MKMapItem alloc] initWithPlacemark:placemark];
		if (completionBlock) completionBlock(item, nil);
	}];
}

+ (void)clLocationForContact:(OBContact *)contact withBlock:(void (^)(CLLocationCoordinate2D *location, NSError *error))completionBlock
{
	[contact.objectsUser refreshInBackgroundWithBlock:^(PFObject *object, NSError *error) {
		if (error && completionBlock) {
			completionBlock (nil, error);
			
		} else {
			PFGeoPoint *userLocation = [object objectForKey:@"location"];
			
			if (!userLocation) {
				NSMutableDictionary* details = [NSMutableDictionary dictionary];
				NSString *errorMessage = [NSString stringWithFormat:@"User %@ %@ does not have location set",
										  [object objectForKey:@"firstName"],
										  [object objectForKey:@"lastName"]];
				[details setValue:errorMessage forKey:NSLocalizedDescriptionKey];
				NSError *error = [NSError errorWithDomain:@"OBLocationManager" code:200 userInfo:details];
				
				if (completionBlock) completionBlock (nil, error);
				
			} else {
				double lat = [userLocation latitude];
				double lon = [userLocation longitude];
				CLLocationCoordinate2D cCLLoc = CLLocationCoordinate2DMake(lat, lon);
				
				if (completionBlock) completionBlock(&cCLLoc, nil);
			}
		}
	}];
}

+ (void)directionsFromContact:(OBContact *)fromContact toContact:(OBContact *)toContact withBlock:(void (^)(MKDirections *directions, NSError *error))completionBlock
{
	[self mapItemForContact:fromContact withBlock:^(MKMapItem *item, NSError *error) {
		if (error)
		{
			completionBlock(nil, error);
		}
		
		else
		{
			MKMapItem *fromItem = item;
			[self mapItemForContact:toContact withBlock:^(MKMapItem *item, NSError *error) {
				if (error)
				completionBlock(nil, error);
				
				else
				{
					MKMapItem *toItem = item;
					
					// Request
					MKDirectionsRequest *request = [[MKDirectionsRequest alloc] init];
					request.source = fromItem;
					request.destination = toItem;
					request.requestsAlternateRoutes = NO;
					
					// Directions
					MKDirections *directions = [[MKDirections alloc] initWithRequest:request];
					completionBlock (directions, nil);
				}
			}];
		}
	}];
}

+ (void)directionsResponseFromContact:(OBContact *)fromContact toContact:(OBContact *)toContact withBlock:(void (^)(MKDirectionsResponse *response, NSError *error))completionBlock
{
	[self directionsFromContact:fromContact
					  toContact:toContact
					  withBlock:^(MKDirections *directions, NSError *error) {
						  if (error)
						  {
							  completionBlock(nil, error);
						  }
						  
						  else
						  {
							  [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
								  completionBlock(response, error);
							  }];
						  }
					  }];
}

+ (void)etaFromContact:(OBContact *)fromContact toContact:(OBContact *)toContact withBlock:(void (^)(MKETAResponse *directions, NSError *error))completionBlock
{
	[self directionsFromContact:fromContact
					  toContact:toContact
					  withBlock:^(MKDirections *directions, NSError *error) {
						  if (error)
						  {
							  completionBlock(nil, error);
						  }
						  
						  else
						  {
							  [directions calculateETAWithCompletionHandler:^(MKETAResponse *response, NSError *error) {
								  completionBlock(response, error);
							  }];
						  }
					  }];
}

@end
