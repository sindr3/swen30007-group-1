//
//  CarerInitialNavigationViewController.m
//  OldBook
//
//  Created by Sindre on 8/10/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import "CarerInitialNavigationViewController.h"
#import "OBUser.h"
#import "OBCarerUser.h"
#import "ClientDetailsNavViewController.h"

@interface CarerInitialNavigationViewController () <UITableViewDataSource, UITableViewDelegate, UISplitViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *clientTable;
@property (strong, nonatomic) NSArray *clientList;
@property (weak, nonatomic) OBCarerUser *carer;
@property (weak, nonatomic) OBAssistedContact *chosenClient;
- (IBAction)overview:(id)sender;




@end

@implementation CarerInitialNavigationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.splitViewController.delegate = self;
      self.carer = [OBCarerUser getCurrentCarerUser];
    
    if(self.clientList==nil){
        [SVProgressHUD showWithStatus:@"Loading Clients List"];
        [self loadClients];
    }
}


- (void) viewDidAppear:(BOOL)animated {
    if(self.clientList==nil){
        [self loadClients];
    }
}


- (void) loadClients {
	[self.carer downloadClientListWithBlock:^(NSArray *clientList, NSError *error) {
		if (!error) {
            [SVProgressHUD dismiss];
			[self setClientList:clientList];
			[self.clientTable reloadData];
		}
	}];
}


#pragma mark Table View 

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.clientList count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CLIENTCELL" forIndexPath:indexPath];

    OBContact *contact = [self.clientList objectAtIndex:indexPath.row];
	
    [cell.imageView setImage:contact.profilePicture];
    [cell.textLabel setText: contact.firstName];
    [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18]];
    [cell.detailTextLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12]];

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
        self.chosenClient = [self.clientList objectAtIndex:indexPath.row];
        [self performSegueWithIdentifier:@"showDetails" sender:self];
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.chosenClient = nil;
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"showDetails"]){
        ClientDetailsNavViewController *destination = (ClientDetailsNavViewController *)[segue destinationViewController];
        destination.client = self.chosenClient;
    }
}




#pragma mark Orientation Methods
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
    return NO;
}


- (IBAction)overview:(id)sender
{
    [self performSegueWithIdentifier:@"showOverview" sender:self];
    
}
@end