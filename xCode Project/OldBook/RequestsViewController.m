//
// RequestsViewController.m
// OldBook
//
// Created by Mat on 9/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "RequestsViewController.h"
#import "OBContactDetailViewViewController.h"

@interface RequestsViewController () <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *requestsLabel;
@property (weak, nonatomic) IBOutlet UITableView *requestsTableView;
@property (weak, nonatomic) IBOutlet UIView *noRequestsView;

@property (assign, nonatomic) NSInteger selectedRequest;
@end

@implementation RequestsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self.requestsLabel setHidden:YES];
	[self.requestsTableView setHidden:YES];
	[self.noRequestsView setHidden:YES];
	[self.lastSeen setText:[self.currentCarer getLastSeenString]];
	[self loadInformation];
	[self.carerImage setImage:[self.currentCarer profilePicture]];
	[self.carersName setText:[self.currentCarer firstName]];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.requestsAvailable count];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [[UITableViewCell alloc] init];
	[cell.textLabel setText:[self.requestsAvailable objectAtIndex:indexPath.row]];
	return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	self.selectedRequest = indexPath.row;
	NSString *str = [[@"Do you need help with " stringByAppendingString:[self.requestsAvailable objectAtIndex:indexPath.row]] stringByAppendingString:@"?"];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Help"
													message:str
												   delegate:self
										  cancelButtonTitle:@"Yes"
										  otherButtonTitles:@"Cancel", nil];
	
	[alert show];
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}


-(void) alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex {
	if(buttonIndex==0){
		[self.currentUser sendCarerRequest:self.selectedRequest to:self.currentCarer];
	}
}

- (void) loadInformation
{
	//Load friend requests
	if(self.requestsAvailable==nil){
		[self.currentUser getCarerRequestsFor:self.currentCarer withCallback:^(NSArray *carerRequests, NSError *error) {
			[self setRequestsAvailable:[NSMutableArray arrayWithArray:carerRequests]];
			
			//If carer requests aren't available. show appropriate view
			if(self.requestsAvailable.count==0){
				[self.noRequestsView setHidden:NO];
			} else {
				[self.requestsLabel setHidden:NO];
				[self.requestsTableView setHidden:NO];
			}
		}];
	}
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	if([segue.identifier isEqualToString:@"toContactDetails"]){
		OBContactDetailViewViewController *view = (OBContactDetailViewViewController *)[segue destinationViewController];
		[view setFriendUser:self.currentCarer];
	}
}


@end
