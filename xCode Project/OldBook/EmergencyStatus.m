//
// EmergencyStatus.m
// OldBook
//
// Created by Mat on 8/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#define METERS_PER_MILE 1609.344

#import "EmergencyStatus.h"

#import <MapKit/MapKit.h>

#import "OBLocationManager.h"
#import "OBAssistedUser.h"
#import "AMBlurView.h"

@interface EmergencyStatus () <UIAlertViewDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) AMBlurView *blurView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (weak, nonatomic) CLLocation *currentLocation;
@property (weak, nonatomic) OBAssistedUser *currentUser;

@property (weak, nonatomic) IBOutlet UILabel *minutesToArrivalLabel;
@property (weak, nonatomic) IBOutlet UIView *arrivalTimeView;

@end

@implementation EmergencyStatus

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	[self setCurrentUser:[OBAssistedUser getCurrentUser]];
	
	// Create the location manager if this object does not
	// already have one.
	if (nil == self.locationManager)
		self.locationManager = [[CLLocationManager alloc] init];
	
	self.locationManager.delegate = self;
	self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
	
	// Set a movement threshold for new events.
	self.locationManager.distanceFilter = 500;
	
	[self.locationManager startUpdatingLocation];
	
	/*
	// Blur the map out.
	[self setBlurView:[AMBlurView new]];
	[[self blurView] setFrame:CGRectMake(0, 0, [self.view bounds].size.width, [self.view bounds].size.height)];
    [[self blurView] setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
    [self.view addSubview:[self blurView]];
	[self.blurView setAlpha:0.8f];
	 */
	
	[self.arrivalTimeView setAlpha:0];
	[SVProgressHUD showWithStatus:@"Finding carer..."];
	[self performSelectorInBackground:@selector(refreshCarerLocation) withObject:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	CLLocationCoordinate2D zoomLocation = self.currentLocation.coordinate;
	MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
	[_mapView setRegion:viewRegion animated:YES];
}

- (void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
	self.currentLocation = [locations lastObject];
	[OBLocationManager updateServerLocationForLoggedInUserWithLocation:self.currentLocation inBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
		//
	}];
	
	return;
}

- (void)refreshCarerLocation
{
	[self.currentUser locationForCarerInBackgroundWithBlock:^(CLLocationCoordinate2D *location, NSError *error) {
		if (!error) {
			
			[SVProgressHUD dismiss];
			[self.mapView setShowsUserLocation:NO];
			[self.mapView removeAnnotations:[self.mapView annotations]];
			[self.mapView setCenterCoordinate:*location];
			
			MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
			annotation.coordinate = *location;
			annotation.title = NSLocalizedString(@"Carer is here", @"Title");
			NSString *date = [[NSDate date] stringWithDateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterMediumStyle];
			annotation.subtitle = [NSString stringWithFormat:@"Last updated at %@", date];
			[self.mapView addAnnotation:annotation];
			
			// Update again in 30 seconds
			[self performSelector:@selector(refreshCarerLocation) withObject:nil afterDelay:30];
			
		} else {
			[SVProgressHUD showWithStatus:@"Finding carer..."];
			[self performSelector:@selector(refreshCarerLocation) withObject:nil afterDelay:10];
		}
	}];
	
	[self.currentUser etaForCarerInBackgroundWithBlock:^(MKETAResponse *response, NSError *error) {
		if (!error) {
			NSInteger minutes = ceil([response expectedTravelTime] / 60);
			NSString *minutesString = [NSString stringWithFormat:@"%d", minutes];
			[self.minutesToArrivalLabel setText:minutesString];
			
			[UIView animateWithDuration:0.5f animations:^{
				[self.arrivalTimeView setAlpha:1];
			}];
		}
	}];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (IBAction)emergencyCancelled:(id)sender{
	UIAlertView *areYouSureAlert = [[UIAlertView alloc] initWithTitle:@"Cancel Emergency"
															  message:@"Are You Sure You Want To Cancel?"
															 delegate:self
													cancelButtonTitle:@"NO"
													otherButtonTitles:@"YES",
									nil];
	[areYouSureAlert show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if(buttonIndex == [alertView cancelButtonIndex]){
		return;
	} else {
		// Send cancel notification
		[[OBAssistedUser getCurrentUser] cancelEmergencyRequest];
		[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
	}
}


@end
