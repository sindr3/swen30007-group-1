//
// OBParseQueryManager.m
// OldBook
//
// Created by Mat on 15/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBParseQueryManager.h"
#import "OBAssistedContact.h"
#import "OBEmergencyContact.h"
#import "OBEmergencyRequest.h"
#import "OBCarerRequest.h"
#import "OBRecentAcitvity.h"

@interface OBParseQueryManager ()

@property (strong, nonatomic) PFQuery *friendListQuery;

@end


@implementation OBParseQueryManager

static OBParseQueryManager *qm = nil;

//Create a new black query manager
+ (id) initializeQueryManager
{
	OBParseQueryManager *qm = [[OBParseQueryManager alloc] init];
	return qm;
}

//Get the current query manager, a singleton instance that is accessible from anywhere
+ (id) getQueryManager
{
	if(qm==nil){
		qm = [OBParseQueryManager initializeQueryManager];
	}
	return qm;
}


//===========================================
// -- Social and Assisted User Methods ------
//===========================================

//Query for friendlist relevant to the user.
- (void) queryForFriendlistForUser:(PFUser *)user onCompletion: (void(^)(NSArray *friendList,NSError *error))completion
{
	
	//Get friend list from parse
	PFQuery *firstQuery =[PFQuery queryWithClassName:@"friendLink"];
	[firstQuery whereKey:@"personOne" equalTo:[PFUser currentUser]];
	PFQuery *secondQuery =[PFQuery queryWithClassName:@"friendLink"];
	[secondQuery whereKey:@"personTwo" equalTo:[PFUser currentUser]];
	
	//Combine the list
	self.friendListQuery = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:firstQuery,secondQuery,nil]];
	self.friendListQuery.cachePolicy = kPFCachePolicyNetworkElseCache;
	
	//Now search in background and execute block with results
	
	//Load the list from parse
	[self.friendListQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if(!error){
			if(objects.count==0){
				completion(nil,error);
			} else {
				completion([self friendsFromFriendLinks:objects], error);
			}
		} else {
			completion(nil,error);
		}
	}];
}

//Get the list of frienSd Requests for a user
- (void) retrieveFriendRequestsWithCallback:(void(^)(NSArray *friendRequests, NSError *error))callback
{
	PFQuery *findRequests = [PFQuery queryWithClassName:@"friendRequest"];
	[findRequests whereKey:@"toUser" equalTo:[PFUser currentUser]];
	[findRequests findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if(!error && objects.count!=0){
			//Then go through the array and get the users from each thing,
			NSMutableArray *userObjects = [NSMutableArray array];
			for(int i=0; i<objects.count; i++){
				PFObject *friendRequest = [objects objectAtIndex:i];
				PFUser *user = [friendRequest objectForKey:@"fromUser"];
				[userObjects addObject:user];
			}
			
			NSMutableArray *new = [NSMutableArray arrayWithArray:[OBParseQueryManager contactsFromUsers:userObjects]];
			callback(new,error);
		}
	}];
	
}

//Changes an array of friendlinks to PFuser;
- (NSArray *) friendsFromFriendLinks:(NSArray *) friendLinks
{
	//Cool now we have a list of friendLinks, we need to go through each object, and for each object find the person who is not us.
	int max = friendLinks.count;
	NSMutableArray *friendList = [NSMutableArray array];
	for(int i=0; i<max; i++){
		PFObject *fLink = [friendLinks objectAtIndex:i];
		PFUser *firstUser = [fLink objectForKey:@"personOne"];
		if([firstUser.objectId isEqualToString:[PFUser currentUser].objectId]){
			//then we need the other one
			PFUser *toAdd = [fLink objectForKey:@"personTwo"];
			[friendList addObject:toAdd];
		} else {
			//Then add this one
			[friendList addObject:firstUser];
		}
	}
	return friendList;
}

//Query for messages between user1 and user2
- (void) queryForMessagesBetween:(PFUser *)userOne andUser:(PFUser *)userTwo onCompletion:(void(^)(NSArray *objects, NSError *error))completionCode
{
	
	//All received messages
	PFQuery *sentMessages = [PFQuery queryWithClassName:@"Message"];
	[sentMessages whereKey:@"fromUser" equalTo:userOne];
	[sentMessages whereKey:@"toUser" equalTo:userTwo];
	//All sent messagtes
	PFQuery *receivedMessages = [PFQuery queryWithClassName:@"Message"];
	[receivedMessages whereKey:@"toUser" equalTo:userOne];
	[receivedMessages whereKey:@"fromUser" equalTo:userTwo];
	//Combine them and sort by date created
	PFQuery *combine = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:receivedMessages,sentMessages, nil]];
	[combine orderByDescending:@"createdAt"];
	//Get objects
	[combine findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
		if(!error){
			completionCode(objects, error);
		} else {
			completionCode(nil, error);
		}
	}];
}

- (void) queryForMessagesBetween:(PFUser *)userOne andUser:(PFUser *)userTwo newerThan:(NSDate *)date onCompletion:(void(^)(NSArray *objects, NSError *error))completionCode
{
	//All received messages
	PFQuery *sentMessages = [PFQuery queryWithClassName:@"Message"];
	[sentMessages whereKey:@"fromUser" equalTo:userOne];
	[sentMessages whereKey:@"toUser" equalTo:userTwo];
	//All sent messagtes
	PFQuery *receivedMessages = [PFQuery queryWithClassName:@"Message"];
	[receivedMessages whereKey:@"toUser" equalTo:userOne];
	[receivedMessages whereKey:@"fromUser" equalTo:userTwo];
	//Combine them and sort by date created
	PFQuery *combine = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:receivedMessages,sentMessages, nil]];
	[combine orderByDescending:@"createdAt"];
	combine.limit = 20;
	
	//Get objects
	[combine findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
		if(!error){
			NSMutableArray *array = [NSMutableArray new];
			//For each of the 20 newest, see if they are newer and if so add
			for(PFObject *o in objects){
				if([o.createdAt timeIntervalSinceDate:date]>0){
					[array addObject:o];
				}
			}
			completionCode(array, error);
		} else {
			completionCode(nil, error);
		}
	}];
}

- (void) queryForMessagesFrom:(PFUser *)userOne toUser:(PFUser *)userTwo newerThan:(NSDate *)date onCompletion:(void(^)(NSArray *objects, NSError *error))completionCode
{
	//All received messages
	PFQuery *sentMessages = [PFQuery queryWithClassName:@"Message"];
	[sentMessages whereKey:@"fromUser" equalTo:userTwo];
	[sentMessages whereKey:@"toUser" equalTo:userOne];
	[sentMessages orderByDescending:@"createdAt"];
	sentMessages.limit = 20;
	
	//Get objects
	[sentMessages findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
		if(!error){
			NSMutableArray *array = [NSMutableArray new];
			//For each of the 20 newest, see if they are newer and if so add
			for(PFObject *o in objects){
				if([o.createdAt timeIntervalSinceDate:date]>0){
					[array addObject:o];
				}
			}
			completionCode(array, error);
		} else {
			completionCode(nil, error);
		}
	}];
}

//Find friendLink for two given users
- (void) findFriendLink:(PFUser *)userOne andUser:(PFUser *)userTwo withCompletion:(void (^)(NSArray *friendLinks, NSError *error))completion
{
	//Find the link, have to check both directions
	PFQuery *peopleToDelete = [PFQuery queryWithClassName:@"friendLink"];
	[peopleToDelete whereKey:@"personOne" equalTo:userOne];
	[peopleToDelete whereKey:@"personTwo" equalTo:userTwo];
	PFQuery *peopleToDelete2 = [PFQuery queryWithClassName:@"friendLink"];
	[peopleToDelete2 whereKey:@"personTwo" equalTo:userOne];
	[peopleToDelete2 whereKey:@"personOne" equalTo:userTwo];
	
	//Combine the query
	
	//Find the one with an object
	if(peopleToDelete.countObjects!=0){
		//This one has it
		[peopleToDelete findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
			//Found the person
			completion(objects,error);
		}];
	} else {
		[peopleToDelete2 findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
			//Found the person
			completion(objects,error);
		}];
	}
}

//Find nearby friends for a given location
- (void) findNearbyFriendsFor:(PFGeoPoint *)location withBlock:(void (^)(NSArray *friends, NSError *err))block
{
	PFQuery *locationQuery = [PFUser query];
	[locationQuery whereKey:@"location" nearGeoPoint:location withinKilometers:5.0];
	[locationQuery whereKey:@"type" notEqualTo:@"CARER"];
	[locationQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if(!error){
			//Convert users to OBContacts
			block([OBParseQueryManager contactsFromUsers:objects],error);
		} else {
			block(nil,error);
		}
	}];
}

//==============================
// -- Assisted User methods ----
//==============================

//Changes an array of carers to PFuser;
- (NSArray *) carersFromCarerLinks:(NSArray *) carerLinks
{
	//Cool now we have a list of friendLinks, we need to go through each object, and for each object find the person who is not us.
	int max = carerLinks.count;
	NSMutableArray *carerList = [NSMutableArray array];
	for(int i=0; i<max; i++){
		PFObject *link = [carerLinks objectAtIndex:i];
		PFUser *carer = [link objectForKey:@"carer"];
		[carerList addObject:carer];
	}
	return carerList;
}

//Find list of carers for a user;
- (void) findCarersFor:(PFUser *)assistedPerson withCallback:(void (^)(NSArray *carers,NSError *error))callback
{
	PFQuery *carerQuery = [PFQuery queryWithClassName:@"carerLink"];
	[carerQuery whereKey:@"assistedPerson" equalTo:assistedPerson];
	[carerQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if(!error){
			
			//Then we need the users from the carerLinks
			NSArray *carers = [self carersFromCarerLinks:objects];
			callback([OBParseQueryManager carerContactsFromUsers:carers],error);
		} else {
			callback(nil,error);
		}
	}];
}

//Find the single carer for a user;
- (void) findCarerFor:(PFUser *)assistedPerson withCallback:(void (^)(OBCarer *carer,NSError *error))callback
{
	PFQuery *carerQuery = [PFQuery queryWithClassName:@"carerLink"];
	[carerQuery whereKey:@"assistedPerson" equalTo:assistedPerson];
	[carerQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if(!error && objects){
			//Then we need the users from the carerLinks
			NSArray *carers = [self carersFromCarerLinks:objects];
			if ([carers count] > 0) {
				callback([[OBParseQueryManager carerContactsFromUsers:carers] objectAtIndex:0],error);
				
			} else {
				// No carers registered
				NSMutableDictionary* details = [NSMutableDictionary dictionary];
				[details setValue:@"AP has no registered carers" forKey:NSLocalizedDescriptionKey];
				NSError *error = [NSError errorWithDomain:@"OBParseQueryManager" code:200 userInfo:details];
				callback(nil, error);
			}
			
		} else {
			callback(nil,error);
		}
	}];
}

//Find the requests available for a carer;
- (void) findCarerRequestsFor:(PFUser *)carer andUser:(PFUser *)user withCallback:(void (^)(NSArray *available,NSError *error))callback
{
	PFQuery *requestsQuery = [PFQuery queryWithClassName:@"carerLink"];
	[requestsQuery whereKey:@"assistedPerson" equalTo:user];
	[requestsQuery whereKey:@"carer" equalTo:carer];
	[requestsQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
		if (!error) {
			[object fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
				//This is our carer link, we want to retrieve the array from it of available requests.
				NSArray *requests = [NSArray arrayWithArray:[object objectForKey:@"requestsAvailable" ]];
				callback (requests, error);
			}];
		} else {
			callback (nil, error);
		}
	}];
	
}

//Find the requests sent for a carer;
- (void) findSentCarerRequestsFor:(PFUser *)user withCallback:(void (^)(NSArray *available,NSError *error))callback
{
	PFQuery *requestsQuery = [PFQuery queryWithClassName:@"carerRequest"];
	[requestsQuery whereKey:@"assistedPerson" equalTo:user];
	[requestsQuery findObjectsInBackgroundWithBlock:^(NSArray *object, NSError *error) {
		if(!error){
			NSMutableArray *array = [NSMutableArray new];
			//Cool now we have the carer request, get the array from the link.
			for(PFObject *o in object){
				OBCarerRequest *request = [OBCarerRequest createRequestFromPFCarerRequest:o];
				[array addObject:request];
			}
			callback(array, error);
		} else {
			callback(nil, error);
		}
	}];
}

//=========================
// -- Utitlity methods ----
//=========================

// Get the PFUsers from an array of carer links
+(NSArray *) apsFromCarerLinks:(NSArray *)carerLinks
{
	NSMutableArray *aps = [NSMutableArray array];
	for (PFObject *link in carerLinks) {
		PFUser *ap = [link objectForKey:@"assistedPerson"];
		[aps addObject:ap];
	}
	return aps;
}

//Change an array of users into an array of obassistedcontacts
+ (NSArray *) carerContactsFromUsers:(NSArray *) users
{
	NSMutableArray *newArray = [NSMutableArray array];
	for(PFUser *u in users){
		OBCarer *newContact = [[OBCarer alloc] initWithUser:u];
		[newArray addObject:newContact];
	}
	return newArray;
}

//Change an array of users into an array of obcontacts
+ (NSArray *) contactsFromUsers:(NSArray *) users
{
	NSMutableArray *newArray = [NSMutableArray array];
	for(PFUser *u in users){
		[newArray addObject:[[OBContact alloc] initWithUser:u]];
	}
	return newArray;
}

//Change an array of users into an array of obassistedcontacts
+ (NSArray *) assistedContactsFromUsers:(NSArray *) users
{
	NSMutableArray *newArray = [NSMutableArray array];
	for(int i=0; i<users.count; i++){
		OBAssistedContact *newContact = [OBAssistedContact getAssistedContactFromUser:[users objectAtIndex:i]];
		[newArray addObject:newContact];
	}
	return newArray;
}

//Find emergency contacts
+ (NSArray *) emergencyContactsFromObjects:(NSArray *)users
{
	NSMutableArray *newArray = [NSMutableArray new];
	//Now we need to go through each friend request and extract the user from it
	for(int i=0; i<users.count; i++){
		OBEmergencyContact *newContact = [OBEmergencyContact getEmergencyContactFrom:[users objectAtIndex:i]];
		[newArray addObject:newContact];
	}
	return newArray;
}

+ (void) findAndDeleteFriendRequestFrom:(PFUser *)user to:(PFUser *)recipient
{
	PFQuery *findRequests = [PFQuery queryWithClassName:@"friendRequest"];
	[findRequests whereKey:@"toUser" equalTo:recipient];
	[findRequests whereKey:@"fromUser" equalTo:user];
	[findRequests getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
		[object deleteEventually];
	}];
}

//======================
// -- Carer methods ----
//======================

//Find the clients for a given carer
- (void) findClientsFor:(PFUser *)carer withBlock:(void (^)(NSArray *clientList, NSError *error))completionBlock
{
	PFQuery *clientQuery = [PFQuery queryWithClassName:@"carerLink"];
	[clientQuery whereKey:@"carer" equalTo:carer];
	[clientQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if(!error){
			NSArray *aps = [OBParseQueryManager apsFromCarerLinks:objects];
			completionBlock([OBParseQueryManager assistedContactsFromUsers:aps], error);
		} else {
			completionBlock(nil, error);
		}
	}];
}

//Find nearby carers for a given location
- (void) findNearbyCarersFor:(PFGeoPoint *)location withBlock:(void (^)(NSArray *friends, NSError *err))block
{
	PFQuery *locationQuery = [PFUser query];
	[locationQuery whereKey:@"location" nearGeoPoint:location withinKilometers:5.0];
	[locationQuery whereKey:@"type" equalTo:@"CARER"];
	[locationQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if(!error){
			//Convert users to OBContacts
			block([OBParseQueryManager contactsFromUsers:objects],error);
		} else {
			block(nil,error);
		}
	}];
}

//Find the OBCarerRequests available from a carer from an AP.
- (void) findRequestsForCarer:(PFUser *)carer fromUser:(PFUser *)user withBlock:(void (^)(NSArray *requests, NSError *error))block
{
	PFQuery *requestsQuery = [PFQuery queryWithClassName:@"carerLink"];
	[requestsQuery whereKey:@"assistedPerson" equalTo:user];
	[requestsQuery whereKey:@"carer" equalTo:carer];
	
	[requestsQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
			NSMutableArray *requests = [NSMutableArray array];
			for (PFObject *o in objects) {
				[requests addObject:[OBCarerRequest createRequestFromPFCarerRequest:o]];
			}
			block (requests, error);
		} else {
			block (nil, error);
		}
	}];
}

//Find emergency contacts for a user
- (void) findEmergencyContactsFor:(OBAssistedContact *)contact withBlock:(void (^)(NSArray *emergencyContacts, NSError *error))completionBlock
{
	PFQuery *emergencyQuery = [PFQuery queryWithClassName:@"emergencyContact"];
	[emergencyQuery whereKey:@"userRelation" equalTo:contact.objectsUser];
	[emergencyQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error){
		if(!error){
			//Then we need to extract the contracts for the friends list.
			completionBlock([OBParseQueryManager emergencyContactsFromObjects:objects],error);
		}
	}];
}

- (OBEmergencyRequest *) retrieveEmergencyRequestFor:(OBAssistedContact *)contact
{
	PFQuery *query = [PFQuery queryWithClassName:@"emergencyRequest"];
	[query whereKey:@"assistedUser" equalTo:contact.objectsUser];
	OBEmergencyRequest *emergencyContact = [OBEmergencyRequest getEmergencyRequestFrom:[query getFirstObject]];
	return emergencyContact;
}

- (void) getLastInteractionFor:(PFUser *) user withCallBack:(void (^)(NSDate *lastInteraction)) callback
{
	//To be used so that we know the last time the person used the service
	PFQuery *socialQuery = [PFQuery queryWithClassName:@"socialActivity"];
	[socialQuery whereKey:@"user" equalTo:user];
	[socialQuery orderByDescending:@"createdAt"];
	[socialQuery getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error) {
//		NSDate* localDateTime = [NSDate dateWithTimeInterval:[[NSTimeZone systemTimeZone] secondsFromGMT] sinceDate:object.createdAt];
		callback(object.createdAt);
	}];
}

- (void) getInteractionsForUsers:(NSMutableArray *) users withCallback:(void (^)(NSArray *requests, NSError *error))completionBlock
{
	//Make the users array.
	NSMutableArray *parseUsers = [NSMutableArray new];
	for(OBContact *c in users){
		[parseUsers addObject:c.objectsUser];
	}
	
	//To be used so that we know the last time the person used the service
	PFQuery *socialQuery = [PFQuery queryWithClassName:@"socialActivity"];
	[socialQuery whereKey:@"user" containedIn:parseUsers];
	[socialQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
		if (!error) {
			NSMutableArray *results = [NSMutableArray new];
			for(PFObject *o in objects){
				NSString *type = [o objectForKey:@"type"];
				PFUser *user = [o objectForKey:@"user"];
				OBContact *contact = [[OBContact alloc] initWithUser:user];
				[results addObject:[OBRecentAcitvity createNewActivitytForUser:contact ofType:type]];
			}
			if(completionBlock!=nil){
				completionBlock(results,nil);
			}
		}
		else {
			if(completionBlock!=nil){
				completionBlock(nil,nil);
			}
		}
	}];
}
@end

//===========================================
// -- Synchronous methods for accesing ------
// -- The data stored on parse.    ------
//===========================================

@implementation OBSYNCHParseQueryManager

static OBSYNCHParseQueryManager *synchqm = nil;

//Create a new black query manager
+ (id) initializeSynchQueryManager
{
	OBSYNCHParseQueryManager *synchqm = [[OBSYNCHParseQueryManager alloc] init];
	return synchqm;
}

//Get the current query manager, a singleton instance that is accessible from anywhere
+ (id) getSynchQueryManager
{
	if(synchqm==nil){
		synchqm = [OBSYNCHParseQueryManager initializeSynchQueryManager];
	}
	return synchqm;
}

//===========================================
// -- Social and Assisted User Methods ------
//===========================================

//Query for friendlist relevant to the user.
-(NSArray *) queryForFriendlistForUserSYNCH:(PFUser *) user
{
	
	//Get friend list from parse
	PFQuery *firstQuery =[PFQuery queryWithClassName:@"friendLink"];
	[firstQuery whereKey:@"personOne" equalTo:[PFUser currentUser]];
	PFQuery *secondQuery =[PFQuery queryWithClassName:@"friendLink"];
	[secondQuery whereKey:@"personTwo" equalTo:[PFUser currentUser]];
	
	//Combine the list
	self.friendListQuery = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:firstQuery,secondQuery,nil]];
	self.friendListQuery.cachePolicy = kPFCachePolicyNetworkElseCache;
	
	//Load the list from parse
	NSArray *friends = [NSArray arrayWithArray:[self.friendListQuery findObjects]];
	return [self friendsFromFriendLinksSYNCH:friends];
}

//Get the list of frienSd Requests for a user
- (NSArray *) retrieveFriendRequestsWithCallbackSYNCH
{
	PFQuery *findRequests = [PFQuery queryWithClassName:@"friendRequest"];
	[findRequests whereKey:@"toUser" equalTo:[PFUser currentUser]];
	NSArray *objects = [findRequests findObjects];
	//Then go through the array and get the users from each thing,
	NSMutableArray *userObjects = [NSMutableArray array];
	for(int i=0; i<objects.count; i++){
		PFObject *friendRequest = [objects objectAtIndex:i];
		PFUser *user = [friendRequest objectForKey:@"fromUser"];
		[userObjects addObject:user];
	}
	NSMutableArray *new = [NSMutableArray arrayWithArray:[OBParseQueryManager contactsFromUsers:userObjects]];
	return new;
}

//Changes an array of friendlinks to PFuser;
- (NSArray *) friendsFromFriendLinksSYNCH:(NSArray *) friendLinks
{
	//Cool now we have a list of friendLinks, we need to go through each object, and for each object find the person who is not us.
	int max = friendLinks.count;
	NSMutableArray *friendList = [NSMutableArray array];
	for(int i=0; i<max; i++){
		PFObject *fLink = [friendLinks objectAtIndex:i];
		PFUser *firstUser = [fLink objectForKey:@"personOne"];
		if([firstUser.objectId isEqualToString:[PFUser currentUser].objectId]){
			//then we need the other one
			PFUser *toAdd = [fLink objectForKey:@"personTwo"];
			[friendList addObject:toAdd];
		} else {
			//Then add this one
			[friendList addObject:firstUser];
		}
	}
	return friendList;
}

//Query for messages between user1 and user2
- (NSArray *) queryForMessagesBetweenSYNCH:(PFUser *)userOne andUser:(PFUser *)userTwo
{
	//All received messages
	PFQuery *sentMessages = [PFQuery queryWithClassName:@"Message"];
	[sentMessages whereKey:@"fromUser" equalTo:userOne];
	[sentMessages whereKey:@"toUser" equalTo:userTwo];
	//All sent messagtes
	PFQuery *receivedMessages = [PFQuery queryWithClassName:@"Message"];
	[receivedMessages whereKey:@"toUser" equalTo:userOne];
	[receivedMessages whereKey:@"fromUser" equalTo:userTwo];
	//Combine them and sort by date created
	PFQuery *combine = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:receivedMessages,sentMessages, nil]];
	[combine orderByDescending:@"createdAt"];
	//Get objects
	NSArray *obj = [NSArray arrayWithArray:[combine findObjects]];
	return obj;
}

- (NSArray *) queryForMessagesBetweenSYNCH:(PFUser *)userOne andUser:(PFUser *)userTwo newerThan:(NSDate *)date
{
	//All received messages
	PFQuery *sentMessages = [PFQuery queryWithClassName:@"Message"];
	[sentMessages whereKey:@"fromUser" equalTo:userOne];
	[sentMessages whereKey:@"toUser" equalTo:userTwo];
	//All sent messagtes
	PFQuery *receivedMessages = [PFQuery queryWithClassName:@"Message"];
	[receivedMessages whereKey:@"toUser" equalTo:userOne];
	[receivedMessages whereKey:@"fromUser" equalTo:userTwo];
	//Combine them and sort by date created
	PFQuery *combine = [PFQuery orQueryWithSubqueries:[NSArray arrayWithObjects:receivedMessages,sentMessages, nil]];
	[combine orderByDescending:@"createdAt"];
	[combine whereKey:@"dateCreated" greaterThan:date];
	
	//Get objects
	NSArray *obj = [NSArray arrayWithArray:[combine findObjects]];
	return obj;
}

//Find friendLink for two given users
- (NSArray *) findFriendLinkSYNCH:(PFUser *)userOne andUser:(PFUser *)userTwo
{
	//Find the link, have to check both directions
	PFQuery *peopleToDelete = [PFQuery queryWithClassName:@"friendLink"];
	[peopleToDelete whereKey:@"personOne" equalTo:userOne];
	[peopleToDelete whereKey:@"personTwo" equalTo:userTwo];
	PFQuery *peopleToDelete2 = [PFQuery queryWithClassName:@"friendLink"];
	[peopleToDelete2 whereKey:@"personTwo" equalTo:userOne];
	[peopleToDelete2 whereKey:@"personOne" equalTo:userTwo];
	
	//Combine the query
	NSArray *obj;
	//Find the one with an object
	if(peopleToDelete.countObjects!=0){
		obj = [NSArray arrayWithArray:[peopleToDelete findObjects]];
	} else {
		obj = [NSArray arrayWithArray:[peopleToDelete2 findObjects]];
	}
	return obj;
}

//Find nearby friends for a given location
- (NSArray *) findNearbyFriendsForSYNCH:(PFGeoPoint *)location
{
	PFQuery *locationQuery = [PFUser query];
	[locationQuery whereKey:@"location" nearGeoPoint:location withinKilometers:5.0];
	[locationQuery whereKey:@"type" notEqualTo:@"CARER"];
	NSArray *objects = [locationQuery findObjects];
	return [OBParseQueryManager contactsFromUsers:objects];
}

//==============================
// -- Assisted User methods ----
//==============================

//Changes an array of carers to PFuser;
- (NSArray *) carersFromCarerLinks:(NSArray *) carerLinks
{
	//Cool now we have a list of friendLinks, we need to go through each object, and for each object find the person who is not us.
	int max = carerLinks.count;
	NSMutableArray *carerList = [NSMutableArray array];
	for(int i=0; i<max; i++){
		PFObject *link = [carerLinks objectAtIndex:i];
		PFUser *carer = [link objectForKey:@"carer"];
		[carerList addObject:carer];
	}
	return carerList;
}

//Find list of carers for a user;
- (NSArray *) findCarersForSYNCH:(PFUser *)assistedPerson
{
	PFQuery *carerQuery = [PFQuery queryWithClassName:@"carerLink"];
	[carerQuery whereKey:@"assistedPerson" equalTo:assistedPerson];
	NSArray *objects = [carerQuery findObjects];
	NSArray *carers = [self carersFromCarerLinks:objects];
	return [OBSYNCHParseQueryManager carerContactsFromUsers:carers];
}

//Find the single carer for a user;
- (OBCarer *) findCarerForSYNCH:(PFUser *)assistedPerson
{
	PFQuery *carerQuery = [PFQuery queryWithClassName:@"carerLink"];
	[carerQuery whereKey:@"assistedPerson" equalTo:assistedPerson];
	NSArray *objects = [carerQuery findObjects];
	NSArray *carers = [self carersFromCarerLinks:objects];
	return [[OBSYNCHParseQueryManager carerContactsFromUsers:carers]objectAtIndex:0];
}

//Find the requests available for a carer;
- (NSArray *) findCarerRequestsForSYNCH:(PFUser *)carer
{
	PFQuery *requestsQuery = [PFQuery queryWithClassName:@"carerRequests"];
	[requestsQuery whereKey:@"carer" equalTo:carer];
	PFObject *object = [requestsQuery getFirstObject];
	[object fetch];
	NSArray *requests = [object objectForKey:@"carerRequests"];
	return requests;
}

//Find the requests sent for a carer;
- (NSArray *) findSentCarerRequestsForSYNCH:(PFUser *)user
{
	PFQuery *requestsQuery = [PFQuery queryWithClassName:@"carerRequests"];
	[requestsQuery whereKey:@"assistedPerson" equalTo:user];
	PFObject *object = [requestsQuery getFirstObject];
	[object fetch];
	NSArray *requests = [object objectForKey:@"sentRequestsFromAP"];
	return requests;
}


//======================
// -- Carer methods ----
//======================

//Find the clients for a given carer
- (NSArray *) findClientsForSYNCH:(PFUser *)carer
{
	PFQuery *clientQuery = [PFQuery queryWithClassName:@"carerLink"];
	[clientQuery whereKey:@"carer" equalTo:carer];
	NSArray *objects = [clientQuery findObjects];
	NSArray *aps = [OBSYNCHParseQueryManager apsFromCarerLinks:objects];
	return [OBSYNCHParseQueryManager assistedContactsFromUsers:aps];
}

//Find nearby carers for a given location
- (NSArray *) findNearbyCarersForSYNCH:(PFGeoPoint *)location
{
	PFQuery *locationQuery = [PFUser query];
	[locationQuery whereKey:@"location" nearGeoPoint:location withinKilometers:5.0];
	[locationQuery whereKey:@"type" equalTo:@"CARER"];
	NSArray *objects= [locationQuery findObjects];
	return [OBParseQueryManager contactsFromUsers:objects];
}

//Find the OBCarerRequests sent to a carer from an AP.
- (NSArray *) findRequestsForCarerSYNCH:(PFUser *)carer fromUser:(PFUser *)user
{
	PFQuery *requestsQuery = [PFQuery queryWithClassName:@"carerRequests"];
	[requestsQuery whereKey:@"assistedPerson" equalTo:user];
	[requestsQuery whereKey:@"carer" equalTo:carer];
	
	NSArray *objects = [requestsQuery findObjects];
	NSMutableArray *requests = [NSMutableArray array];
	for (PFObject *o in objects) {
		[requests addObject:[OBCarerRequest createRequestFromPFCarerRequest:o]];
	}
	return requests;
}

//Find emergency contacts for a user
- (NSArray *) findEmergencyContactsForSYNCH:(OBAssistedContact *)contact
{
	PFQuery *emergencyQuery = [PFQuery queryWithClassName:@"emergencyContact"];
	[emergencyQuery whereKey:@"userRelation" equalTo:contact.objectsUser];
	NSArray *objects = [emergencyQuery findObjects];
	return [OBSYNCHParseQueryManager emergencyContactsFromObjects:objects];
}

- (OBEmergencyRequest *) retrieveEmergencyRequestFor:(OBAssistedContact *)contact
{
	PFQuery *query = [PFQuery queryWithClassName:@"emergencyRequest"];
	[query whereKey:@"assistedUser" equalTo:contact.objectsUser];
	OBEmergencyRequest *emergencyContact = [OBEmergencyRequest getEmergencyRequestFrom:[query getFirstObject]];
	return emergencyContact;
}

//=========================
// -- Utitlity methods ----
//=========================

// Get the PFUsers from an array of carer links
+(NSArray *) apsFromCarerLinks:(NSArray *)carerLinks
{
	NSMutableArray *aps = [NSMutableArray array];
	for (PFObject *link in carerLinks) {
		PFUser *ap = [link objectForKey:@"assistedPerson"];
		[aps addObject:ap];
	}
	return aps;
}

//Change an array of users into an array of obassistedcontacts
+ (NSArray *) carerContactsFromUsers:(NSArray *) users
{
	NSMutableArray *newArray = [NSMutableArray array];
	for(PFUser *u in users){
		OBCarer *newContact = [[OBCarer alloc] initWithUser:u];
		[newArray addObject:newContact];
	}
	return newArray;
}

//Change an array of users into an array of obcontacts
+ (NSArray *) contactsFromUsers:(NSArray *) users
{
	NSMutableArray *newArray = [NSMutableArray array];
	for(PFUser *u in users){
		[newArray addObject:[[OBContact alloc] initWithUser:u]];
	}
	return newArray;
}

//Change an array of users into an array of obassistedcontacts
+ (NSArray *) assistedContactsFromUsers:(NSArray *) users
{
	NSMutableArray *newArray = [NSMutableArray array];
	for(int i=0; i<users.count; i++){
		OBAssistedContact *newContact = [[OBAssistedContact alloc] initWithUser:[users objectAtIndex:i]];
		[newArray addObject:newContact];
	}
	return newArray;
}

//Find emergency contacts
+ (NSArray *) emergencyContactsFromObjects:(NSArray *)users
{
	NSMutableArray *newArray = [NSMutableArray new];
	//Now we need to go through each friend request and extract the user from it
	for(int i=0; i<users.count; i++){
		OBEmergencyContact *newContact = [OBEmergencyContact getEmergencyContactFrom:[users objectAtIndex:i]];
		[newArray addObject:newContact];
	}
	return newArray;
}

+ (void) findAndDeleteFriendRequestFromSYNCH:(PFUser *)user to:(PFUser *)recipient
{
	PFQuery *findRequests = [PFQuery queryWithClassName:@"friendRequest"];
	[findRequests whereKey:@"toUser" equalTo:recipient];
	[findRequests whereKey:@"fromUser" equalTo:user];
	[[findRequests getFirstObject] delete];
}


@end



