//
// ClientRequestViewController.h
// OldBook
//
// Created by Mat on 9/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBAssistedContact.h"
#import "OBCarerUser.h"
@interface ClientRequestViewController : UIViewController

@property (strong, nonatomic) OBAssistedContact *client;
@property (strong, nonatomic) NSArray *requests;
@property (strong, nonatomic) OBCarerUser *user;

@end
