//
// OBContactDetailViewViewController.m
// OldBook
//
// Created by Mat on 7/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBContactDetailViewViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AGPhotoBrowserView.h"

@implementation OBContactDetailViewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self loadMessageInboxForUser];
	self.delegate = self;
	self.dataSource = self;
  
  
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(newMessage)
												 name:@"reloadMessage"
                        object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(messagesLoaded)
												 name:@"finishedLoadingMessages"
											   object:nil];

	
	
    
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(sendPictureMessage)
												 name:@"messagePictureTaken"
                                               object:nil];
}

- (void) loadMessageInboxForUser
{
    //Initialise the mailbox
    self.finishedLoading=NO;
    [self.currentUser getMessageinboxForContact:self.friendUser withCallback:^(OBMessageInbox *inbox, NSError *error) {
        self.inbox = inbox;
        [self.tableView reloadData];
        [self scrollToBottomAnimated:YES];
        self.title = [self.inbox.messagesTo firstName];
        
    }];
}

- (void) viewDidAppear:(BOOL)animated {
	if([self.inbox.messages count]==0){
		[SVProgressHUD show];
	}
}

- (void) newMessage
{
  [JSMessageSoundEffect playMessageReceivedSound];
  [self.tableView reloadData];
  [self scrollToBottomAnimated:YES];
}

- (void) messagesLoaded
{
	[self.tableView reloadData];
	[self scrollToBottomAnimated:YES];
	[SVProgressHUD dismiss];
}

- (void) sendPictureMessage
{
    [self.inbox createAndSendPictureMessageWith:self.camHelper.nonCircular withProgress:nil];
	[JSMessageSoundEffect playMessageSentSound];
	[self.tableView reloadData];
	[self finishSend];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return self.inbox.messages.count;
}

#pragma mark - Messages view delegate
- (void)sendPressed:(UIButton *)sender withText:(NSString *)text
{
  [self.inbox createAndSendTextMessageWith:text withProgress:nil];
  [JSMessageSoundEffect playMessageSentSound];
  [self.tableView reloadData];
  [self finishSend];
}

- (JSBubbleMessageType)messageTypeForRowAtIndexPath:(NSIndexPath *)indexPath
{
//  OBMessage *msg = [self.inbox getMessageAtIndex:(self.inbox.messages.count - indexPath.row)-1];
//  // We need to see which side to put it on, i.e. did we send it.
//  if([msg.toUser.objectId isEqualToString:[PFUser currentUser].objectId]){
//    return JSBubbleMessageTypeIncoming;
//  } else {
//    return JSBubbleMessageTypeOutgoing;
//  }
  OBMessage *msg = [self.inbox getMessageAtIndex:(self.inbox.messages.count - indexPath.row)-1];
  // We need to see which side to put it on, i.e. did we send it.
  if(!msg.outgoing){
    return JSBubbleMessageTypeIncoming;
  } else {
    return JSBubbleMessageTypeOutgoing;
  }
}


- (JSBubbleMessageStyle)messageStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return JSBubbleMessageStyleSquare;
}

- (JSMessagesViewTimestampPolicy)timestampPolicy
{
  return JSMessagesViewTimestampPolicyEveryFive;
}

- (JSMessagesViewAvatarPolicy)avatarPolicy
{
  return JSMessagesViewAvatarPolicyBoth;
}

- (JSAvatarStyle)avatarStyle
{
  return JSAvatarStyleCircle;
}

- (void) didSelectMessageAtIndexPath:(NSIndexPath *)indexPath {
	OBMessage *msg = [self.inbox getMessageAtIndex:(self.inbox.messages.count - indexPath.row)-1];
	//Check if it is a text message
	if(![msg.messageType isEqualToString:@"text"]){
		self.selectedImage = [UIImage imageWithData:msg.messageContents];
		AGPhotoBrowserView *view = [AGPhotoBrowserView new];
		view.delegate = self;
		view.dataSource = self;
		[view show];
	}
}

#pragma mark - Messages view data source
- (NSString *)textForRowAtIndexPath:(NSIndexPath *)indexPath
{
  OBMessage *msg = [self.inbox getMessageAtIndex:(self.inbox.messages.count - indexPath.row)-1];
  //Check if it is a text message
           NSString *string;
  if([msg.messageType isEqualToString:@"text"]){
    string = [[NSString alloc] initWithData:msg.messageContents encoding:NSUTF8StringEncoding];
  } else {
    string = @"Picture message";
  }
  return string;
}

- (NSDate *)timestampForRowAtIndexPath:(NSIndexPath *)indexPath
{
  OBMessage *msg = [self.inbox getMessageAtIndex:(self.inbox.messages.count-indexPath.row)-1];
  return msg.sent;
}

- (UIImage *)avatarImageForIncomingMessage
{
  return self.inbox.toUserPicture;
}

- (UIImage *)avatarImageForOutgoingMessage
{
  return self.inbox.fromUserPicture;
}

- (NSInteger) numberOfPhotosForPhotoBrowser:(AGPhotoBrowserView *)photoBrowser {
	return 1;
}

- (UIImage *) photoBrowser:(AGPhotoBrowserView *)photoBrowser imageAtIndex:(NSInteger)index {
	return self.selectedImage;
}

- (void) photoBrowser:(AGPhotoBrowserView *)photoBrowser didTapOnDoneButton:(UIButton *)doneButton {
	[photoBrowser hideWithCompletion:nil];
}

@end
