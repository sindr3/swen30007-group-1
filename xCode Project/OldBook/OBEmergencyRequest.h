//
// OBEmergencyRequest.h
// OldBook
//
// Created by Mat on 19/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "OBCarer.h"
#import "OBAssistedContact.h"

@class OBAssistedUser;

//An object that holds the PFObject for an emergency request, as well as that update based
//on whether the request has been sent
@interface OBEmergencyRequest : NSObject

//The user that initiated the emergency request
@property (strong, nonatomic) PFUser *user;
@property (strong, nonatomic) OBAssistedContact *requestingContact;
//The server side object
@property (strong, nonatomic) PFObject *serverRequest;

//Function for creating emergency requests
+ (OBEmergencyRequest *) makeEmergencyRequestFor:(OBAssistedUser *)contact;
+ (OBEmergencyRequest *) getEmergencyRequestFrom:(PFObject *)object;

//Cancel emergency request
- (void) cancelRequest;


@end

