//
// LogInViewController.h
// OldBook
//
// Created by Mat on 7/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogInViewController : UIViewController 

@property(nonatomic, weak) IBOutlet UIButton *loginButton;

//The user details to log in
@property(nonatomic, strong) NSString *username;
@property(nonatomic, strong) NSString *password;

//Log In Functions
- (IBAction)checkLoginValid:(id)sender;

@end
