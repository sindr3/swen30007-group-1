//
// OBNotificationHandler.h
// OldBook
//
// Created by Mat on 26/09/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OBUser.h"

@interface OBNotificationHandler : NSObject

//Our only two publicly acessible methods to handle notifications
//in all possible scenarios

//Will handle a notification received whilst currently running.
//Posts a local notification to be handled anywhere required.
+ (void) handleNotificationWithinApp: (NSDictionary *) notificationData inApp:(UIViewController *) app andStoryboard:(UIStoryboard *) storyboard;

//Our method for handling opening the app from a notification. Will
//return the identifier of the view we need to swap to. 
+ (NSString *) getViewFromOpenWithNotification: (NSDictionary *) notificationData inApp:(UIViewController *) app andStoryboard:(UIStoryboard *) storyboard;

@end
