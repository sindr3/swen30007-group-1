//
// SignUpViewController-iPad.m
// OldBook
//
//  Created by Sindre on 18/08/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import "SignUpViewController-iPad.h"
#import "OBCameraViewControllerHelper.h"
#import "OBSearchQueryManager.h"
#import "OBUser.h"
#import "AppDelegate.h"

@interface SignUpViewController_iPad () <UIImagePickerControllerDelegate, UIPopoverControllerDelegate, UITextFieldDelegate>

// UI items
@property (strong, nonatomic) IBOutlet UIButton* doneButton;
@property (strong, nonatomic) IBOutlet UIImageView* userImage;

// Personal details UI items
@property(nonatomic, weak) IBOutlet UITextField* firstNameField;
@property(nonatomic, weak) IBOutlet UITextField* lastNameField;
@property(nonatomic, weak) IBOutlet UITextField* birthdayField;

// Account details UI items
@property(nonatomic, weak) IBOutlet UITextField* usernameField;
@property(nonatomic, weak) IBOutlet UITextField* emailField;
@property(nonatomic, weak) IBOutlet UITextField* passwordField;
@property(nonatomic, weak) IBOutlet UITextField* passwordRepeatedField;

// Popover
@property (nonatomic, strong) UIDatePicker* datePicker;
@property (nonatomic, strong) UIPopoverController* pcDatePicker;

// Selected date
@property(nonatomic, weak) NSDate* birthdate;

// Camera view controller helper
@property (strong, nonatomic) OBCameraViewControllerHelper* cameraView;
@property (strong, nonatomic) OBSearchQueryManager* queryManager;

@end

@implementation SignUpViewController_iPad

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.doneButton setEnabled:NO];
    
    self.queryManager = [OBSearchQueryManager getSearchQueryManager];
    [self initialiseTextFieldEventListeners];
    self.pcDatePicker = [self initaliseDatePickerPopover];
    self.datePicker = self.pcDatePicker.contentViewController.view.subviews[0];
    self.cameraView = [[OBCameraViewControllerHelper alloc] initWithImageDest:self.userImage inView:self withNotifier:@"finishSignUpPic"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Methods for checking the text fields
- (BOOL)allFieldsNonEmpty
{
    BOOL done = [self.firstNameField.text length] != 0 &
        [self.lastNameField.text length] != 0 &
        [self.birthdayField.text length] != 0 &
        [self.usernameField.text length] != 0 &
        [self.passwordField.text length] != 0 &
        [self.passwordRepeatedField.text length] != 0 &
        [self.emailField.text length] != 0;
    return done;
}

- (IBAction)didTappedBackground:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:YES];
}

- (BOOL)validateTextFields
{
    // Is the email valid?
    if ( ![OBSearchQueryManager NSStringIsValidEmail:[self.emailField text]] ){
        UIAlertView *invalidEmailAlert = [[UIAlertView alloc] initWithTitle:@"Invalid Email"
                                                                    message:@"The email address you entered is invalid."
                                                                   delegate:nil
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
        [invalidEmailAlert show];
        return false;
    }
    
    // Do the passwords match?
    if ( ![[self.passwordField text] isEqualToString:[self.passwordRepeatedField text]] ) {
        UIAlertView *inconsistentPassowrdAlert = [[UIAlertView alloc] initWithTitle:@"Inconsistent password"
                                                                            message:@"The passwords do not match."
                                                                           delegate:nil
                                                                  cancelButtonTitle:@"OK"
                                                                  otherButtonTitles:nil, nil];
        [inconsistentPassowrdAlert show];
        
        // Set password fields to be empty
        [self.passwordField setText:@""];
        [self.passwordRepeatedField setText:@""];
        
        return false;
    }

    // If all the statements above fail, the fields are valid
    return true;
}

#pragma mark View interaction methods

- (IBAction)selectPhoto:(id)sender
{
    [self.cameraView selectPhoto:sender];
}

- (IBAction)signupButtonPressed:(id)sender
{
    [self.view endEditing:YES];
    if ([self validateTextFields]) {
        [self signUpUser];
    }
}

- (IBAction)cancelButtonTapped
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown;
}

#pragma mark Methods for signing up
- (void)signUpUser
{
    [SVProgressHUD showWithStatus:@"Creating Account"];
    OBUser *temp =  [OBUser createTempUserFor:[self.usernameField text]
                                  andPassword:[self.passwordField text]
                                     andEmail:[self.emailField text]
                                  andBirthday:[self.datePicker date]
                                 andFirstName:[self.firstNameField text]
                                  andLastName:[self.lastNameField text]];
    
    // Send to server with completion block
    [temp createNewUserWithProfilePicture:[self.cameraView profilePicture] andCompletion:^(BOOL succeeded, NSError *error) {
        [SVProgressHUD dismiss];
        if(!error){
            // registered with server, sign user in
            [self changeUserView];
        } else {
            // Something went wrong
            NSString *errorMessage = [[error userInfo] objectForKey:@"error"];
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                 message:errorMessage
                                                                delegate:nil
                                                       cancelButtonTitle:@"OK"
                                                       otherButtonTitles:nil];
            [errorAlert show];
        }
    }];
}


- (void)changeUserView
{
    AppDelegate* app = [AppDelegate getCurrentAppDelegate];
    NSString* viewControllerName;
    
    if([OBUser getCurrentUser] ){
        NSString *userType = [OBUser getCurrentUserType];
        
        if([userType isEqualToString:@"SOCIAL"]){
            viewControllerName = @"ROOTSOCIALVIEW";
        }else if([userType isEqualToString:@"ASSISTED"]){
            viewControllerName = @"ROOTAPVIEW";
        }else if([userType isEqualToString:@"CARER"]){
            viewControllerName = @"ROOTCARERVIEW";
        }
    }
    
    [app changeRootViewController:viewControllerName];
}

#pragma mark Text Field Delegate Methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField==[self firstNameField]) {
        [self.lastNameField becomeFirstResponder];
    } else if (textField == [self lastNameField]) {
        [self.lastNameField resignFirstResponder];
        [self showDatePickerPopover:[self birthdayField]];
    } else if (textField == [self birthdayField]) {
        [[self usernameField] becomeFirstResponder];
    } else if (textField == [self usernameField]) {
        [[self emailField] becomeFirstResponder];
    } else if (textField == [self emailField]) {
        [[self passwordField] becomeFirstResponder];
    } else if (textField == [self passwordField]) {
        [[self passwordRepeatedField] becomeFirstResponder];
    } else if (textField == [self passwordRepeatedField]) {
        [[self passwordRepeatedField] resignFirstResponder];
        if ([self allFieldsNonEmpty]) {
            [self signUpUser];
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField == self.birthdayField){
        // Birthday field presents a date picker popover
        [self showDatePickerPopover:self.birthdayField];
        return NO;
    } else {
        [self.pcDatePicker dismissPopoverAnimated:YES];
        return YES;
    }
}

- (void)initialiseTextFieldEventListeners
{
    [self.firstNameField addTarget:self action:@selector(textFieldValueChanged) forControlEvents:UIControlEventEditingChanged];
    [self.lastNameField addTarget:self action:@selector(textFieldValueChanged) forControlEvents:UIControlEventEditingChanged];
    [self.birthdayField addTarget:self action:@selector(textFieldValueChanged) forControlEvents:UIControlEventEditingChanged];
    [self.usernameField addTarget:self action:@selector(textFieldValueChanged) forControlEvents:UIControlEventEditingChanged];
    [self.passwordField addTarget:self action:@selector(textFieldValueChanged) forControlEvents:UIControlEventEditingChanged];
    [self.passwordRepeatedField addTarget:self action:@selector(textFieldValueChanged) forControlEvents:UIControlEventEditingChanged];
}

- (void)textFieldValueChanged
{
    if ( [self allFieldsNonEmpty] ) {
        [self.passwordRepeatedField setReturnKeyType:UIReturnKeyGo];
        [self.doneButton setEnabled:YES];
    } else {
        [self.passwordRepeatedField setReturnKeyType:UIReturnKeyDefault];
        [self.doneButton setEnabled:NO];
    }
}


#pragma mark Popover Delegate Methods
-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.birthdate = [self.datePicker date];
    NSString *date = [NSDateFormatter localizedStringFromDate:self.birthdate
                                                 dateStyle:NSDateFormatterLongStyle
                                                 timeStyle:NSDateFormatterNoStyle];
    self.birthdayField.text = date;
    [self.usernameField becomeFirstResponder];
}

- (UIPopoverController *)initaliseDatePickerPopover
{
    UIDatePicker *picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 320, 216)];     // date picker in frame
    picker.datePickerMode = UIDatePickerModeDate; // Displays month, day, and year depending on the locale setting (e.g. November | 15 | 2007)
    
    // Set the initial date and the maximum date
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear fromDate:[NSDate date]];
    [components setYear:components.year - 50];
    [components setMonth:1];
    [components setDay:1];
    NSDate *defaultDate = [[NSCalendar currentCalendar] dateFromComponents:components];
    
    [picker setMaximumDate:[NSDate date]];      // Maximum date, today
    [picker setDate:defaultDate];               // Date the date picker starts with
    
    UIView *pickerView = [[UIView alloc] init];         // The view the picker goes into
    [pickerView addSubview:picker];                     // Adds the date picker to the view
    
    UIViewController *pickerViewController = [[UIViewController alloc] init];
    pickerViewController.view = pickerView;                   // Connects the view controller and the view
    pickerViewController.preferredContentSize = CGSizeMake(320, 216);
    
    // popover controller which contains the date picker view controller
    UIPopoverController *popoverController = [[UIPopoverController alloc] initWithContentViewController:pickerViewController];
    
    // set the delegate to be the controller of the main view, to access the data from the picker
    popoverController.delegate = self;   // Not too sure about this
    
    return popoverController;
}

-(void)showDatePickerPopover:(UITextField *)sender
{
    [self.pcDatePicker presentPopoverFromRect:sender.frame
                                              inView:self.view
                            permittedArrowDirections:UIPopoverArrowDirectionLeft
                                            animated:YES];
}

@end
