//
// SocialUserViewController.m
// OldBook
//
// Created by Mat on 8/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "SocialUserViewController.h"
#import "OBParseQueryManager.h"
#import "OBCameraViewControllerHelper.h"
#import <QuartzCore/QuartzCore.h>


@interface SocialUserViewController () <UIAlertViewDelegate>

@property (strong, nonatomic) UIAlertView *logOutAlert;
@property (strong, nonatomic) IBOutlet UIImageView *userImage;
@property (strong, nonatomic) IBOutlet UILabel *userFirstName;
@property (strong, nonatomic) IBOutlet UILabel *welcomeBackLabel;
@property (weak, nonatomic) IBOutlet UIButton *emergencyButton;
@property (weak, nonatomic) IBOutlet UIButton *friendRequestButton;
@property (assign,nonatomic) CGPoint highWelcome;
@property (assign,nonatomic) CGPoint lowName;
@property (strong, nonatomic) OBCameraViewControllerHelper *cameraHelper;


@end

@implementation SocialUserViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.highWelcome = CGPointMake(200, 35);
	self.lowName = CGPointMake(200, 68);
	[self.friendRequestButton setHidden:YES];
	self.currentUser = [OBUser getCurrentUser];
	self.cameraHelper = [[OBCameraViewControllerHelper alloc] initWithImageDest:self.userImage inView:self withNotifier:@"socialProfilePic"];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(updateProfilePic)
												 name:@"socialProfilePic"
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(newFriendRequests)
												 name:@"newFriendRequest"
											   object:nil];
}


- (void) updateProfilePic {
  [[OBUser getCurrentUser] setProfilePicture:self.cameraHelper.profilePicture showProgress:YES];
}

- (void) viewDidLayoutSubviews
{
//  if([self.userFirstName.text length]!=0){
//    [self.welcomeBackLabel setCenter:self.highWelcome];
//    [self.userFirstName setCenter:self.lowName];
//  }
}

- (void) viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	//Set the userName logo
	if([self.userFirstName.text length]==0){
		OBContact *contact = self.currentUser.userDetails;
		
		[self.userFirstName setAlpha:0];
		[self.userFirstName setText:[contact firstName]];
		[UIView animateWithDuration:0.5 animations:^{
			[self.welcomeBackLabel setCenter:self.highWelcome];
			[self.userFirstName setCenter:self.lowName];
			[self.userFirstName setAlpha:1];
		}];
		
		[UIView animateWithDuration:1 animations:^{
			[self.userImage setAlpha:0];
		}];
		[self.userImage setImage:[contact profilePicture]];
		[UIView animateWithDuration:0.2 animations:^{
			[self.userImage setAlpha:1];
		}];
		
	}
  [[self friendRequestButton] setHidden:![self checkForFriendRequests]];

}

- (void) newFriendRequests {
	[[self friendRequestButton] setHidden:NO];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

- (IBAction)logOut:(id)sender
{
  self.logOutAlert = [[UIAlertView alloc] initWithTitle:@"Log Out?"
                         message:@"Are you sure you want to log out?"
                         delegate:self
                    cancelButtonTitle:@"NO"
                    otherButtonTitles:@"YES",nil
            ];
  [self.logOutAlert show];  return;
}

- (IBAction)changeProfilePic:(id)sender {
  [self.cameraHelper selectPhoto:sender];
}


#pragma mark - UIAlertViewDelegate

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  if(buttonIndex != [alertView cancelButtonIndex]){
    [OBUser logOut];
    [self performSegueWithIdentifier:@"BACKTOLOGIN" sender:self];
    [self resignFirstResponder];
    return;
  } else {
    [self resignFirstResponder];
  }
  
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  [SVProgressHUD dismiss];
  if([segue.identifier isEqualToString:@"toContactDetails"]){
    OBContactDetailViewViewController *destination = (OBContactDetailViewViewController *) [segue destinationViewController];
    destination.friendUser = self.userSelected;
  }
}


- (BOOL) checkForFriendRequests
{
  return [[OBUser getCurrentUser] userHasFriendRequests];
}

- (void) loadUserDetailsFromServer
{
//  if([self.userFirstName.text length]!=0){
//    [self.welcomeBackLabel setCenter:self.highWelcome];
//    [self.userFirstName setCenter:self.lowName];
//  } else {
//    OBContact *contact = [[OBUser getCurrentUser] userDetails];
//    
//    [self.userFirstName setText:[contact firstName]];
//    [self.userFirstName setAlpha:0];
//    [UIView animateWithDuration:0.5 animations:^{
//      [self.welcomeBackLabel setCenter:self.highWelcome];
//			[self.userFirstName setCenter:self.lowName];
//      [self.userFirstName setAlpha:1];
//    }];
//    
//    UIImage *dummyImage = [contact profilePicture];
//		[UIView animateWithDuration:0.2 animations:^{
//			[self.userImage setAlpha:0];
//		}];
//		[self.userImage setImage:dummyImage];
//		[UIView animateWithDuration:0.2 animations:^{
//			[self.userImage setAlpha:1];
//		}];
//  }
	
//	[super viewDidAppear:animated];
//	//Set the userName logo
//	if([self.userFirstName.text length]==0){
//		OBContact *contact = self.currentUser.userDetails;
//		
//		[self.userFirstName setAlpha:0];
//		[self.userFirstName setText:[contact firstName]];
//		[UIView animateWithDuration:0.5 animations:^{
//			[self.welcomeBackLabel setCenter:self.highWelcome];
//			[self.userFirstName setCenter:self.lowName];
//			[self.userFirstName setAlpha:1];
//		}];
//		
//		[UIView animateWithDuration:1 animations:^{
//			[self.userImage setAlpha:0];
//		}];
//		[self.userImage setImage:[contact profilePicture]];
//		[UIView animateWithDuration:0.2 animations:^{
//			[self.userImage setAlpha:1];
//		}];
//		
//	}

  
}





@end
