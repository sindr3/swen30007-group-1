//
// OBCameraViewControllerHelper.h
// OldBook
//
// Created by Mat on 27/09/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OBCameraViewControllerHelper : NSObject

//For creating the view to pick images
@property (weak, nonatomic) UIImage *profilePicture;
@property (weak, nonatomic) UIImage *nonCircular;

//Our functions
- (IBAction)selectPhoto:(id)sender;;
- (id) initWithImageDest:(UIImageView *)destinationView inView:(UIViewController *) viewController withNotifier:(NSString *)notify;


@end
