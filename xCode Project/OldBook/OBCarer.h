//
// OBCarer.h
// OldBook
//
// Created by Mat on 17/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//
#import "OBContact.h"
#import <Foundation/Foundation.h>
#import "OBParseQueryManager.h"


@interface OBCarer : OBContact

//The requests a carer can provide for local storage abailites
@property (strong,nonatomic) NSMutableArray *requestsAvailable;

//The getter method that instantiates the requestsAvailable
+(OBCarer *) getCarerForUser:(PFUser *)carer;

@end
