//
// OBCameraViewControllerHelper.m
// OldBook
//
// Created by Mat on 27/09/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBCameraViewControllerHelper.h"
#import <AVFoundation/AVFoundation.h>
#import "UIImage-Extensions.h"

@interface OBCameraViewControllerHelper () <UIImagePickerControllerDelegate, UITextFieldDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UITableViewDelegate>

//Contversion macros
CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;};
CGFloat RadiansToDegrees(CGFloat radians) {return radians * 180/M_PI;};
@property (nonatomic, weak) IBOutlet UIToolbar *toolBar;
@property (nonatomic) IBOutlet UIView *overlayView;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *takePictureButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *startStopButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *delayedPhotoButton;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *doneButton;

//Images taken by the camera
@property (strong, nonatomic) UIImagePickerController *imagePickerController;
@property (strong, nonatomic) NSMutableArray *capturedImages;
//For our action sheet
@property (strong, nonatomic) UIActionSheet *actionSheet;
@property (weak, nonatomic) UIImageView *imageToPick;
@property (weak, nonatomic) UIViewController *presentingView;
@property (strong, nonatomic) NSString *toNotify;


- (void) createActionSheet;
- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex;
- (IBAction)showImagePickerForCamera:(id)sender;
- (IBAction)showImagePickerForPhotoPicker:(id)sender;
- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType;
#pragma mark - Toolbar actions
- (IBAction)done:(id)sender;
- (IBAction)takePhoto:(id)sender;
- (void)finishAndUpdate;
// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info;
- (void)imagePickerControllerDidCancel:(UIImagePickerController *) picker;
- (UIImage*) maskImage:(UIImage *)image ;
- (void) navigationController: (UINavigationController *) navigationController willShowViewController: (UIViewController *) viewController animated: (BOOL) animated;
- (void) showCamera: (id) sender;
- (void) showLibrary: (id) sender;

@end

@implementation OBCameraViewControllerHelper

- (id) initWithImageDest:(UIImageView *)destinationView inView:(UIViewController *) viewController withNotifier:(NSString *)notify{
  self = [super init];
  self.capturedImages = [[NSMutableArray alloc] init];
  self.imageToPick = destinationView;
  self.presentingView = viewController;
  [self createActionSheet];
  self.toNotify = notify;
  return self;
}

- (IBAction)selectPhoto:(id)sender {
  
  if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
    [self showImagePickerForPhotoPicker:self];
  } else {
    [self.actionSheet showInView:self.presentingView.view];
  }
}

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex {
  if(buttonIndex==0){
    [self showImagePickerForCamera:self];
  } else if(buttonIndex==1){
    [self showImagePickerForPhotoPicker:self];
  }
}

- (void) createActionSheet {
  self.actionSheet = [[UIActionSheet new] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera", @"Photo Library", nil];
}


- (IBAction)showImagePickerForCamera:(id)sender
{
  [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera];
}


- (IBAction)showImagePickerForPhotoPicker:(id)sender
{
  [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
}


- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
  if (self.imageToPick.isAnimating)
  {
    [self.imageToPick stopAnimating];
  }
  
  if (self.capturedImages.count > 0)
  {
    [self.capturedImages removeAllObjects];
  }
  
  UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
  imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
  imagePickerController.sourceType = sourceType;
  imagePickerController.delegate = self;
  imagePickerController.allowsEditing = YES;
  
  if (sourceType == UIImagePickerControllerSourceTypeCamera)
  {
    imagePickerController.showsCameraControls = YES;
  }
  
  self.imagePickerController = imagePickerController;
  [self.presentingView presentViewController:self.imagePickerController animated:YES completion:nil];
}


#pragma mark - Toolbar actions

- (IBAction)done:(id)sender
{
  [self finishAndUpdate];
}


- (IBAction)takePhoto:(id)sender
{
  [self.imagePickerController takePicture];
}


- (void)finishAndUpdate
{
  [self.presentingView dismissViewControllerAnimated:YES completion:NULL];
  
  if ([self.capturedImages count] > 0)
  {
    
    if ([self.capturedImages count] == 1)
    {
      // Camera took a single picture.
      //[self.imageToPick setImage:[self.capturedImages objectAtIndex:0]];
      UIImage *taken = [[self capturedImages] objectAtIndex:0];
      UIImage *masked = [self maskImage:taken];
      if(taken.imageOrientation == UIImageOrientationUp){
        self.imageToPick.image = masked;
        self.profilePicture = masked;
      } else {
        UIImage *rotated = [masked imageRotatedByDegrees:90];
        self.imageToPick.image = rotated;
        self.profilePicture = rotated;
      }
		
		self.nonCircular =taken;
      
      [[NSNotificationCenter defaultCenter] postNotificationName:self.toNotify object:self];
    }
    [self.capturedImages removeAllObjects];
  }
  self.imagePickerController = nil;
}

#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
  UIImage *image = [info valueForKey:UIImagePickerControllerEditedImage];
  [self.capturedImages addObject:image];  
  [self finishAndUpdate];
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
  [self.presentingView dismissViewControllerAnimated:YES completion:nil];
}


- (UIImage*) maskImage:(UIImage *)image {
  
  CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
  
  UIImage *maskImage = [UIImage imageNamed:@"mask.png"];
  CGImageRef maskImageRef = [maskImage CGImage];
  
  // create a bitmap graphics context the size of the image
  CGContextRef mainViewContentContext = CGBitmapContextCreate (NULL, maskImage.size.width, maskImage.size.height, 8, 0, colorSpace, kCGImageAlphaPremultipliedLast);
  
  if (mainViewContentContext==NULL)
    return NULL;
  
  CGFloat ratio = 0;
  
  ratio = maskImage.size.width/ image.size.width;
  
  if(ratio * image.size.height < maskImage.size.height) {
    ratio = maskImage.size.height/ image.size.height;
  }
  
  CGRect rect1 = {{0, 0}, {maskImage.size.width, maskImage.size.height}};
  CGRect rect2 = {{-((image.size.width*ratio)-maskImage.size.width)/2 , -((image.size.height*ratio)-maskImage.size.height)/2}, {image.size.width*ratio, image.size.height*ratio}};
  CGContextClipToMask(mainViewContentContext, rect1, maskImageRef);
  CGContextDrawImage(mainViewContentContext, rect2, image.CGImage);
  // Create CGImageRef of the main view bitmap content, and then
  // release that bitmap context
  CGImageRef newImage = CGBitmapContextCreateImage(mainViewContentContext);
  CGContextRelease(mainViewContentContext);
  UIImage *theImage = [UIImage imageWithCGImage:newImage];
  
  CGImageRelease(newImage);
  
  // return the imageh
  return theImage;
}

- (void) navigationController: (UINavigationController *) navigationController willShowViewController: (UIViewController *) viewController animated: (BOOL) animated {
  if (self.imagePickerController.sourceType == UIImagePickerControllerSourceTypePhotoLibrary && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
    UIBarButtonItem* button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(showCamera:)];
    viewController.navigationItem.rightBarButtonItems = [NSArray arrayWithObject:button];
    [viewController.navigationController.navigationBar setBarStyle:UIBarStyleDefault];
    [viewController.navigationController.navigationBar setTintColor:[UIColor blackColor]];
  } else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
    UIBarButtonItem* button = [[UIBarButtonItem alloc] initWithTitle:@"Library" style:UIBarButtonItemStylePlain target:self action:@selector(showLibrary:)];
    viewController.navigationItem.leftBarButtonItems = [NSArray arrayWithObject:button];
    viewController.navigationItem.title = @"Take Photo";
    viewController.navigationController.navigationBarHidden = NO; // important
    [viewController.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [viewController.navigationController.navigationBar setBarStyle:UIBarStyleBlackTranslucent];
  }
}

- (void) showCamera: (id) sender {
  self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
}

- (void) showLibrary: (id) sender {
  self.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
}
@end
