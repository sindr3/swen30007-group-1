//
// OBAssistedContact.h
// OldBook
//
// Created by Mat on 17/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBContact.h"

@class OBEmergencyRequest;
@class OBEmergencyContact;

@interface OBAssistedContact : OBContact

//Medical information that is specific to an assisted person
@property (strong,nonatomic) NSMutableArray *allergies;
@property (strong, nonatomic) NSMutableArray *bloodPressureReadings;
@property (strong, nonatomic) NSMutableArray *weightReadings;

//Next of kin contact details
@property (strong, nonatomic) NSMutableArray *nextOfKin;
@property (strong, nonatomic) NSMutableArray *medicalContacts;

//Factory methods
+ (OBAssistedContact *) getAssistedContactFromUser:(PFUser *) user;

//Sort the emergency contacts
+ (void) sortEmergencyContacts:(NSArray *)array withCallback:(void (^)(NSArray *nextOfKin, NSArray *medicalContacts))callback;

@end
