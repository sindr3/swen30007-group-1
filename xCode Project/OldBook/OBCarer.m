//
// OBCarer.m
// OldBook
//
// Created by Mat on 17/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBCarer.h"

@implementation OBCarer

+(OBCarer *) getCarerForUser:(PFUser *)carer
{
  //Create a new carer with all information in an ob contact
  OBCarer *newCarer = [[OBCarer alloc] initWithUser:carer];
  return newCarer;
}

@end
