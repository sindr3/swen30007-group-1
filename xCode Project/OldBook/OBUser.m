//
// OBUser.m
// OldBook
//
// Created by Mat on 15/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBUser.h"

@implementation OBUser

//Our consistent user across all requests.
static OBUser *currentUser =nil;

// Creates the OBUser given the data from Parse, is also responsible
// For asynchronously loading all information from the server into message inboxes and
// loading the friend list.
- (id) init
{
	self = [super init];
	self.currentParseUser = [PFUser currentUser];
	if(self.currentParseUser!=nil){
		//Create a contact with the user details
		self.userDetails = [[OBContact alloc] initWithUser:self.currentParseUser];
		
		//Initialise the query manager
		self.queryManager = [OBParseQueryManager getQueryManager];
		
		//Start downloading the friendlist
		[self.queryManager queryForFriendlistForUser:self.currentParseUser onCompletion:^(NSArray *objects, NSError *error) {
			if(!error) {
				[self performSelectorInBackground:@selector(backgroundFriendLoading:) withObject:objects];
			}
		}];
		
		
		//Set up the message inboxes;
		self.messageInboxes = [NSMutableDictionary dictionary];
	}
	return self;
}

- (void) backgroundFriendLoading:(NSArray *) objects {
	self.friendList = [NSMutableArray arrayWithArray:[OBParseQueryManager contactsFromUsers:objects]];
}

- (id) initWithMockObjectsMailbox:(OBParseQueryManager *) qm andUser:(PFUser *) user andDetails:(OBContact *) details andFriendlist:(NSMutableArray *) fl andMessages:(NSMutableDictionary *) inbox
{
	self = [super init];
	self.currentParseUser = user;
	if(self.currentParseUser!=nil){
		//Create a contact with the user details
		self.userDetails = details;
		//Initialise the query manager
		self.queryManager = qm;
		self.friendList = fl;
		//Set up the message inboxes;
		self.messageInboxes = inbox;
	}
	return self;
}

// Singleton access point, will check if the user has been created and if so
// just return the user, this will ensure that the user can only be accessed once
// and that things are persistant across views.
+ (id) getCurrentUser
{
	if(currentUser==nil){
		currentUser = [[OBUser alloc] init];
	}
	return currentUser;
}

// Creates a blank OBUser to use to signup.
+ (OBUser *) createTempUserFor:(NSString *) username andPassword:(NSString *)password andEmail:(NSString *)email andBirthday:(NSDate *)birthday andFirstName:(NSString *)firstName andLastName:(NSString *)lastName
{
	
	//Create the temp user
	OBUser *tempUser = [OBUser new];
	
	//Now we initialize what we know
	
	//Set up the PFUser for parse
	PFUser *newUser = [PFUser user];
	newUser.username = username;
	newUser.password = password;
	newUser.email = email;
	[newUser setObject:firstName forKey:@"firstName"];
	[newUser setObject:lastName forKey:@"lastName"];
	[newUser setObject:birthday forKey:@"birthday"];
	[newUser setObject:@"SOCIAL" forKey:@"type"];
	
	//Assign the link to parse
	tempUser.currentParseUser = newUser;
	return tempUser;
}

+ (NSString *) getCurrentUserType
{
	PFUser *currentUser = [PFUser currentUser];
	if(currentUser==nil){
		return @"NA";
	} else {
		OBUser *currentUser = [OBUser getCurrentUser];
		NSString *userType = [currentUser.currentParseUser objectForKey:@"type"];
		return userType;
	}
	return @"NA";
}

// Retrieves the friendlist as required, first checking for local updates then checking for additional friends.
// Will only need to be forced to update if a new friend is added.
- (void) updateFriendListWithCallback:(void(^)(NSArray *friendList, NSError *error))callback
{
	//Then we need to retrieve the friendlist
	[self.queryManager queryForFriendlistForUser:self.currentParseUser onCompletion:^(NSArray *objects, NSError *error) {
		if(!error) {
			//We have found the friends so add them to the local store.
			self.friendList = [NSMutableArray arrayWithArray:[OBParseQueryManager contactsFromUsers:objects]];
			//Then produce callback so that view can reload
			//and dismiss errors
			callback(self.friendList,error);
		} else {
			//Then we couldn't get the friendlist, this is an error
			callback(nil,error);
		}
	}];
}

// Retrieves the friendlist as required, first checking for local updates then checking for additional friends.
// Will only need to be forced to update if a new friend is added.
- (void) retrieveFriendlistWithCallback:(void(^)(NSArray *friendList, NSError *error))callback
{
	if(self.friendList==nil){
		//Then we need to retrieve the friendlist
		[self updateFriendListWithCallback:^(NSArray *friendList, NSError *error) {
			callback(friendList, error);
		}];
	} else {
		callback(self.friendList, nil);
	};
}

- (OBContact *) contactFromFriendLink:(PFObject *)friendLink
{
	PFUser *testuser = [friendLink objectForKey:@"personOne"];
	if(testuser==self.currentParseUser){
		PFUser *testTwo = [friendLink objectForKey:@"personTwo"];
		OBContact *newContact = [[OBContact alloc] initWithUser:testTwo];
		return newContact;
	} else {
		OBContact *newContact = [[OBContact alloc] initWithUser:testuser];
		return newContact;
	}
	
	return nil;
}

- (BOOL) userHasFriendRequests
{
	if ([self.friendRequests count]!=0) {
		return YES;
	} else {
		PFQuery *friendRequests = [PFQuery queryWithClassName:@"friendRequest"];
		[friendRequests whereKey:@"toUser" equalTo:[self currentParseUser]];
		[friendRequests countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
			if(number!=0){
				[[NSNotificationCenter defaultCenter] postNotificationName:@"newFriendRequest" object:nil];
			}
		}];
	}
	
	return NO;
}

- (void) sendFriendRequest:(OBContact *)newFriend
{
	PFObject *newFriendRequest = [[PFObject alloc] initWithClassName:@"friendRequest"];
	[newFriendRequest setObject:self.currentParseUser forKey:@"fromUser"];
	[newFriendRequest setObject:newFriend.objectsUser forKey:@"toUser"];
	[newFriendRequest saveEventually:^(BOOL succeeded, NSError *error) {
		if(!succeeded){
		}
	}];
	
	//Register new activity
	[self newSocialInteractionWithType:@"Sent a friend request"];
}

- (void) retrieveFriendRequestsWithCallback:(void(^)(NSArray *friendRequests, NSError *error))callback
{
	if(self.friendRequests!=nil){
		callback(self.friendRequests,nil);
	} else {
		[self.queryManager retrieveFriendRequestsWithCallback:^(NSArray *friendRequests, NSError *error) {
			if(!error){
				self.friendRequests = [NSMutableArray arrayWithArray:friendRequests];
				callback(friendRequests,nil);
			} else {
				callback(nil,error);
			}
		}];
	}
}


- (void) retrieveNewFriendRequestsWithCallback:(void(^)(NSArray *friendRequests, NSError *error))callback
{
	[self.queryManager retrieveFriendRequestsWithCallback:^(NSArray *friendRequests, NSError *error) {
		if(!error){
			self.friendRequests = [NSMutableArray arrayWithArray:friendRequests];
			callback(friendRequests,nil);
		} else {
			callback(nil,error);
		}
	}];
}

- (void) setProfilePicture:(UIImage *)profilePic showProgress:(BOOL)display
{
	self.userDetails.profilePicture = profilePic;
	NSData *imageFile = [OBUser imageToData:profilePic];
	PFFile *profileToUpload = [PFFile fileWithData:imageFile];
	[profileToUpload saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
		//Check if we are displaying progress and if we are we can dismiss it.
		if(display){
			[SVProgressHUD dismiss];
		}
		[self.currentParseUser setObject:profileToUpload forKey:@"profilePicture"];
		[self.currentParseUser saveEventually];
	} progressBlock:^(int percentDone) {
		if(display){
			[SVProgressHUD showProgress:percentDone/100.0f status:@"Uploading picture"];
		}
	}];
	
	//Register new activity
	[self newSocialInteractionWithType:@"Updated profiled picture"];
}

+ (NSData *) imageToData:(UIImage *) image
{
	return UIImageJPEGRepresentation(image,1.0f);
}

-(void) addFriend:(OBContact *)newFriend
{
	PFObject *friendLink = [PFObject objectWithClassName:@"friendLink"];
	[friendLink setObject:[self currentParseUser] forKey:@"personOne"];
	[friendLink setObject:[newFriend objectsUser] forKey:@"personTwo"];
	[friendLink saveEventually];
	
	[self.friendList addObject:newFriend];
	[[NSNotificationCenter defaultCenter] postNotificationName:@"newFriends" object:nil];
}

- (void) removeFriend:(OBContact *)exFriend
{
	[self.friendList removeObject:exFriend];
	[self.queryManager findFriendLink:exFriend.objectsUser andUser:self.currentParseUser withCompletion:^(NSArray *friendLinks, NSError *error) {
		if(friendLinks.count!=0){
			for(PFObject *o in friendLinks){
				[o deleteEventually];
			}
		}
	}];
}

- (void) acceptFriendRequestFrom:(OBContact *)user
{
	[self.friendRequests removeObject:user];
	//Need to find and delete friend request.
	[OBParseQueryManager findAndDeleteFriendRequestFrom:user.objectsUser to:self.currentParseUser];
	//And add as friend
	[self addFriend:user];
	
	//Register new activity
	[self newSocialInteractionWithType:@"Accepted friend requests"];
}

- (void) denyFriendRequestFrom:(OBContact *)user
{
	[self.friendRequests removeObject:user];
	//Need to find and delete friend request.
	[OBParseQueryManager findAndDeleteFriendRequestFrom:user.objectsUser to:self.currentParseUser];
	// If we wanted to block things we would do so here.
	
	//Register new activity
	[self newSocialInteractionWithType:@"Checked for friend requests"];
}

- (void) getMessageinboxForContact:(OBContact *)contact withCallback:(void(^)(OBMessageInbox *inbox, NSError *error)) callback
{
	OBMessageInbox *messageInbox = [self.messageInboxes objectForKey:contact.objectsUser.objectId];
	if(messageInbox!=nil){
		if(callback!=nil){
			callback(messageInbox,nil);
		}
	} else {
		//Need to create it
		OBMessageInbox *inbox = [OBMessageInbox createInboxFor:contact andContact:self.userDetails];
		[self.messageInboxes setValue:inbox forKey:contact.objectsUser.objectId];
		if(callback!=nil){
			callback(inbox,nil);
		}
    }
}


- (void) reloadMessageInboxFor:(OBContact *)user withCallback:(void (^)(BOOL downloadedNew))completionBlock
{
	OBMessageInbox *messageInbox = [self.messageInboxes objectForKey:user.objectsUser.objectId];
	if(messageInbox==nil){
		//Then the user needs to download the message inbox in the first place
		//Have to downlaod from the server
		OBMessageInbox *inbox = [OBMessageInbox createInboxFor:user andContact:self.userDetails];
		[self.messageInboxes setValue:inbox forKey:user.objectsUser.objectId];
		if(completionBlock!=nil){
			completionBlock(YES);
		}
	} else {
		//Then check for new messages
		[messageInbox checkForNewMessagesWithUpdate:^(BOOL downloadedNew){
			if(completionBlock!=nil){
				completionBlock(downloadedNew);
			}
		}];
	}
	
	//Register new activity
	[self newSocialInteractionWithType:@"Reloaded message inbox"];
}

//For manipulating messages;
- (void) markMessageAsRead:(OBMessage *)message
{
	//Check who sent the message (need to find the one that isn't us to then find the inbox)
	PFUser *testUser = [message toUser];
	if(testUser==self.currentParseUser){
		testUser = message.fromUser;
	}
	//Get the message inbox
	OBMessageInbox *messageInbox = [self.messageInboxes objectForKey:testUser.objectId];
	[messageInbox markAsRead:message];
	
	//Register new activity
	[self newSocialInteractionWithType:@"Read a message"];
	
}

- (void) deleteMessage:(OBMessage *)message
{
	//Check who sent the message (need to find the one that isn't us to then find the inbox)
	PFUser *testUser = message.toUser;
	if(testUser==self.currentParseUser){
		testUser = message.fromUser;
	}
	//Get the message inbox
	OBMessageInbox *messageInbox = [self.messageInboxes objectForKey:testUser.objectId];
	[messageInbox deleteMessage:message];
	
	//Register new activity
	[self newSocialInteractionWithType:@"Deleted message"];
	
}

//For sending messages;
//Maybe add the ability to show progress with sending
- (void) createAndSendPictureMessageWith:(UIImage *)contents andRecipient:(OBContact *)recipient withProgress:(void (^)(int percentdone))showProgress
{
	//Get the message inbox
	OBMessageInbox *messageInbox = [self.messageInboxes objectForKey:recipient.objectsUser.objectId];
	if(messageInbox!=nil){
		[messageInbox createAndSendPictureMessageWith:contents withProgress:showProgress];
	} else {
		//We have to create the message inbox!
		OBMessageInbox *inbox = [OBMessageInbox createInboxFor:recipient andContact:self.userDetails];
		[self.messageInboxes setValue:inbox forKey:recipient.objectsUser.objectId];
		[inbox createAndSendPictureMessageWith:contents withProgress:showProgress];
	}
	
	//Register new activity
	[self newSocialInteractionWithType:@"Sent a picture message"];
}

- (void) createAndSendTextMessageWith:(NSString *)contents andRecipient:(OBContact *)recipient withProgress:(void (^)(int percentdone))showProgress
{
	//Get the message inbox
	OBMessageInbox *messageInbox = [self.messageInboxes objectForKey:recipient.objectsUser.objectId];
	if(messageInbox!=nil){
		[messageInbox createAndSendTextMessageWith:contents withProgress:showProgress];
	} else {
		//We have to create the message inbox!
		OBMessageInbox *inbox = [OBMessageInbox createInboxFor:recipient andContact:self.userDetails];
		[self.messageInboxes setValue:inbox forKey:recipient.objectsUser.objectId];
		[inbox createAndSendTextMessageWith:contents withProgress:showProgress];
    };
	
	//Register new activity
	[self newSocialInteractionWithType:@"Sent a text message"];
}

//Functions for signing up new users
- (void) signUpUserInBackgroundWithBlock:(void (^)(BOOL succeeded, NSError *error))completion
{
	[self.currentParseUser signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
		completion(succeeded,error);
	}];
}

-(void)createNewUserWithProfilePicture:(UIImage *)profilePicture andCompletion:(void (^)(BOOL succeeded, NSError *error))completionBlock
{
	[self signUpUserInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
		if(!error){
			if(profilePicture!=nil){
				[self setProfilePicture:profilePicture showProgress:true];
				[OBUser registerForPushNotificationsFor:self.currentParseUser];
				if(completionBlock!=nil){
					completionBlock(succeeded,error);
				}
			} else {
				if(completionBlock!=nil){
					completionBlock(succeeded,error);
				}
			}
		}
	}];
}

//Functions for loading details from server
- (void) checkForNewMessagesFrom:(OBContact *)user withUpdate:(void (^)(BOOL downloadedNew))hasUpdates
{
	//Get teh inbox and instruct it to do an update
	OBMessageInbox *messageInbox = [self.messageInboxes objectForKey:user.objectsUser.objectId];
	[messageInbox checkForNewMessagesWithUpdate:hasUpdates];
	
	//Register new activity
	[self newSocialInteractionWithType:@"Load messaging"];
}

+ (void) logOut
{
	//Set the instalation
	[[PFInstallation currentInstallation] setObject:@"" forKey:@"user"];
	//Set the user logged in as nil (therefore deleting all of the user)
	currentUser = nil;
	//Log out through parse
	[PFUser logOut];
}

+ (void) logInWithUserNameInBackground:(NSString *) userName andPassword:(NSString *)password onCompletion:(void(^)( NSError *error))completion
{
	[PFUser logInWithUsernameInBackground:userName password:password block:^(PFUser *user, NSError *error) {
		if(!error){
			[OBUser registerForPushNotificationsFor:user];
		}
		if(completion!=nil){
			completion(error);
		}
	}];
}

+ (void) registerForPushNotificationsFor:(PFUser *)user
{
	[[PFInstallation currentInstallation] setObject:user forKey:@"user"];
	[[PFInstallation currentInstallation] saveEventually:^(BOOL succeeded, NSError *error) {
		if (succeeded)
			NSLog(@"successfully saved installation");
		else
			NSLog(@"failed to save installation");
	}];
}

//Function for logging recentActivity
- (void) newSocialInteractionWithType:(NSString *) type
{
	[[self userDetails] newSocialInteractionWithType:type];
}

@end