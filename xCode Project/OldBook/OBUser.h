//
// OBUser.h
// OldBook
//
// Created by Mat on 15/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//
// A local user for all data required by the current
// PFUser that is logged in, such as their message indexes
// their contact details (Stored in a OBContact)


#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "OBContact.h"
#import "OBMessageInbox.h"
#import "OBParseQueryManager.h"
#import "SVProgressHUD.h"


@interface OBUser : NSObject

//The query manager
@property (strong, nonatomic) OBParseQueryManager *queryManager;
//The user we are logged in as
@property (strong, nonatomic) PFUser *currentParseUser;
//The users contact details
@property (strong, nonatomic) OBContact *userDetails;
//The users friends as a mutable array (so we can add to them)
@property (strong, nonatomic) NSMutableArray *friendList;
//The users friend requests
@property (strong, nonatomic) NSMutableArray *friendRequests;

//The users message lists
@property (strong, nonatomic) NSMutableDictionary *messageInboxes;

//Functions for creating OBUser
+ (id) getCurrentUser;
- (id) initWithMockObjectsMailbox:(OBParseQueryManager *) qm andUser:(PFUser *) user andDetails:(OBContact *) details andFriendlist:(NSMutableArray *) fl andMessages:(NSMutableDictionary *) inbox;
+ (OBUser *) createTempUserFor:(NSString *) username andPassword:(NSString *)password andEmail:(NSString *)email andBirthday:(NSDate *)birthday andFirstName:(NSString *)firstName andLastName:(NSString *)lastName;
+ (NSString *) getCurrentUserType;

//Functions for loading details from server
- (void) checkForNewMessagesFrom:(OBContact *)user withUpdate:(void (^)(BOOL downloadedNew))hasUpdates;

//Functions for getting information for the program
- (void) getMessageinboxForContact:(OBContact *)contact withCallback:(void(^)(OBMessageInbox *inbox, NSError *error)) callback;

//Functions for updating OBUser
- (void) setProfilePicture:(UIImage *)profilePic showProgress:(BOOL)display;

//Functions for managing friends
- (void) sendFriendRequest:(OBContact *) newFriend;
- (void) addFriend:(OBContact *) newFriend;
- (void) removeFriend:(OBContact *) exFriend;
- (OBContact *) contactFromFriendLink:(PFObject *)friendLink;
- (void) acceptFriendRequestFrom:(OBContact *)user;
- (void) denyFriendRequestFrom:(OBContact *)user;

//For retrieving information in the program
- (void) updateFriendListWithCallback:(void(^)(NSArray *friendList, NSError *error))callback;
- (void) retrieveFriendlistWithCallback:(void(^)(NSArray *friendList, NSError *error))callback;
- (BOOL) userHasFriendRequests;
- (void) retrieveFriendRequestsWithCallback:(void(^)(NSArray *friendRequests, NSError *error))callback;
- (void) retrieveNewFriendRequestsWithCallback:(void(^)(NSArray *friendRequests, NSError *error))callback;
- (void) reloadMessageInboxFor:(OBContact *)user withCallback:(void (^)(BOOL downloadedNew))completionBlock;

//For sending messages;
- (void) createAndSendPictureMessageWith:(UIImage *)contents andRecipient:(OBContact *)recipient withProgress:(void (^)(int percentdone))showProgress;
- (void) createAndSendTextMessageWith:(NSString *)contents andRecipient:(OBContact *)recipient withProgress:(void (^)(int percentdone))showProgress;

//For manipulating messages;
- (void) markMessageAsRead:(OBMessage *)message;
- (void) deleteMessage:(OBMessage *)message;

//Functions for signing up new users
- (void) signUpUserInBackgroundWithBlock:(void (^)(BOOL succeeded, NSError *error))completion;
- (void)createNewUserWithProfilePicture:(UIImage *)profilePicture andCompletion:(void (^)(BOOL succeeded, NSError *error))completionBlock;

//Functions for logging in and logging out
+ (void) logOut;
+ (void) logInWithUserNameInBackground:(NSString *) userName andPassword:(NSString *)password onCompletion:(void(^)(NSError *error))completion ;
+ (void) registerForPushNotificationsFor:(PFUser *) user;

@end
