//
//  SocialDefaultViewController.m
//  OldBook
//
//  Created by Sindre on 16/10/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import "SocialDefaultViewController.h"
#import "OBCameraViewControllerHelper.h"
#import "AppDelegate.h"
#import "OBUser.h"


@interface SocialDefaultViewController () <UIImagePickerControllerDelegate>

@property(nonatomic, strong) OBCameraViewControllerHelper* cameraHelper;
@property (strong, nonatomic) IBOutlet UIImageView* userImage;
@property(strong, nonatomic) UIAlertView* logOutAlert;

@property(strong, nonatomic) OBUser* user;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;

@end

@implementation SocialDefaultViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.user = [OBUser getCurrentUser];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateProfilePic)
                                                 name:@"socialProfilePic"
                                               object:nil];

    self.cameraHelper = [[OBCameraViewControllerHelper alloc] initWithImageDest:self.userImage inView:self withNotifier:@"socialProfilePic"];
}

- (void)viewDidAppear:(BOOL)animated
{
    self.userImage.image = [self.user.userDetails profilePicture];
    self.userNameLabel.text = [self.user.userDetails getFullName];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Profile Picture Methods
- (void)updateProfilePic
{
    [[OBUser getCurrentUser] setProfilePicture:[self.cameraHelper profilePicture] showProgress:YES];
}

- (void)changeProfilePic:(id)sender
{
    [self.cameraHelper selectPhoto:sender];
}

- (IBAction)changePhotoButtonTapped:(id)sender
{
    [self changeProfilePic:sender];
}

- (IBAction)userImageTapped:(id)sender
{
    [self changeProfilePic:sender];
}


#pragma mark Log Out Methods
- (IBAction)logOutButtonPressed:(id)sender
{
    self.logOutAlert = [[UIAlertView alloc] initWithTitle:@"Log Out?"
                                                  message:@"Are you sure you want to log out?"
                                                 delegate:self
                                        cancelButtonTitle:@"NO"
                                        otherButtonTitles:@"YES", nil];
    [self.logOutAlert show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView == self.logOutAlert) {
        if (buttonIndex != [alertView cancelButtonIndex]) {
            [OBUser logOut];
            AppDelegate* app = [AppDelegate getCurrentAppDelegate];
            [app presentStoryBoardView:@"LOGINVIEW"];
        } else {
            [self resignFirstResponder];
        }
    } else {
        [self resignFirstResponder];
    }
}
@end
