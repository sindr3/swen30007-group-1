//
//  CarerClientEmergencyViewController.h
//  OldBook
//
//  Created by Oscar Morrison on 17/10/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBEmergencyRequest.h"
#import "OBCarerUser.h"

@interface CarerClientEmergencyViewController : UIViewController
//The emergency request for our given user.
@property (strong, nonatomic) OBEmergencyRequest *request;
//The caerer user
@property (strong, nonatomic) OBCarerUser *carer;

@end
