//
// OBMessageInbox.m
// OldBook
//
// Created by Mat on 15/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBMessageInbox.h"

@implementation OBMessageInbox

//Function to create message inbox
+ (OBMessageInbox *) createInboxFor:(OBContact *)contact andContact:(OBContact *)contactTwo
{
	//Create an inbox
	OBMessageInbox *newInbox = [OBMessageInbox new];
	newInbox.unread=0;
	newInbox.messagesTo = contact;
	//Initialise the users of the inbox
	newInbox.messagesFrom = contactTwo;
	
	newInbox.toUserName = contact.firstName;
	newInbox.toUserPicture = contact.profilePicture;
	newInbox.fromUserName = contactTwo.firstName;
	newInbox.fromUserPicture = contactTwo.profilePicture;
	
	[newInbox performSelectorInBackground:@selector(findMessages) withObject:nil];
	newInbox.lastUpdated = [NSDate date];
	return newInbox;
}

- (void) findMessages {
	OBParseQueryManager *qm = [OBParseQueryManager getQueryManager];
	[qm queryForMessagesBetween:self.messagesTo.objectsUser andUser:self.messagesFrom.objectsUser onCompletion:^(NSArray *objects, NSError *error) {
		self.messages = [NSMutableArray arrayWithArray:[self messagesFromServerObjects:objects]];
		[[NSNotificationCenter defaultCenter] postNotificationName:@"finishedLoadingMessages" object:nil];
	}];
}

//Converts our PFObjects into local messages
- (NSArray *) messagesFromServerObjects:(NSArray *) serverObjects
{
	NSMutableArray *mut = [NSMutableArray new];
	for(PFObject *o in serverObjects){
		OBMessage *m = [OBMessage saveMessageFromServer:o];
		if(!m.read){
			self.unread++;
		}
		[mut addObject:m];
	}
	return [NSArray arrayWithArray:mut];
}

//Functions for sending messages
- (void) createAndSendTextMessageWith:(NSString *)textContents withProgress:(void (^)(int percentdone))showProgress
{
	OBMessage *sending = [OBMessage createAndSendTextMessageWith:textContents toUser:self.messagesTo.objectsUser fromUser:self.messagesFrom.objectsUser withProgress:showProgress];
	[self.messages insertObject:sending atIndex:0];
	[self.messagesFrom newSocialInteractionWithType:@"Sent a text message"];
	
}

- (void) createAndSendPictureMessageWith:(UIImage *)imageContents withProgress:(void (^)(int percentdone))showProgress
{
	OBMessage *sending = [OBMessage createAndSendPictureMessageWith:imageContents andToUser:self.messagesTo.objectsUser andFromUser:self.messagesFrom.objectsUser withProgress:showProgress];
	[self.messages insertObject:sending atIndex:0];
	[self.messagesFrom newSocialInteractionWithType:@"Sent a picture message"];

}


//Functions for checking for updates
- (void) checkForNewMessagesWithUpdate:(void (^)(BOOL downloadedNew))hasUpdates
{
	//Get the query manager to perform our query
	OBParseQueryManager *qm = [OBParseQueryManager getQueryManager];
	[qm queryForMessagesFrom:self.messagesFrom.objectsUser toUser:self.messagesTo.objectsUser newerThan: self.lastUpdated onCompletion:^(NSArray *objects, NSError *error) {
		if(objects.count>0)
		{
			NSArray *messages = [self messagesFromServerObjects:objects];
			for(OBMessage *message in messages){
				if(!message.read){
					self.unread++;
				}
				[self.messages insertObject:message atIndex:0];
			}
			self.lastUpdated = [NSDate date];
		}
		hasUpdates(objects.count);
	}];
}

//Fucntions for deleting messages
- (void) deleteMessage:(OBMessage *)messageToDelete
{
	//Delete it locally
	[self.messages removeObject:messageToDelete];
	//Then delete it on the server
	[messageToDelete.serverObject deleteEventually];
}

//Functions for editing messages
- (void) markAsRead:(OBMessage *)message
{
	PFObject *serverMessage = message.serverObject;
	[serverMessage setObject:[NSNumber numberWithBool:YES] forKey:@"hasBeenRead"];
	[serverMessage saveEventually];
	message.read=YES;
	self.unread--;
}

- (void) markAsUnread:(OBMessage *)message
{
	PFObject *serverMessage = message.serverObject;
	[serverMessage setObject:[NSNumber numberWithBool:NO] forKey:@"hasBeenRead"];
	[serverMessage saveEventually];
	message.read=NO;
	self.unread++;
}


- (OBMessage *) getMessageAtIndex:(int) index
{
	OBMessage *msg = [self.messages objectAtIndex:index];
	if(!msg.outgoing && !msg.read){
		[self markAsRead:msg];
	}
	return msg;
}
@end


@implementation OBMessageInboxSYNCH

//Function to create message inbox
+ (OBMessageInboxSYNCH *) createInboxForSYNCH:(OBContact *)contact andContact:(OBContact *)contactTwo
{
	//Create an inbox
	OBMessageInboxSYNCH *newInbox = [OBMessageInboxSYNCH new];
	newInbox.messagesTo = contact;
	newInbox.messagesFrom = contactTwo;
	//Initialise the users of the inbox
	newInbox.toUserName = contact.firstName;
	newInbox.toUserPicture = contact.profilePicture;
	newInbox.fromUserName = contactTwo.firstName;
	newInbox.fromUserPicture = contactTwo.profilePicture;
	OBSYNCHParseQueryManager *test = [OBSYNCHParseQueryManager getSynchQueryManager];
	NSArray *objects = [test queryForMessagesBetweenSYNCH:contact.objectsUser andUser:contactTwo.objectsUser];
	newInbox.messages = [NSMutableArray arrayWithArray:[newInbox messagesFromServerObjectsSYNCH:objects]];
	newInbox.lastUpdated = [NSDate date];
	return newInbox;
}

//Functions for sending messages
- (void) createAndSendTextMessageWith:(NSString *)textContents withProgress:(void (^)(int percentdone))showProgress
{
	OBMessage *sending = [OBMessage createAndSendTextMessageWithSYNCH:textContents toUser:self.messagesTo.objectsUser fromUser:self.messagesFrom.objectsUser withProgress:showProgress];
	[self.messages insertObject:sending atIndex:0];
}

- (void) createAndSendPictureMessageWith:(UIImage *)imageContents withProgress:(void (^)(int percentdone))showProgress
{
	OBMessage *sending = [OBMessage createAndSendPictureMessageWithSYNCH:imageContents andToUser:self.messagesTo.objectsUser andFromUser:self.messagesFrom.objectsUser withProgress:showProgress];
	[self.messages insertObject:sending atIndex:0];
}


//Functions for checking for updates
- (BOOL) checkForNewMessagesWithSYNCH
{
	//Need to use the query manager to get message updates from a given time
	OBSYNCHParseQueryManager *qm = [OBSYNCHParseQueryManager getSynchQueryManager];
	NSArray *objects = [qm queryForMessagesBetweenSYNCH:self.messagesFrom.objectsUser andUser:self.messagesTo.objectsUser newerThan:self.lastUpdated];
	if(objects.count>0){
		NSArray *messages = [self messagesFromServerObjectsSYNCH:objects];
		for(OBMessage *message in messages){
			[self.messages insertObject:message atIndex:0];
		}
		self.lastUpdated = [NSDate date];
		return YES;
	} else {
		return NO;
	}
}


//Fucntions for deleting messages
- (void) deleteMessageSYNCH:(OBMessage *)messageToDelete
{
	//Delete it locally
	[self.messages removeObject:messageToDelete];
	//Then delete it on the server
	[messageToDelete.serverObject delete];
}

//Functions for editing messages
- (void) markAsReadSYNCH:(OBMessage *)message
{
	PFObject *serverMessage = message.serverObject;
	[serverMessage setObject:[NSNumber numberWithBool:YES] forKey:@"hasBeenRead"];
	[serverMessage save];
	message.read=YES;
}

- (void) markAsUnreadSYNCH:(OBMessage *)message
{
	PFObject *serverMessage = message.serverObject;
	[serverMessage setObject:[NSNumber numberWithBool:NO] forKey:@"hasBeenRead"];
	[serverMessage save];
	message.read=NO;
	
}

- (OBMessage *) getMessageAtIndex:(int) index
{
	return [self.messages objectAtIndex:index];
}

//Converts our PFObjects into local messages
- (NSArray *) messagesFromServerObjectsSYNCH:(NSArray *) serverObjects
{
	NSMutableArray *mut = [NSMutableArray new];
	for(PFObject *o in serverObjects){
		[mut addObject:[OBMessage saveMessageFromServerSYNCH:o]];
	}
	return [NSArray arrayWithArray:mut];
}


@end

