//
// RequestsViewController.h
// OldBook
//
// Created by Mat on 9/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBAssistedUser.h"

@interface RequestsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *carerImage;
@property (weak, nonatomic) IBOutlet UILabel *carersName;
@property (weak, nonatomic) IBOutlet UILabel *lastSeen;
@property (strong, nonatomic) OBCarer *currentCarer;
@property (strong, nonatomic) OBAssistedUser *currentUser;
@property (strong, nonatomic) NSMutableArray *requestsAvailable;

@end
