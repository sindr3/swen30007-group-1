//
// OBCarerUser.m
// OldBook
//
// Created by Mat on 17/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "OBLocationManager.h"
#import "OBCarerUser.h"

@implementation OBCarerUser

static OBCarerUser *currentUser = nil;

//Methods for creating an OBCarerUser;
- (id) init
{
	self = [super init];
	
	//Then download the client list
	[self downloadClientListWithBlock:^(NSArray *clientList, NSError *error) {
		[self performSelectorInBackground:@selector(backgroundClientActivity) withObject:nil];
		//Then download client requests
		//[self getAllClientRequests];
	}];
	
	return self;
}

- (void) backgroundThreadClientList {
	
}

- (void) backgroundClientActivity {
	OBParseQueryManager *qm = [OBParseQueryManager getQueryManager];
	[qm getInteractionsForUsers:self.clientList withCallback:^(NSArray *updates, NSError *error) {
		self.clientUpdates = updates;
		[[NSNotificationCenter defaultCenter] postNotificationName:@"loadedRecentActivity" object:nil];
	}];
}

- (void) backgroundClientRequests {
	
}

+ (OBCarerUser *) getCurrentCarerUser
{
	if(currentUser==nil){
		currentUser = [[OBCarerUser alloc] init];
	}
	return currentUser;
}

//Downloads the client list from the sever if we don't have it locally
- (void) downloadClientListWithBlock:(void (^)(NSArray *clientList, NSError *error))completionBlock
{
	if(!self.clientList){
		//Get the query manager
		[self.queryManager findClientsFor:self.currentParseUser withBlock:^(NSArray *clientList, NSError *error) {
			self.clientList = [NSMutableArray arrayWithArray:clientList];
			completionBlock(self.clientList,nil);
		}];
	} else {
		completionBlock(self.clientList,nil);
	}
	
}

- (void)getDirectionsResponseToAP:(OBAssistedContact *)apContact withCallback:(void (^)(MKDirectionsResponse *response, NSError *error))completionBlock
{
	[OBLocationManager updateServerLocationForLoggedInUserInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
		if (error)
		{
			completionBlock(nil, error);
		}
		
		else
		{
			OBContact *selfContact = [self userDetails];
			[OBLocationManager directionsResponseFromContact:selfContact toContact:apContact withBlock:^(MKDirectionsResponse *response, NSError *error) {
				completionBlock(response, error);
			}];
		}
	}];
}

- (CLLocation *) getUserLocation:(OBAssistedContact *)contact
{
	PFGeoPoint *geoPoint = [contact.objectsUser objectForKey:@"location"];
	CLLocation *location = [[CLLocation alloc] initWithLatitude:geoPoint.latitude longitude:geoPoint.longitude];
	return location;
}

- (void)getAllClientRequests
{
	for(OBAssistedContact *client in self.clientList){
		[self getClientRequestsForContact:client withCallback:nil];
	}
}


- (void )getClientRequestsForContact:(OBContact *) contact withCallback:(void(^)(NSMutableArray *requests, NSError *error))callback
{
	//Check if it already exists
	NSArray *requests = [self.clientRequests objectForKey:contact.objectsUser.objectId];
	if(requests){
		callback([NSMutableArray arrayWithArray:requests],nil);
	} else {
		//Need to download clientlist
		[self.queryManager findSentCarerRequestsFor:contact.objectsUser withCallback:^(NSArray *available, NSError *error) {
			if(!error && available.count!=0){
				[self.clientRequests setObject:available forKey:contact.objectsUser.objectId];
				if(callback!=nil){
					callback([NSMutableArray arrayWithArray:available],error);
				}
			} else {
				if(callback!=nil){
					callback(nil, error);
				}
			}
		}];
	}
}


//Methods for interacting specific to ob assited contacts
- (void) checkInForUser:(OBAssistedContact *)contact
{
	[contact newSocialInteractionWithType:@"Carer Check-in"];
}

//Functions for checking recent activity
- (void) getRecentClientActivityWithCallback:(void (^)(NSArray *recentActivity, NSError *error))completionCode
{
	if(self.clientUpdates!=nil){
		completionCode(self.clientUpdates,nil);
		[[NSNotificationCenter defaultCenter] postNotificationName:@"loadedRecentActivity" object:nil];
	} else {
		OBParseQueryManager *qm = [OBParseQueryManager getQueryManager];
		[qm getInteractionsForUsers:self.clientList withCallback:^(NSArray *updates, NSError *error) {
			self.clientUpdates = updates;
			[[NSNotificationCenter defaultCenter] postNotificationName:@"loadedRecentActivity" object:nil];
			completionCode(self.clientUpdates,nil);
		}];
	}
}

+ (void) logOut
{
	[super logOut];
	currentUser = nil;
}


@end
