//
// SocialUserViewController.h
// OldBook
//
// Created by Mat on 8/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SVProgressHUD.h"
#import "OBContactDetailViewViewController.h"
#import "OBUser.h"


@interface SocialUserViewController : UIViewController

@property (strong, nonatomic) OBContact *userSelected;
@property (strong, nonatomic) OBUser *currentUser;
@end
