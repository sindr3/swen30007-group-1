//
// ;
// OldBook
//
// Created by Mat on 19/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OBContact.h"

@interface OBRecentAcitvity : NSObject

//The contact it was with
@property (strong, nonatomic) OBContact *contact;
//The type of recent activity (message or friend request)
@property (strong, nonatomic) NSString *type;

//Funtion to create it from friend request or from message
+ (OBRecentAcitvity *) createNewActivitytForUser:(OBContact *)user ofType:(NSString *) type;

@end
