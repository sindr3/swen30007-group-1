//
//  ApNavViewController.h
//  OldBook
//
//  Created by Sindre on 9/10/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "OBAssistedUser.h"
#import "OBContactDetailViewViewController.h"

@interface ApNavViewController : UIViewController
@property (strong, nonatomic) OBContact* chosenFriend;
@property (strong, nonatomic) OBContactDetailViewViewController* messagingView;

- (void)showMessagingViewForContact:(OBContact*)contact;

@end
