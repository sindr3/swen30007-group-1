//
//  MessagingViewController.h
//  OldBook
//
//  Created by Sindre on 23/09/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBUser.h"
#import "OBContact.h"

@interface MessagingViewController : UIViewController
@property (nonatomic) OBContact *contact;
@property (strong,nonatomic) NSMutableArray * messages;

- (NSString *) lastSeenTime:(NSTimeInterval) lastSeen;
@end
