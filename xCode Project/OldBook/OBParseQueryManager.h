//
// OBParseQueryManager.h
// OldBook
//
// Created by Mat on 15/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>


@class OBCarer;
@class OBAssistedContact;
@class OBParseQueryManager;
@class OBEmergencyRequest;

@interface OBParseQueryManager : NSObject


//Class methods
+ (id) getQueryManager;
+ (id) initializeQueryManager;

//==========================
// -- Social Methods ------
//==========================

//Query for friendlist relevant to the user.
-(void) queryForFriendlistForUser:(PFUser *) user onCompletion: (void(^)(NSArray *objects,NSError *error))completion;
//Query for messages between user1 and user2
- (void) queryForMessagesBetween:(PFUser *)userOne andUser:(PFUser *)userTwo onCompletion:(void(^)(NSArray *objects, NSError *error))completionCode;
- (void) queryForMessagesBetween:(PFUser *)userOne andUser:(PFUser *)userTwo newerThan:(NSDate *)date onCompletion:(void(^)(NSArray *objects, NSError *error))completionCode;
- (void) queryForMessagesFrom:(PFUser *)userOne toUser:(PFUser *)userTwo newerThan:(NSDate *)date onCompletion:(void(^)(NSArray *objects, NSError *error))completionCode;
//Changes an array of friendlinks to friends;
- (NSArray *) friendsFromFriendLinks:(NSArray *) friendLinks;
//Find friendLink for two given users
- (void) findFriendLink:(PFUser *)userOne andUser:(PFUser *)userTwo withCompletion:(void (^)(NSArray *friendLinks, NSError *error))completion;
//Get the list of friend Requests for a user
- (void) retrieveFriendRequestsWithCallback:(void(^)(NSArray *friendRequests, NSError *error))callback;
//Find nearby friends for a given location
- (void) findNearbyFriendsFor:(PFGeoPoint *)location withBlock:(void (^)(NSArray *friends, NSError *err))block;

//==============================
// -- Assisted User methods ----
//==============================

//Changes an array of carers to PFuser;
- (NSArray *) carersFromCarerLinks:(NSArray *) carerLinks;

//Find list of carers for a user;
- (void) findCarersFor:(PFUser *)assistedPerson withCallback:(void (^)(NSArray *carers,NSError *error))callback;

//Find the requests available for a carer;
- (void) findCarerRequestsFor:(PFUser *)carer andUser:(PFUser *)user withCallback:(void (^)(NSArray *available,NSError *error))callback;

//Find the requests sent for a carer;
- (void) findSentCarerRequestsFor:(PFUser *)user withCallback:(void (^)(NSArray *available,NSError *error))callback;

//Find the single carer for a user;
- (void) findCarerFor:(PFUser *)assistedPerson withCallback:(void (^)(OBCarer *carer,NSError *error))callback;

//======================
// -- Carer methods ----
//======================
//Find emergency contacts for a user
- (void) findEmergencyContactsFor:(OBAssistedContact *)contact withBlock:(void (^)(NSArray *emergencyContacts, NSError *error))completionBlock;
// Find requests made to a carer from a user
- (void) findRequestsForCarer:(PFUser *)carer fromUser:(PFUser *)user withBlock:(void (^)(NSArray *requests, NSError *error))block;
//Find the clients for a given carer
- (void) findClientsFor:(PFUser *)carer withBlock:(void (^)(NSArray *clientList, NSError *error))completionBlock;
//Find nearby carers for a given location
- (void) findNearbyCarersFor:(PFGeoPoint *)location withBlock:(void (^)(NSArray *friends, NSError *err))block;
//Get an existing emergency request
- (OBEmergencyRequest *) retrieveEmergencyRequestFor:(OBAssistedContact *)contact;

//=========================
// -- Utitlity methods ----
//=========================

//Change an array of users into an array of obcontacts
+ (NSArray *) contactsFromUsers:(NSArray *) users;
//Delete a friend request from the server
+ (void) findAndDeleteFriendRequestFrom:(PFUser *)user to:(PFUser *)recipient;
- (void) getLastInteractionFor:(PFUser *) user withCallBack:(void (^)(NSDate *lastInteraction)) callback;
- (void) getInteractionsForUsers:(NSMutableArray *) users withCallback:(void (^)(NSArray *requests, NSError *error))completionBlock;

@end

@interface OBSYNCHParseQueryManager : NSObject

//Our queries for various things
@property (strong, nonatomic) PFQuery *friendListQuery;
@property (strong, nonatomic) NSDictionary *messageInboxQueries;

//Class methods
+ (id) getSynchQueryManager;
+ (id) initializeSynchQueryManager;

//==========================
// -- Social Methods ------
//==========================

//Query for friendlist relevant to the user.
-(NSArray *) queryForFriendlistForUserSYNCH:(PFUser *) user;
//Query for messages between user1 and user2
- (NSArray *) queryForMessagesBetweenSYNCH:(PFUser *)userOne andUser:(PFUser *)userTwo;
- (NSArray *) queryForMessagesBetweenSYNCH:(PFUser *)userOne andUser:(PFUser *)userTwo newerThan:(NSDate *)date;
//Changes an array of friendlinks to friends;
- (NSArray *) friendsFromFriendLinksSYNCH:(NSArray *) friendLinks;
//Find friendLink for two given users
- (NSArray *) findFriendLinkSYNCH:(PFUser *)userOne andUser:(PFUser *)userTwo;
//Get the list of friend Requests for a user
- (NSArray *) retrieveFriendRequestsWithCallbackSYNCH;
//Find nearby friends for a given location
- (NSArray *) findNearbyFriendsForSYNCH:(PFGeoPoint *)location;

//==============================
// -- Assisted User methods ----
//==============================

//Changes an array of carers to PFuser;
- (NSArray *) carersFromCarerLinks:(NSArray *) carerLinks;

//Find list of carers for a user;
- (NSArray *) findCarersForSYNCH:(PFUser *)assistedPerson;

//Find the requests available for a carer;
- (NSArray *) findCarerRequestsForSYNCH:(PFUser *)carer;

//Find the requests sent for a carer;
- (NSArray *) findSentCarerRequestsForSYNCH:(PFUser *)user;

//Find the single carer for a user;
- (OBCarer *) findCarerForSYNCH:(PFUser *)assistedPerson;

//======================
// -- Carer methods ----
//======================

//Find emergency contacts for a user
- (NSArray *) findEmergencyContactsForSYNCH:(OBAssistedContact *)contact;
// Find requests made to a carer from a user
- (NSArray *) findRequestsForCarerSYNCH:(PFUser *)carer fromUser:(PFUser *)user;
//Find the clients for a given carer
- (NSArray *) findClientsForSYNCH:(PFUser *)carer;
//Find nearby carers for a given location
- (NSArray *) findNearbyCarersForSYNCH:(PFGeoPoint *)location;
//Get an existing emergency request
- (OBEmergencyRequest *) retrieveEmergencyRequestFor:(OBAssistedContact *)contact;

//=========================
// -- Utitlity methods ----
//=========================

//Change an array of users into an array of obcontacts
+ (NSArray *) contactsFromUsers:(NSArray *) users;
//Delete a friend request from the server
+ (void) findAndDeleteFriendRequestFromSYNCH:(PFUser *)user to:(PFUser *)recipient;

@end

