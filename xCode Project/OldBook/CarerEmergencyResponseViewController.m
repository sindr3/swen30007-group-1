//
// CarerEmergencyResponseViewController.m
// OldBook
//
// Created by Mat on 9/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//
// Using map route code adapted from
// http://www.techotopia.com/index.php/Using_MKDirections_to_get_iOS_7_Map_Directions_and_Routes
//

#define METERS_PER_MILE 1609.344

#import "CarerEmergencyResponseViewController.h"
#import "OBLocationManager.h"

@interface CarerEmergencyResponseViewController () <MKMapViewDelegate, CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (weak, nonatomic) CLLocation *currentLocation;

@property (weak, nonatomic) IBOutlet UILabel *timeSinceRequest;

@end

@implementation CarerEmergencyResponseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.mapView.showsUserLocation = YES;
	MKUserLocation *userLocation = self.mapView.userLocation;
	MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.location.coordinate,
																   20000, 20000);
	[self.mapView setRegion:region animated:NO];
	self.mapView.delegate = self;
	
	//Initiate OBCarerUser
	self.carer = [OBCarerUser getCurrentCarerUser];
	
	//Get the emergency request for the user.
	self.request = [self.carer request];
	
	// Create the location manager if this object does not
	// already have one.
	if (nil == self.locationManager)
		self.locationManager = [[CLLocationManager alloc] init];
	
	self.locationManager.delegate = self;
	self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
	
	// Set a movement threshold for new events.
	self.locationManager.distanceFilter = 500;
	
	[self.locationManager startUpdatingLocation];
	
	[self getDirections];
}

- (void)viewWillAppear:(BOOL)animated{
	
	CLLocationCoordinate2D zoomLocation = self.currentLocation.coordinate;
	MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
	[_mapView setRegion:viewRegion animated:YES];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
	self.currentLocation = [locations lastObject];
	
	// Update server for AP
	[OBLocationManager updateServerLocationForLoggedInUserWithLocation:self.currentLocation inBackgroundWithBlock:^(BOOL succeeded, NSError *error){}];
	
	//Update map
	CLLocationCoordinate2D zoomLocation = self.currentLocation.coordinate;
	MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation, 0.5*METERS_PER_MILE, 0.5*METERS_PER_MILE);
	[self.mapView setRegion:viewRegion animated:YES];
	
	return;
}

- (void)getDirections
{
	OBAssistedContact *apContact = self.request.requestingContact;
	
	[self.carer getDirectionsResponseToAP:apContact withCallback:^(MKDirectionsResponse *response, NSError *error) {
		if (error) {
			UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error getting directions"
																 message:[error localizedDescription]
																delegate:self
													   cancelButtonTitle:@"OK"
													   otherButtonTitles:nil];
			[errorAlert show];
			
		} else {
			[self showRoute:response];
		}
	}];
}

-(void)showRoute:(MKDirectionsResponse *)response
{
    for (MKRoute *route in response.routes)
    {
        [self.mapView addOverlay:route.polyline level:MKOverlayLevelAboveRoads];
		
        for (MKRouteStep *step in route.steps)
        {
            NSLog(@"%@", step.instructions);
        }
    }
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id< MKOverlay >)overlay
{
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor blueColor];
    renderer.lineWidth = 5.0;
    return renderer;
}

@end
