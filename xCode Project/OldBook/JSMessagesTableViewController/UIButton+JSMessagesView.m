//
// UIButton+JSMessagesView.m
// MessagesDemo
//
// Created by Jesse Squires on 3/24/13.
// Copyright (c) 2013 Hexed Bits. All rights reserved.
//

#import "UIButton+JSMessagesView.h"

@implementation UIButton (JSMessagesView)

+ (UIButton *)defaultSendButton
{
  UIButton *sendButton = [UIButton buttonWithType:UIButtonTypeSystem];
  sendButton.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin);
  
  UIEdgeInsets insets = UIEdgeInsetsMake(0.0f, 13.0f, 0.0f, 13.0f);
  UIImage *sendBack = [[UIImage imageNamed:@"send"] resizableImageWithCapInsets:insets];
  UIImage *sendBackHighLighted = [[UIImage imageNamed:@"send-highlighted"] resizableImageWithCapInsets:insets];
  [sendButton setBackgroundImage:sendBack forState:UIControlStateNormal];
  [sendButton setBackgroundImage:sendBack forState:UIControlStateDisabled];
  [sendButton setBackgroundImage:sendBackHighLighted forState:UIControlStateHighlighted];
  
  NSString *title = NSLocalizedString(@"Send", nil);
  [sendButton setTitle:title forState:UIControlStateNormal];
  [sendButton setTitle:title forState:UIControlStateHighlighted];
  [sendButton setTitle:title forState:UIControlStateDisabled];
  sendButton.titleLabel.font = [UIFont boldSystemFontOfSize:16.0f];
  
  [sendButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
  
  return sendButton;
}

@end