//
// ContactSearchController.m
// OldBook
//
// Created by Mat on 7/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "ContactSearchController.h"
#import "SVProgressHUD.h"
#import <Parse/Parse.h>
#import "OBSearchQueryManager.h"
#import "OBContact.h"

//Constants for user
#define CELLHEIGHT 60

@interface ContactSearchController () <UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

//Query for results
@property (weak, nonatomic) PFQuery *results;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) OBContact *potentialFriend;

@end

@implementation ContactSearchController

- (void)viewDidLoad
{
  [super viewDidLoad];
  [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
}


- (void) searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
  OBSearchQueryManager *qm =[OBSearchQueryManager getSearchQueryManager];
  [SVProgressHUD showWithStatus:@"Searching for friends"];
  [qm makeSearchQueryWithoutKeyFor:searchBar.text withCompletion:^(NSMutableArray *searchResults, NSError *error) {
    if(!error){
      if(searchResults.count!=0){
        self.searchResults = [NSMutableArray arrayWithArray:searchResults];
        [self refreshView];
        [SVProgressHUD dismiss];
      }
      else {
        [SVProgressHUD showErrorWithStatus:@"No Results Found."];
        [self refreshView];
      }
    } else {
      [SVProgressHUD showErrorWithStatus:@"Could not connect to server"];
      [self refreshView];
    }
    [SVProgressHUD dismiss];
  }];
}



#pragma mark - UITableViewDelegate Methods
-(UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell *cell = [[PFTableViewCell alloc] init];
  if(tableView!=self.tableView){
    cell = nil;
  } else {
    static NSString *identifier = @"SEARCHCELL";
    cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    OBContact *cellUser = [self.searchResults objectAtIndex:indexPath.row];
    cell.textLabel.text = cellUser.firstName;
    cell.detailTextLabel.text = cellUser.lastName;
    cell.imageView.image = cellUser.profilePicture;
  }
  return cell;
}


-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return [self.searchResults count];
}


#pragma mark - UITableViewDataSourceMethods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
  return CELLHEIGHT;
}

-(void) refreshView
{
  [self.tableView reloadData];
  [self.searchDisplayController setActive:NO animated:YES];
}


- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  // We selected so show a popup
  self.potentialFriend = [self.searchResults objectAtIndex:indexPath.row];
  NSMutableString *messageString = [NSMutableString stringWithString:@"Do you want to add "];;
  [messageString appendString: self.potentialFriend.firstName];
  [messageString appendString:@" as a friend?"];
  
  UIAlertView *addFriendAlert = [[UIAlertView alloc] initWithTitle:@"Add Friend"
                               message:messageString
                              delegate:self
                          cancelButtonTitle:@"NO"
                          otherButtonTitles:@"YES",nil
                  ];
  [addFriendAlert show];
  [self refreshView];
}


- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
  if(buttonIndex != alertView.cancelButtonIndex){
    OBUser *user = [OBUser getCurrentUser];
    [user sendFriendRequest:self.potentialFriend];
    [self.searchResults removeObject:self.potentialFriend];
    [self refreshView];
    self.potentialFriend=nil;
  }
}

@end
