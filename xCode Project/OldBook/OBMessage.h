//
// OBMessage.h
// OldBook
//
// Created by Mat on 14/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>



@interface OBMessage : NSObject

// The contents and envelope for our message.
@property (strong, nonatomic) NSData *messageContents;
@property (strong, nonatomic) NSString *messageType;
@property (strong, nonatomic) PFUser *toUser;
@property (strong, nonatomic) PFUser *fromUser;
@property (strong, nonatomic) NSString *objectID;
@property (strong, nonatomic) PFObject *serverObject;
@property (assign, nonatomic) BOOL read;
@property (strong, nonatomic) NSDate *sent;
@property (assign, nonatomic) BOOL outgoing;


//Asynch Creating a message
+ (OBMessage *) createAndSendPictureMessageWith:(UIImage *)image andToUser:(PFUser *)toUser andFromUser:(PFUser *)fromUser withProgress:(void (^)(int percentdone))showProgress;


+ (OBMessage *) createAndSendTextMessageWith:(NSString *)textContents toUser:(PFUser *)toUser fromUser:(PFUser *)fromUser withProgress:(void (^)(int percentdone))showProgress;

//Saving message from server
+ (OBMessage *) saveMessageFromServer:(PFObject *) serverMessage;

+ (OBMessage *) saveMessageFromServerSYNCH:(PFObject *)serverMessage ;

//Deleting message from server
- (void) deleteMessageFromServer;

- (void) deleteMessageFromServerSYNCH;


//Synchronous methods
+ (OBMessage *) createAndSendTextMessageWithSYNCH:(NSString *)textContents toUser:(PFUser *)toUser fromUser:(PFUser *)fromUser withProgress:(void (^)(int percentdone))showProgress;

+ (OBMessage *) createAndSendPictureMessageWithSYNCH:(UIImage *)image andToUser:(PFUser *)toUser andFromUser:(PFUser *)fromUser withProgress:(void (^)(int percentdone))showProgress;


@end
