//
//  OBLocationManager.h
//  OldBook
//
//  Created by James Fitzsimmons on 14/10/2013.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "OBUser.h"

@interface OBLocationManager : NSObject

+ (void)updateServerLocationForLoggedInUserWithLocation:(CLLocation *)location inBackgroundWithBlock:(void (^)(BOOL succeeded, NSError *error))block;
+ (void)updateServerLocationForLoggedInUserInBackgroundWithBlock:(void (^)(BOOL succeeded, NSError *error))block;

+ (void)mapItemForContact:(OBContact *)contact withBlock:(void (^)(MKMapItem *item, NSError *error))completionBlock;
+ (void)clLocationForContact:(OBContact *)contact withBlock:(void (^)(CLLocationCoordinate2D *location, NSError *error))completionBlock;

+ (void)directionsFromContact:(OBContact *)fromContact toContact:(OBContact *)toContact withBlock:(void (^)(MKDirections *directions, NSError *error))completionBlock;
+ (void)directionsResponseFromContact:(OBContact *)fromContact toContact:(OBContact *)toContact withBlock:(void (^)(MKDirectionsResponse *response, NSError *error))completionBlock;
+ (void)etaFromContact:(OBContact *)fromContact toContact:(OBContact *)toContact withBlock:(void (^)(MKETAResponse *directions, NSError *error))completionBlock;

@end
