//
//  ClientDetailsNavViewController.h
//  OldBook
//
//  Created by Oscar Morrison on 14/10/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBAssistedContact.h"
#import "OBContactDetailViewViewController.h"

@interface ClientDetailsNavViewController : UIViewController
@property (strong, nonatomic) OBAssistedContact *client;
@property (strong, nonatomic) OBContactDetailViewViewController* messagingView;
@property (strong, nonatomic) OBContact* chosenFriend;

@end
