//
// FriendRequestViewController.m
// OldBook
//
// Created by Mat on 12/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "FriendRequestViewController.h"
#import <Parse/Parse.h>
#import "SVProgressHUD.h"
#import "OBUser.h"

//Constants for user
#define CELLHEIGHT 60

@interface FriendRequestViewController ()

@property (strong, nonatomic) NSMutableArray *searchResults;
@property (weak, nonatomic) OBContact *potentialFriend;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation FriendRequestViewController

- (id)initWithStyle:(UITableViewStyle)style
{
	self = [super initWithStyle:style];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)viewDidAppear:(BOOL)animated {
	[SVProgressHUD showWithStatus:@"Loading Friend Requests"];
	[self loadRequests];
}

- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{  return [self.searchResults count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *identifier = @"SEARCHCELL";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
	OBContact *cellUser = [self.searchResults objectAtIndex:indexPath.row];
	cell.textLabel.text = cellUser.firstName;
	cell.detailTextLabel.text = cellUser.lastName;
	cell.imageView.image = cellUser.profilePicture;
	return cell;
}

#pragma mark - Table Controls
- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	// We selected so show a popup
	self.potentialFriend = [self.searchResults objectAtIndex:indexPath.row];
	
	//Create message string
	NSMutableString *messageString = [NSMutableString stringWithString:@"Do you want to add "];;
	[messageString appendString:self.potentialFriend.firstName];
	[messageString appendString:@" as a friend?"];
	
	//Create allert
	UIAlertView *addFriendAlert = [[UIAlertView alloc] initWithTitle:@"Add Friend"
															 message:messageString
															delegate:self
												   cancelButtonTitle:@"Accept"
												   otherButtonTitles:@"Decline",@"Later",nil
								   
								   ];
	[addFriendAlert show];
	[self.tableView reloadData];
	
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return CELLHEIGHT;
}

- (void) loadRequests {
	[[OBUser getCurrentUser] retrieveFriendRequestsWithCallback:^(NSArray *friendRequests, NSError *error) {
		if(!error && friendRequests.count!=0){
			self.searchResults = [NSMutableArray arrayWithArray:friendRequests];
			[SVProgressHUD dismiss];
			[self.tableView reloadData];
		}
	}];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if(buttonIndex == alertView.cancelButtonIndex){
		[[OBUser getCurrentUser] acceptFriendRequestFrom:self.potentialFriend];
		[self.searchResults removeObject:self.potentialFriend];
		self.potentialFriend=nil;
	} else if (buttonIndex==1){
		[[OBUser getCurrentUser] denyFriendRequestFrom:self.potentialFriend];
		[self.searchResults removeObject:self.potentialFriend];
	} else {
		//Ignore friend request here
	}
	[self.tableView reloadData];
	
}

@end
