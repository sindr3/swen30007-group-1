//
//  ApHelpViewController.m
//  OldBook
//
//  Created by Sindre on 9/10/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import "ApHelpViewController.h"
#import "OBAssistedUser.h"
#import "OBCarer.h"

@interface ApHelpViewController () <UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate>
@property (strong, nonatomic) OBAssistedUser* user;

// Carer properties
@property (strong, nonatomic) OBCarer* carer;
@property (strong, nonatomic) IBOutlet UIImageView* carerImage;
@property (strong, nonatomic) IBOutlet UILabel* carerName;
@property (strong, nonatomic) IBOutlet UILabel* carerLastSeen;

// Help list
@property (strong, nonatomic) NSMutableArray* availableRequests;
@property (strong, nonatomic) IBOutlet UITableView* requestsTable;
@property (strong, nonatomic) IBOutlet UIView* noRequestsView;

@property (assign, nonatomic) NSInteger selectedRequest;

@end

@implementation ApHelpViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.user = [OBAssistedUser getCurrentUser];
    self.availableRequests = [self.user carerRequestsAvailable];
    
    if ([self.availableRequests count] == 0) {
        [self.requestsTable setHidden:YES];
    } else {
        [self.noRequestsView setHidden:YES];
    }
    [self loadRequestInformation];
}

- (void)viewDidAppear:(BOOL)animated
{
    self.carer = [self.user carer];
    self.carerName.text = [self.carer getFullName];
    self.carerLastSeen.text = [self.carer getLastSeenString];
    self.carerImage.image = [self.carer profilePicture];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)messageCarerTapped
{
    [self.masterNav showMessagingViewForContact:self.carer];
}

#pragma Table View Delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.availableRequests count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    [cell.textLabel setText:[self.availableRequests objectAtIndex:indexPath.row]];
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Available requests";
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedRequest = indexPath.row;
    NSString *str = [[@"Do you need help with " stringByAppendingString:[self.availableRequests objectAtIndex:indexPath.row]] stringByAppendingString:@"?"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Request Help"
                                                    message:str
                                                   delegate:self
                                          cancelButtonTitle:@"Yes"
                                          otherButtonTitles:@"Cancel", nil];
    
    [alert show];
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void) alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex==0){
        [self.user sendCarerRequest:self.selectedRequest to:self.carer];
    }
}

- (void) loadRequestInformation
{
    // Load available requests
    if(self.availableRequests == nil) {
        [self.user getCarerRequestsFor:self.carer withCallback:^(NSArray *carerRequests, NSError *error) {
            [self setAvailableRequests:[NSMutableArray arrayWithArray:carerRequests]];
			// If carer requests aren't available. show appropriate view
            if([self.availableRequests count] == 0){
                [self.noRequestsView setHidden:NO];
                [self.requestsTable setHidden:YES];
            } else {
                [self.noRequestsView setHidden:YES];
                [self.requestsTable setHidden:NO];
            }
        }];
    }
}

@end
