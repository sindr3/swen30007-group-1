//
// ClientListViewController.m
// OldBook
//
// Created by Mat on 8/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "ClientListViewController.h"
#import "OBUser.h"
#import "OBCarerUser.h"
#import "SVProgressHUD.h"
#import "JSBadgeView.h"
#import "CarerClientOverViewViewController.h"


@interface ClientListViewController () <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *listView;
@property (weak, nonatomic) IBOutlet UIView *noFriendsView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *altviewButton;
@property (weak, nonatomic) OBCarerUser *carer;

@property (weak, nonatomic) OBAssistedContact *chosenClient;

@end

@implementation ClientListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  self.carer = [OBCarerUser getCurrentCarerUser];
  [self.listView setHidden:YES];
  [self.noFriendsView setHidden:YES];
  [self.altviewButton setEnabled:NO];
  if(self.carer.clientList.count!=0){
    self.clientList = self.carer.clientList;
    [self.listView setHidden:NO];
    [self.listView reloadData];
  } else {
    [SVProgressHUD showWithStatus:@"Loading Clients"];
    [self loadClients];
  }
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}


- (void) viewDidAppear:(BOOL)animated {
//  if(self.carer.clientList.count!=0){
//    self.clientList = self.carer.clientList;
//    [self.listView setHidden:NO];
//    [self.listView reloadData];
//  }
  
  if(self.clientList==nil){
    [SVProgressHUD showWithStatus:@"Loading Clients"];
    [self loadClients];
  }
}


#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
  return [[self clientList] count];
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
  UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"ClientCell" forIndexPath:indexPath];
	
	OBContact *contact = [[self clientList] objectAtIndex:indexPath.row];
	
  //Set the profile picture
  UIImageView *cellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 4.0f, 73, 73)];
  cellImageView.image = contact.profilePicture ;
  [cell.contentView addSubview:cellImageView];

  //Add the name badge.
  JSBadgeView *nameLabel = [[JSBadgeView alloc] initWithParentView:cell.contentView alignment:JSBadgeViewAlignmentBottomCenter];
  [nameLabel setBadgeBackgroundColor:[UIColor whiteColor]];
  [nameLabel setBackgroundColor:[UIColor colorWithWhite:1.0f alpha:0.2f]];
  [nameLabel setBadgeStrokeColor:[UIColor darkGrayColor]];
  [nameLabel setBadgeTextColor:[UIColor blackColor]];
  [nameLabel setBadgeText:contact.firstName];
  [cell.contentView addSubview:nameLabel];
  
  return cell;
}


#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
  self.chosenClient = [self.clientList objectAtIndex:indexPath.row];
  [self performSegueWithIdentifier:@"toClientDetails" sender:self];
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
  self.chosenClient = nil;
}


#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  return CGSizeMake(75, 85);
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
  return UIEdgeInsetsMake(50, 20, 50, 20);
}

#pragma mark – UITableView Delegate and Datasource


- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  return nil;
}

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return self.clientList.count;
}

- (IBAction)changeView:(id)sender {
  //Need to implement table view delegates here and do all that jazz
}

- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  [SVProgressHUD dismiss];
  if([segue.identifier isEqualToString:@"toClientDetails"]){
    CarerClientOverViewViewController *destination = (CarerClientOverViewViewController *) [segue destinationViewController];
    destination.client = self.chosenClient;
  }
}

- (void) loadClients {
	[self.carer downloadClientListWithBlock:^(NSArray *clientList, NSError *error) {
		if (error) {
      [SVProgressHUD showErrorWithStatus:@"Could Not Load Clients"];
			return;
		}
    
		if (clientList.count == 0) {
			[self.noFriendsView setHidden:NO];
		} else {
			[self setClientList:clientList];
			[self.altviewButton setEnabled:YES];
			[self.listView setHidden:NO];
			[self.listView reloadData];
		}
		
		[SVProgressHUD dismiss];
	}];
}


@end
