//
// MainCarerViewController.m
// OldBook
//
// Created by Mat on 8/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "MainCarerViewController.h"
#import "OBCarerUser.h"
#import "OBCameraViewControllerHelper.h"
#import "OBRecentAcitvity.h"
@interface MainCarerViewController () <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>

@property (strong, nonatomic) UIAlertView *logOutAlert;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;
@property (assign, nonatomic) CGPoint highWelcome;
@property (assign, nonatomic) CGPoint lowName;
@property (weak, nonatomic) IBOutlet UILabel *userFirstName;
@property (weak, nonatomic) IBOutlet UILabel *welcomeBackLabel;
@property (strong, nonatomic) OBCameraViewControllerHelper *cameraHelper;
@property (strong, nonatomic) OBCarerUser *carer;
@property (strong, nonatomic) NSArray *recentActivity;
@property (weak, nonatomic) IBOutlet UITableView *activityTableView;
@property (weak, nonatomic) IBOutlet UIView *loadingView;


@end

@implementation MainCarerViewController

- (void) viewDidLoad {
	[super viewDidLoad];
	
	self.highWelcome = CGPointMake(200, 120);
	self.lowName = CGPointMake(200, 143);
	self.cameraHelper = [[OBCameraViewControllerHelper alloc] initWithImageDest:self.userImage inView:self withNotifier:@"carerProfilePicture"];
	self.carer = [OBCarerUser getCurrentCarerUser];
	[self.activityTableView setHidden:YES];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(updateProfilePic)
												 name:@"carerProfilePicture"
											   object:nil];
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(refreshActivity)
												 name:@"loadedRecentActivity"
											   object:nil];
	
	
}

- (void) updateProfilePic {
	[[OBUser getCurrentUser] setProfilePicture:self.cameraHelper.profilePicture showProgress:YES];
}

- (void) refreshActivity {
	self.recentActivity = self.carer.clientUpdates;
	[self.activityTableView reloadData];
	[self.activityTableView setHidden:NO];
	[self.loadingView setHidden:YES];
}



- (void) viewDidLayoutSubviews {
	[super viewDidLayoutSubviews];
	
	if([self.userFirstName.text length]!=0){
		[self.welcomeBackLabel setCenter:self.highWelcome];
		[self.userFirstName setCenter:self.lowName];
	}
}

- (void) viewDidAppear:(BOOL)animated {
    if(self.recentActivity){
        [self refreshActivity];
	}
    
	if([self.userFirstName.text length]!=0){
		[self.welcomeBackLabel setCenter:self.highWelcome];
		[self.userFirstName setCenter:self.lowName];
	} else {
		
		OBContact *contact = self.carer.userDetails;
		
		[self.userFirstName setAlpha:0];
		[self.userFirstName setText:[contact firstName]];
		[UIView animateWithDuration:0.5 animations:^{
			[self.welcomeBackLabel setCenter:self.highWelcome];
			[self.userFirstName setCenter:self.lowName];
			[self.userFirstName setAlpha:1];
		}];
		
		[UIView animateWithDuration:1 animations:^{
			[self.userImage setAlpha:0];
		}];
		
		[self.userImage setImage:[contact profilePicture]];
		[UIView animateWithDuration:0.2 animations:^{
			[self.userImage setAlpha:1];
		}];
	}
}


-(IBAction)logout:(id)sender{
	self.logOutAlert = [[UIAlertView alloc] initWithTitle:@"Log Out?"
												  message:@"Are you sure you want to log out?"
												 delegate:self
										cancelButtonTitle:@"NO"
										otherButtonTitles:@"YES",
						nil];
	[self.logOutAlert show];
	
	return;
}

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.recentActivity.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ACTIVITYCELL" forIndexPath:indexPath];
	OBRecentAcitvity *activity = [self.recentActivity objectAtIndex:indexPath.row];
	
	//Now fill in the details
	[cell.imageView setImage:activity.contact.profilePicture];
	[cell.textLabel setText:activity.contact.firstName];
	[cell.detailTextLabel setText:activity.type];
	[cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:18]];
	[cell.detailTextLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:12]];
	
	return cell;
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if(buttonIndex != [alertView cancelButtonIndex]){
		[OBCarerUser logOut];
		[self performSegueWithIdentifier:@"BACKTOLOGIN" sender:self];
		[self resignFirstResponder];
		return;
	} else {
		[self resignFirstResponder];
	}
	
}

- (IBAction)changeProfilePic:(id)sender {
	[self.cameraHelper selectPhoto:sender];
}

@end
