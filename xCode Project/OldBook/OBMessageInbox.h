//
// OBMessageInbox.h
// OldBook
//
// Created by Mat on 15/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//
// A wrapper class that holds a message inbox for a given user
// that will be then stored in a dictionary of message inboxes
// provides the function to send messages, create messages,
// retrieve messages etc for a given user.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "OBContact.h"
#import "OBMessage.h"
#import "OBParseQueryManager.h"


@interface OBMessageInbox : NSObject

//The current downloaded data

@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) OBContact *messagesTo;
@property (strong, nonatomic) OBContact *messagesFrom;

//The profile pictures
@property (strong, nonatomic) UIImage *toUserPicture;
@property (strong, nonatomic) UIImage *fromUserPicture;

//The names of the senders
@property (strong, nonatomic) NSString *toUserName;
@property (strong, nonatomic) NSString *fromUserName;

//To check for new messages
@property (strong, nonatomic) NSDate *lastUpdated;

@property (assign, nonatomic) NSInteger unread;

//Function to create message inbox
//+ (OBMessageInbox *) createInboxFor:(OBContact *)contact andContact:(OBContact *)contactTwo withCallback:(void (^)(OBMessageInbox *inbox, NSError *error))completionBlock;
+ (OBMessageInbox *) createInboxFor:(OBContact *)contact andContact:(OBContact *)contactTwo;
- (NSArray *) messagesFromServerObjects:(NSArray *) serverObjects;

//Functions for checking for updates
- (void) checkForNewMessagesWithUpdate:(void (^)(BOOL downloadedNew))hasUpdates;

//Fucntions for deleting messages
- (void) deleteMessage:(OBMessage *)messageToDelete;

//Functions for editing messages
- (void) markAsRead:(OBMessage *)message;
- (void) markAsUnread:(OBMessage *)message;
- (OBMessage *) getMessageAtIndex:(int) index;

//Functions for sending messages
- (void) createAndSendTextMessageWith:(NSString *)textContents withProgress:(void (^)(int percentdone))showProgress;
- (void) createAndSendPictureMessageWith:(UIImage *)imageContents withProgress:(void (^)(int percentdone))showProgress;
@end

@interface OBMessageInboxSYNCH : NSObject

//The current downloaded data

@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) OBContact *messagesTo;
@property (strong, nonatomic) OBContact *messagesFrom;

//The profile pictures
@property (strong, nonatomic) UIImage *toUserPicture;
@property (strong, nonatomic) UIImage *fromUserPicture;

//The names of the senders
@property (strong, nonatomic) NSString *toUserName;
@property (strong, nonatomic) NSString *fromUserName;

//To check for new messages
@property (strong, nonatomic) NSDate *lastUpdated;


//Function to create message inbox
+ (OBMessageInboxSYNCH *) createInboxForSYNCH:(OBContact *)contact andContact:(OBContact *)contactTwo;
- (NSArray *) messagesFromServerObjectsSYNCH:(NSArray *) serverObjects;

//Functions for checking for updates
- (BOOL) checkForNewMessagesWithSYNCH;

//Fucntions for deleting messages
- (void) deleteMessageSYNCH:(OBMessage *)messageToDelete;

//Functions for editing messages
- (void) markAsReadSYNCH:(OBMessage *)message;
- (void) markAsUnreadSYNCH:(OBMessage *)message;
- (OBMessage *) getMessageAtIndex:(int) index;

//Functions for sending messages
- (void) createAndSendTextMessageWith:(NSString *)textContents withProgress:(void (^)(int percentdone))showProgress;
- (void) createAndSendPictureMessageWith:(UIImage *)imageContents withProgress:(void (^)(int percentdone))showProgress;


@end

