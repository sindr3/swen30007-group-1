//
// OBCarerRequest.h
// OldBook
//
// Created by Mat on 19/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OBCarer.h"
#import "OBContact.h"

@interface OBCarerRequest : NSObject

//The user who sent this carer request
@property (strong, nonatomic) OBContact *requestingUser;
//The time it was sent
@property (strong, nonatomic) NSDate *timeSent;
//Whether the request has been acknowledged
@property (assign, nonatomic) BOOL accepted;
//The type of request
@property (strong, nonatomic) NSString *type;
//The carer it was sent to
@property (strong, nonatomic) OBCarer *carerSentTo;
//The object on the server
@property (strong, nonatomic) PFObject *serverObject;

//The method to create an OBCarerRequest;
+ (OBCarerRequest *) createRequestFrom:(OBContact *)contact to:(OBCarer *)carer ofType:(NSString *)type;
//Create the server object
- (PFObject *) createServerObject;
// Create carer request from PFObject
+(OBCarerRequest *)createRequestFromPFCarerRequest:(PFObject *)object;

@end
