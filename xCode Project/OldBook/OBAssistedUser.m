//
// OBAssistedUser.m
// OldBook
//
// Created by Mat on 17/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBAssistedUser.h"
#import "OBLocationManager.h"

@implementation OBAssistedUser

static OBAssistedUser *currentUser = nil;

//Function to set up the assisted user

- (id) init
{
	//Initialise the superclass
	self = [super init];
	
	//Then do all our stuff
	[self performSelectorInBackground:@selector(findSentCarerRequestsOnThread) withObject:nil];
	
	//Get the users carer
	[self retrieveCarerWithCallback:^(OBCarer *carer, NSError *error) {
		if(carer!=nil){
			//Get the requests avaiable for your carer
			[self performSelectorInBackground:@selector(findRequestsAvailableFor) withObject:nil];
		}
	}];
	
	return self;
}

- (void) findSentCarerRequestsOnThread {
	OBParseQueryManager *qm = [OBParseQueryManager getQueryManager];
	[qm findSentCarerRequestsFor:self.currentParseUser withCallback:^(NSArray *available, NSError *error) {
		self.carerRequestsSent = [NSMutableArray arrayWithArray:available];
	}];
}

-(void) findRequestsAvailableFor {
	[self getCarerRequestsFor:self.carer withCallback:^(NSArray *carerRequests, NSError *error) {
		//Cool now we have the requests
		self.carerRequestsAvailable = [NSMutableArray arrayWithArray:carerRequests];
		self.carer.requestsAvailable = self.carerRequestsAvailable;
	}];
}


- (id) initWithMockObjectsMailbox:(OBParseQueryManager *) qm andUser:(PFUser *) user andDetails:(OBContact *) details andFriendlist:(NSMutableArray *) fl andMessages:(NSMutableDictionary *) inbox andCarer:(OBCarer *) carer andCarerRequests:(NSMutableArray *) requests
{
    self = [super initWithMockObjectsMailbox:qm andUser:user andDetails:details andFriendlist:fl andMessages:inbox];
    
    self.carer = carer;
    self.carerRequestsAvailable = requests;
    self.carerRequestsSent = requests;
    
    return self;
}

+ (OBAssistedUser *) getCurrentUser
{
  if(currentUser==nil){
    //Get the superclass and define as this class.
    currentUser = [[OBAssistedUser alloc] init];
  }
  return currentUser;
}

//The functions for getting the carers
- (void) retrieveCarerWithCallback:(void(^)(OBCarer *carer, NSError *error))callback
{
  //Check we don't locally have it
  if(!self.carer){
    //Now run the query
    [self.queryManager findCarerFor:self.currentParseUser withCallback:^(OBCarer *carer, NSError *error) {
      //Cool now we have the list
      if(!error){
        //Then we need to give it back in the call back and cache it locally
        self.carer = carer;
        //Then the callback
          if(callback!=nil){
              callback(carer,error);
          }
      } else {
        //Couldn't find any carers, reply with nil list
          if(callback!=nil){
              callback(nil,error);
          }
      }
    }];
  } else {
      if(callback!=nil){
          callback(self.carer,nil);
      }
  }
}

//Functions for sedning carer requests
- (void) sendCarerRequest:(NSInteger) requestIndex to:(OBCarer *)carer
{
  //Get the request type
  NSString *requestType = [carer.requestsAvailable objectAtIndex:requestIndex];
  
  //Creating the request uploads it to the server so that it can be checked, here we just need to add it locally
  OBCarerRequest *newRequest = [OBCarerRequest createRequestFrom:self.userDetails to:carer ofType:requestType];
  [self.carerRequestsSent addObject:newRequest];
  
}

- (void) getRequestsSentWithCallback:(void(^)(NSArray *requestSent, NSError *error))completionBlock
{
  if(!self.carerRequestsSent)
  {
    //We want to do a query for the requests sent for this particular user.
    [self.queryManager findSentCarerRequestsFor:self.currentParseUser withCallback:^(NSArray *available, NSError *error) {
      if(!error && completionBlock!=nil){
        completionBlock(available,error);
      } else if(completionBlock!=nil){
        completionBlock(nil,error);
      }
    }];
  }
  else if (completionBlock!=nil)
  {
    completionBlock(self.carerRequestsSent,nil);
  }
}

//Check request functions available for a carer
- (void) getCarerRequestsFor:(OBCarer *) carer withCallback:(void (^)(NSArray *carerRequests,NSError *error))completionBlock
{
  if(!self.carerRequestsAvailable)
  {
    //We want to find the requests available for a given carer
    [self.queryManager findCarerRequestsFor:self.carer.objectsUser andUser:self.currentParseUser withCallback:^(NSArray *available, NSError *error) {
      if(!error){
          if(completionBlock!=nil){
              completionBlock(available,error);
          }
      } else {
          if(completionBlock!=nil){
              completionBlock(nil,error);
          }
      }
    }];
  }
  else
  {
      if(completionBlock!=nil){
          completionBlock(self.carerRequestsAvailable,nil);
      }
  }
}

//Functions for emergency requests
- (void) sendEmergencyRequest
{
	//Set the local emergency request, the creation will send the message
	[OBLocationManager updateServerLocationForLoggedInUserInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
		self.emergencyRequest = [OBEmergencyRequest makeEmergencyRequestFor:self];
	}];
}

- (void) cancelEmergencyRequest
{
	[self.emergencyRequest cancelRequest];
	self.emergencyRequest=nil;
}

- (void)etaForCarerInBackgroundWithBlock:(void (^)(MKETAResponse *response, NSError *error))block
{
	[OBLocationManager etaFromContact:[self userDetails] toContact:[self carer] withBlock:^(MKETAResponse *response, NSError *error) {
		block(response, error);
	}];
}

- (void)locationForCarerInBackgroundWithBlock:(void (^)(CLLocationCoordinate2D *location, NSError *))block
{
	[OBLocationManager clLocationForContact:[self carer] withBlock:^(CLLocationCoordinate2D *location, NSError *error) {
		block(location, error);
	}];
}

+ (void) logOut
{
	[super logOut];
	currentUser = nil;
}
@end
