//
// OBContact.h
// OldBook
//
// Created by Mat on 14/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//
// This is a local copy of a contact to be used with the OBUser friend list
// It should NEVER have the ability to add or change the PFUser this represents on the
// server as the local user does not own it. Only the authenticated user should be able
// to change their information.

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>
#import "NSDate+Helper.h"

@interface OBContact : NSObject

@property (strong, nonatomic) UIImage *profilePicture;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;
@property (strong, nonatomic) NSDate *lastUpdated;
@property (strong, nonatomic) PFUser *objectsUser;
@property (strong, nonatomic) NSString *emailAddress;

//Initialising a contact
- (id) initWithUser:(PFUser *)user;

//Custom accessors
- (NSString *) getFullName;

////Checking our information is up to date from the server.
//- (void) checkForUpdate;

//Activity-based update
- (void) newSocialInteractionWithType:(NSString *) type;

//UIStuff
-(NSString *) getLastSeenString;

@end
