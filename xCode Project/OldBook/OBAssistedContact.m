//
// OBAssistedContact.m
// OldBook
//
// Created by Mat on 17/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBAssistedContact.h"
#import "OBParseQueryManager.h"
#import "OBEmergencyContact.h"

@implementation OBAssistedContact

//Factory methods
+ (OBAssistedContact *) getAssistedContactFromUser:(PFUser *) user
{
  //First get the contact basic details from the user information
  OBAssistedContact *newContact = [[OBAssistedContact alloc] initWithUser:user];
  
  //Now we need to download all next of kin and medical contacts
  OBParseQueryManager *qm = [OBParseQueryManager getQueryManager];
  [qm findEmergencyContactsFor:newContact withBlock:^(NSArray *emergencyContacts, NSError *error) {
    [OBAssistedContact sortEmergencyContacts:emergencyContacts withCallback:^(NSArray *nextOfKin, NSArray *medicalContacts) {
      newContact.nextOfKin = [NSMutableArray arrayWithArray:nextOfKin];
      newContact.medicalContacts = [NSMutableArray arrayWithArray:medicalContacts];
    }];
  }];
  
  //Need to find allergies blood pressure etc.
  // TO DO LATER
  return newContact;
}

//Sort the emergency contacts
+ (void) sortEmergencyContacts:(NSArray *)emergencyContacts withCallback:(void (^)(NSArray *nextOfKin, NSArray *medicalContacts))callback
{
  NSMutableArray *nextOfKin = [NSMutableArray new];
  NSMutableArray *medicalContacts = [NSMutableArray new];
  
  //Now go through our contacts and add them to the appropriate array
  for(int i=0; i<emergencyContacts.count; i++){
    OBEmergencyContact *contact = [emergencyContacts objectAtIndex:i];
    if([contact.relation isEqualToString:@"Family"]){
      [nextOfKin addObject:contact];
    } else {
      [medicalContacts addObject:contact];
    }
  }
  
  callback(nextOfKin,medicalContacts);
}
@end
