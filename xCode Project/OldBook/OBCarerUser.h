//
// OBCarerUser.h
// OldBook
//
// Created by Mat on 17/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBUser.h"
#import <MapKit/MapKit.h>
#import "OBEmergencyRequest.h"

@interface OBCarerUser : OBUser
//The client list is stored as an array of OBAssistedContact
@property (strong, nonatomic) NSMutableArray *clientList;
//The client updates are stored as an arry of OBRecentActivity
@property (strong, nonatomic) NSArray *clientUpdates;
//The requests that have been sent to the carer
@property (strong, nonatomic) NSMutableDictionary *clientRequests;
//The current emergency request
@property (strong, nonatomic) OBEmergencyRequest *request;

//Methods for creating an OBCarerUser;
+ (OBCarerUser *)getCurrentCarerUser;
- (void)downloadClientListWithBlock:(void (^)(NSArray *clientList, NSError *error))completionBlock;

//Methods for rdealing with emergency situations
- (void)getDirectionsResponseToAP:(OBAssistedContact *)apContact withCallback:(void (^)(MKDirectionsResponse *response, NSError *error))completionBlock;
- (CLLocation *)getUserLocation:(OBAssistedContact *)contact;

//Methods for dealing with requests for service
- (void)getClientRequestsForContact:(OBContact *)contact withCallback:(void (^)(NSMutableArray *requests, NSError *error))callback;
- (void)getAllClientRequests;

//Methods for interacting specific to ob assited contacts
- (void)checkInForUser:(OBAssistedContact *)contact;

//Functions for checking recent activity
- (void)getRecentClientActivityWithCallback:(void (^)(NSArray *recentActivity, NSError *error))completionCode;

@end
