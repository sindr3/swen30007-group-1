//
// OBCarerRequest.m
// OldBook
//
// Created by Mat on 19/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "OBCarerRequest.h"

@implementation OBCarerRequest

//The method to create an OBCarerRequest;
+ (OBCarerRequest *) createRequestFrom:(OBContact *)contact to:(OBCarer *)carer ofType:(NSString *)type
{
  //The new request as an instantiated object
  OBCarerRequest *newRequest = [OBCarerRequest new];
  
  //Set the local variables
  [newRequest setRequestingUser:contact];
  [newRequest setTimeSent:[NSDate date]];
  [newRequest setType:type];
  [newRequest setCarerSentTo:carer];
  
  //Create the server object
  PFObject *serverObject = [newRequest createServerObject];
  newRequest.serverObject = serverObject;
  
  //Save the server object and return the carer request
  [serverObject saveEventually];
  return newRequest;
}

- (PFObject *) createServerObject
{
  PFObject *serverObject = [PFObject objectWithClassName:@"carerRequest"];
  [serverObject setObject:self.requestingUser.objectsUser forKey:@"assistedPerson"];
  [serverObject setObject:self.carerSentTo.objectsUser forKey:@"carer"];
  [serverObject setObject:@NO forKey:@"accepted"];
  [serverObject setObject:self.type forKey:@"type"];
  
  return serverObject;
}


+(OBCarerRequest *)createRequestFromPFCarerRequest:(PFObject *)object
{
	OBCarerRequest *new = [OBCarerRequest new];
	PFUser *requester = [object objectForKey:@"assistedPerson"];
  PFUser *carer = [object objectForKey:@"carer"];
  
	[new setRequestingUser:[[OBContact alloc] initWithUser:requester]];
	[new setCarerSentTo:[OBCarer getCarerForUser:carer]];
	[new setType:[object objectForKey:@"type"]];
	[new setTimeSent:[object objectForKey:@"createdAt"]];
	[new setAccepted:(BOOL)[object objectForKey:@"accepted"]];
	[new setServerObject:object];
	
	return new;
}

@end
