//
// MainAPViewController.h
// OldBook
//
// Created by Mat on 8/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OBUser.h"
#import "OBAssistedUser.h"


@interface MainAPViewController : UIViewController

@property (weak,nonatomic) OBAssistedUser *currentUser;

-(IBAction)logout:(id)sender;

@end
