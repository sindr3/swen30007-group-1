//
// CheckInViewController.m
// OldBook
//
// Created by Mat on 9/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "CheckInViewController.h"

@interface CheckInViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *clientImage;
@property (weak, nonatomic) IBOutlet UILabel *clientName;
@property (weak, nonatomic) IBOutlet UILabel *clientLastSeen;
@property (weak, nonatomic) IBOutlet UILabel *clientDetailsString;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end

@implementation CheckInViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  [self.clientImage setImage:[self.client profilePicture]];
  [self.clientName setText:[self.client getFullName]];
  [self.clientLastSeen setText:[self.client getLastSeenString]];
  [self.clientDetailsString setText:[[@"Do you want to check in for " stringByAppendingString:self.client.firstName] stringByAppendingString:@"?"]];
}

- (IBAction)didCheckIn:(id)sender {
  [self.client newSocialInteractionWithType:@"Carer Check-in"];
}

@end
