//
//  ApNavViewController.m
//  OldBook
//
//  Created by Sindre on 9/10/13.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import "ApNavViewController.h"
#import "ApHelpViewController.h"
#import "SVProgressHUD.h"


@interface ApNavViewController () <UITableViewDataSource, UITableViewDelegate, UISplitViewControllerDelegate>
@property (strong, nonatomic) OBAssistedUser* user;
@property (strong, nonatomic) OBContact* potentialFriend;

// User properties
@property (strong, nonatomic) IBOutlet UILabel* firstNameField;
@property (strong, nonatomic) IBOutlet UILabel* lastNameField;
@property (strong, nonatomic) IBOutlet UIImageView* userImage;

// Table view related properties
@property (strong, nonatomic) IBOutlet UITableView* tableOfFriends;
@property (strong, nonatomic) NSMutableArray* listOfFriends;
@property (strong, nonatomic) NSMutableArray* listOfFriendRequests;
@property (assign, nonatomic) NSInteger numberOfSections;
@property (assign, nonatomic) NSInteger requestsSection;
@property (assign, nonatomic) NSInteger friendSection;

// Auto Layout constraints
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableBottomConstraint;


@end

@implementation ApNavViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.splitViewController.delegate = self;
    [self observeKeyboard];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(newFriendRequests)
												 name:@"newFriendRequest"
											   object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(newFriends)
												 name:@"newFriends"
											   object:nil];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [self loadUserInfo];
    [self loadTableData];
}

- (void)loadUserInfo
{
    self.user = [OBAssistedUser getCurrentUser];
    OBContact* userDetails = self.user.userDetails;
    [self.firstNameField setText:[userDetails firstName]];
    [self.lastNameField setText:[userDetails lastName]];
    [self.userImage setImage:[userDetails profilePicture]];
}

-(void)loadTableData
{
    if ([[self.user friendList] count] > 0) {
        self.listOfFriends = [self.user friendList];
    }
    [SVProgressHUD showWithStatus:@"Loading Friend List"];
    [self.user retrieveFriendlistWithCallback:^(NSArray *friendList, NSError *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            self.listOfFriends = [friendList mutableCopy];
            [self.tableOfFriends reloadData];
        }
    }];
    
    // This will send a notification newFriendRequest if there is one.
    // Handled by the notification handler
    [self.user userHasFriendRequests];
}

- (void)newFriendRequests
{
    [self.user retrieveNewFriendRequestsWithCallback:^(NSArray *friendRequests, NSError *error) {
        if (!error && [friendRequests count] != 0) {
            self.listOfFriendRequests = [friendRequests mutableCopy];
            [self.tableOfFriends reloadData];
        }
    }];
}

- (void)newFriends
{
    self.listOfFriends = [self.user friendList];
    [self.tableOfFriends reloadData];
}

- (IBAction)addFriendButtonTapped:(id)sender
{
    [self performSegueWithIdentifier:@"showAddFriends" sender:self];
}
- (IBAction)userTappedProfile:(id)sender
{
    [self performSegueWithIdentifier:@"showProfile" sender:self];
    self.messagingView = nil;
}

- (IBAction)emergencyButtonTapped:(id)sender
{
    [self showEmergencyPrompt];
}
- (IBAction)helpButtonTapped:(id)sender
{
    [self performSegueWithIdentifier:@"showHelp" sender:self];
    self.messagingView = nil;
}

// UITableView delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == self.friendSection) {
        return [self.listOfFriends count];
    } else if (section == self.requestsSection) {
        return [self.listOfFriendRequests count];
    } else {
        return 0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    [self updateSectionLogic];
    return self.numberOfSections;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"CONTACTCELL" forIndexPath:indexPath];
    OBContact* friend = nil;
    
    if (indexPath.section == self.friendSection) {
        friend = [self.listOfFriends objectAtIndex:indexPath.row];
    } else if (indexPath.section == self.requestsSection) {
        friend = [self.listOfFriendRequests objectAtIndex:indexPath.row];
    }
    
    // Populate the cell
    [cell.textLabel setText:[friend getFullName]];
    if ([friend profilePicture] == nil) {
        [cell.imageView setImage:[UIImage imageNamed:@"defaultImage.png"]];
    } else {
        [cell.imageView setImage:[friend profilePicture]];
    }
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == self.requestsSection) {
        return @"Friend Requests";
    } else if (section == self.friendSection) {
        return @"Friends";
    } else {
        NSLog(@"titleForHeaderInSection not implemented in SocialNavViewController.m");
        return @""; // Fail safe
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == self.requestsSection) {
        // Selected friend request
        self.potentialFriend = [self.listOfFriendRequests objectAtIndex:indexPath.row];
        [self addFriendPrompt];
        
    } else if (indexPath.section == self.friendSection) {
        [self showMessagingViewForContact:[self.listOfFriends objectAtIndex:indexPath.row]];
    }
}

- (void)updateSectionLogic
{
    if ([self.listOfFriendRequests count] == 0) {
        self.requestsSection = -1;
        if ([self.listOfFriends count] == 0) {
            self.friendSection = -1;
            self.numberOfSections = 0;
        } else {
            self.friendSection = 0;
            self.numberOfSections = 1;
        }
    } else {
        self.requestsSection = 0;
        if ([self.listOfFriends count] == 0) {
            self.friendSection = -1;
            self.numberOfSections = 1;
        } else {
            self.friendSection = 1;
            self.numberOfSections = 2;
        }
    }
}


- (void)showMessagingViewForContact:(OBContact*)contact
{
    if (self.messagingView == nil) {
        // View doesn't exist
        self.chosenFriend = contact;
        [self performSegueWithIdentifier:@"showMessagingView" sender:self];
    } else {
        // View already exists, load other user
        self.messagingView.friendUser = self.chosenFriend;
        self.messagingView.currentUser = self.user;
        [self.messagingView loadMessageInboxForUser];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if([segue.identifier isEqualToString:@"showMessagingView"]){
        if(self.messagingView == nil) {
            self.messagingView = (OBContactDetailViewViewController*) [segue destinationViewController];
            self.messagingView.friendUser = self.chosenFriend;
            self.messagingView.currentUser = self.user;
        }
    } else if ( [segue.identifier isEqualToString:@"showHelp"] ) {
        UINavigationController* nav = [segue destinationViewController];
        ApHelpViewController* vc = (ApHelpViewController*) nav.topViewController;
        vc.masterNav = self;
    }
    
    // The two following lines dismisses a modal view over the detail view controller
    // when the master view is trying to present something else over the detail view.
    // This is a problem when the user is selecting a photo and makes a transition.
    UIViewController* detailView = self.splitViewController.viewControllers[1];
    [detailView dismissViewControllerAnimated:NO completion:NULL];
}

- (void)addFriendPrompt
{
    NSMutableString *messageString = [NSMutableString stringWithString:@"Do you want to add "];;
    [messageString appendString:self.potentialFriend.firstName];
    [messageString appendString:@" as a friend?"];
    
    //Create alert
    UIAlertView *addFriendAlert = [[UIAlertView alloc] initWithTitle:@"Add Friend"
                                                             message:messageString
                                                            delegate:self
                                                   cancelButtonTitle:@"Accept"
                                                   otherButtonTitles:@"Decline",@"Later",nil];
    [addFriendAlert show];
}

- (void)showEmergencyPrompt
{
    NSString* messageString = @"Do you need emergency assistance?";
    
    //Create alert
    UIAlertView *emergencyAlert = [[UIAlertView alloc] initWithTitle:@"Emergency"
                                                             message:messageString
                                                            delegate:self
                                                   cancelButtonTitle:@"YES"
                                                   otherButtonTitles:@"NO",nil];
    [emergencyAlert show];
}


- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView.title isEqualToString:@"Emergency"]) {
        if(buttonIndex == alertView.cancelButtonIndex){
            [self.user sendEmergencyRequest];
            [self performSegueWithIdentifier:@"showEmergency" sender:self];
            self.messagingView = nil;
        } else {
            // Cancelled
        }
    } else if ([alertView.title isEqualToString:@"Add Friend"]) {
        if(buttonIndex == alertView.cancelButtonIndex){
            // Accepted friend request
            [self.user acceptFriendRequestFrom:self.potentialFriend];
            self.listOfFriends = [self.user friendList];
            self.listOfFriendRequests = [self.user friendRequests];
            self.potentialFriend = nil;
            [self.tableOfFriends reloadData];
        } else if (buttonIndex==1){
            // Declined friend request
            [self.user denyFriendRequestFrom:self.potentialFriend];
            self.listOfFriendRequests = [self.user friendRequests];
            [self.tableOfFriends reloadData];
        } else {
            //Ignored friend request
        }
    }
}

#pragma mark Keyboard Dependent Table Resizing
// The three following methods was copied and adapted to this code from
// http://www.think-in-g.net/ghawk/blog/2012/09/practicing-auto-layout-an-example-of-keyboard-sensitive-layout/ 12.October 2013

- (void)observeKeyboard
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

// The callback for frame-changing of keyboard
- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    NSValue *kbFrame = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect keyboardFrame = [kbFrame CGRectValue];
    
    BOOL isPortrait = UIDeviceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation);
    CGFloat height = isPortrait ? keyboardFrame.size.height : keyboardFrame.size.width;
    
    // Because the "space" is actually the difference between the bottom lines of the 2 views,
    // we need to set a negative constant value here.
    self.tableBottomConstraint.constant = height;
    
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    NSDictionary *info = [notification userInfo];
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    self.tableBottomConstraint.constant = 0;
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    }];
}


#pragma mark Orientation Methods
- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

- (BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
    return NO;
}

@end
