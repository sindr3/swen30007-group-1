//
// OBEmergencyContact.h
// OldBook
//
// Created by Mat on 17/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>


@interface OBEmergencyContact : NSObject

//Strings to identify type of relation and contact number
@property (strong, nonatomic) NSNumber *contactNumber;
@property (strong, nonatomic) NSString *relation;
@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *lastName;

// To create an emergency contact from PFUser
+ (OBEmergencyContact *) getEmergencyContactFrom:(PFObject *)eContact;


@end
