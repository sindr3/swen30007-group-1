//
// ClientRequestViewController.m
// OldBook
//
// Created by Mat on 9/08/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import "ClientRequestViewController.h"
#import "OBCarerRequest.h"
#import "NSDate+Helper.h"

@interface ClientRequestViewController () <UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *clientName;
@property (weak, nonatomic) IBOutlet UILabel *clientLastSeen;
@property (weak, nonatomic) IBOutlet UIImageView *clientImage;
@property (weak, nonatomic) IBOutlet UITableView *requestsView;
@property (assign, nonatomic) NSInteger selectedIndex;

@end

@implementation ClientRequestViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
  }
  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
  [self.clientImage setImage:[self.client profilePicture]];
  [self.clientName setText:[self.client getFullName]];
  [self.clientLastSeen setText:[self.client getLastSeenString]];
  self.user = [OBCarerUser getCurrentCarerUser];
  [self.user getClientRequestsForContact:self.client withCallback:^(NSMutableArray *requests, NSError *error) {
    self.requests = [NSArray arrayWithArray:requests];
    [self.requestsView reloadData];
  }];

}

- (int) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return self.requests.count;
}

- (void) tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
  self.selectedIndex = indexPath.row;
  UIAlertView *view = [[UIAlertView alloc] initWithTitle:@"Completed Request"
                         message:@"Do you want to mark this request as completed?"
                         delegate:self
                    cancelButtonTitle:@"YES"
                    otherButtonTitles:@"NO", nil];
  [view show];
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell = [UITableViewCell new];
  OBCarerRequest *request = [self.requests objectAtIndex:indexPath.row];
  
  [cell.textLabel setText:request.type];
  [cell.detailTextLabel setText:[NSDate stringForDisplayFromDate:request.timeSent]];
  
  return cell;
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
  if(buttonIndex==0){
  }
}
@end
