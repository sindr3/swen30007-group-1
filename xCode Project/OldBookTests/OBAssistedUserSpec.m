//
//  OBAssistedUser.m
//  OldBook
//
//  Created by Mat on 7/10/2013.
//  Copyright 2013 Mat. All rights reserved.
//

#import "Kiwi.h"
#import "OBAssistedUser.h"
#import "OBLocationManager.h"

SPEC_BEGIN(OBAssistedUserSpec)

describe(@"An OBAssistedUser", ^{
    
    __block OBParseQueryManager *testQM;
    __block OBAssistedUser *assistedUser;
    __block PFUser *user = nil;
    __block OBContact *contact = nil;
    __block NSMutableDictionary *inbox = nil;
    __block NSMutableArray *friendList = nil;
    __block OBCarer *carer = nil;
    __block NSMutableArray *requestsList = nil;
    
    beforeAll(^{
        testQM = [OBParseQueryManager nullMock];
        assistedUser = [OBAssistedUser nullMock];
        //Initialise OBUser with mock objects
        user = [PFUser nullMock];
        contact = [OBContact nullMock];
        //Create a mocked inbox
        inbox = [NSMutableDictionary new];
        [inbox setObject:[OBMessageInbox nullMock] forKey:@"test"];
        //Mock friendlist
        friendList = [NSMutableArray nullMock];
        //Mock carer
        carer = [OBCarer nullMock];
        //Mock requests lists
        requestsList = [NSMutableArray nullMock];
        
    });
    
    
    context(@"retrieving the carer", ^{
        
        it(@"should return the appropriate carer from query manager if not found already.", ^{
            //Make the test user
            OBAssistedUser *test = [[OBAssistedUser alloc] initWithMockObjectsMailbox:testQM andUser:user andDetails:contact andFriendlist:friendList andMessages:inbox andCarer:nil andCarerRequests:requestsList];
            
            //Set up our expectations
            [test.queryManager stub:@selector(findCarerFor:withCallback:) withArguments:any(),any()];
            KWCaptureSpy *spy = [test.queryManager captureArgument:@selector(findCarerFor:withCallback:) atIndex:1];
            [[test.queryManager should] receive:@selector(findCarerFor:withCallback:)];
            
            //Run the method
            [test retrieveCarerWithCallback:nil];
            
            //Rund the block
            void (^block)(OBCarer *carer, NSError *error) = spy.argument;
            block(carer,nil);
            
            //Check carer is not nil
            [[test.carer should] beNonNil];
        });
        
        it(@"should return the appropriate carer if already found.", ^{
            //Make the test user
            OBAssistedUser *test = [[OBAssistedUser alloc] initWithMockObjectsMailbox:testQM andUser:user andDetails:contact andFriendlist:friendList andMessages:inbox andCarer:carer andCarerRequests:requestsList];
            
            //Ensure things are not being called
            [[test.queryManager shouldNot] receive:@selector(findCarerFor:withCallback:)];

            //Run the method
            [test retrieveCarerWithCallback:nil];
            
            //Check carer is not nil
            [[test.carer should] beNonNil];
        });
        
    });
    
    context(@"getting carer requests", ^{
        
        it(@"should return carer requests from query manager", ^{
            //Make the test user
            OBAssistedUser *test = [[OBAssistedUser alloc] initWithMockObjectsMailbox:testQM andUser:user andDetails:contact andFriendlist:friendList andMessages:inbox andCarer:carer andCarerRequests:nil];

            //Set up our expectations
            [test.queryManager stub:@selector(findCarerRequestsFor:andUser:withCallback:) withArguments:any(),any(),any()];
            [[test.queryManager should] receive:@selector(findCarerRequestsFor:andUser:withCallback:)];
            
            //Run the method
            [test getCarerRequestsFor:carer withCallback:nil];
            
        });
        
        it(@"should return carer requests from local storage", ^{
            //Make the test user
            OBAssistedUser *test = [[OBAssistedUser alloc] initWithMockObjectsMailbox:testQM andUser:user andDetails:contact andFriendlist:friendList andMessages:inbox andCarer:carer andCarerRequests:requestsList];
            
            //Set up our expectations
            [[test.queryManager shouldNot] receive:@selector(findCarerRequestsFor:andUser:withCallback:)];
            
            //Run the method
            [test getCarerRequestsFor:carer withCallback:nil];
        });
        
        it(@"should return sent carer requests from query manager", ^{
            //Make the test user
            OBAssistedUser *test = [[OBAssistedUser alloc] initWithMockObjectsMailbox:testQM andUser:user andDetails:contact andFriendlist:friendList andMessages:inbox andCarer:carer andCarerRequests:nil];
            
            //Set up our expectations
            [test.queryManager stub:@selector(findSentCarerRequestsFor:withCallback:) withArguments:any(),any(),any()];
            [[test.queryManager should] receive:@selector(findSentCarerRequestsFor:withCallback:)];
            
            //Run the method
            [test getRequestsSentWithCallback:nil];
            
        });
        
        it(@"should return sent carer requests from local storage", ^{
            //Make the test user
            OBAssistedUser *test = [[OBAssistedUser alloc] initWithMockObjectsMailbox:testQM andUser:user andDetails:contact andFriendlist:friendList andMessages:inbox andCarer:carer andCarerRequests:requestsList];
            
            //Set up our expectations
            [[test.queryManager shouldNot] receive:@selector(findSentCarerRequestsFor:withCallback:)];
            
            //Run the method
            [test getRequestsSentWithCallback:nil];
        });
        
    });
    
    context(@"dealing with emergency requests", ^{

        it(@"should send emergency requests", ^{
            //Make the test user
            OBAssistedUser *test = [[OBAssistedUser alloc] initWithMockObjectsMailbox:testQM andUser:user andDetails:contact andFriendlist:friendList andMessages:inbox andCarer:carer andCarerRequests:requestsList];
            
            //Set up our expectations
            OBEmergencyRequest *rq = [OBEmergencyRequest nullMock];
            [OBEmergencyRequest stub:@selector(makeEmergencyRequestFor:) andReturn:rq];
            [[OBEmergencyRequest should] receive:@selector(makeEmergencyRequestFor:)];
			
			KWCaptureSpy *spy = [OBLocationManager captureArgument:@selector(updateServerLocationForLoggedInUserInBackgroundWithBlock:) atIndex:0];
			[[OBLocationManager should] receive:@selector(updateServerLocationForLoggedInUserInBackgroundWithBlock:) withArguments:any()];
			[OBLocationManager stub:@selector(updateServerLocationForLoggedInUserInBackgroundWithBlock:)];
            
            //Make the call
            [test sendEmergencyRequest];
            void (^block)(BOOL succeeded, NSError *error) = spy.argument;
			block(YES, nil);
			
            //Check it's not nil
            [[test.emergencyRequest should] beNonNil];
        });
        
        it(@"should cancel emergency requests", ^{
            //Make the test user
            OBAssistedUser *test = [[OBAssistedUser alloc] initWithMockObjectsMailbox:testQM andUser:user andDetails:contact andFriendlist:friendList andMessages:inbox andCarer:carer andCarerRequests:requestsList];
           
            //Set up our expectations
            test.emergencyRequest = [OBEmergencyRequest nullMock];
            [[test.emergencyRequest should] receive:@selector(cancelRequest)];
            
            //Make the call
            [test cancelEmergencyRequest];
            
            //Check it's not nil
            [[test.emergencyRequest should] beNil];
        });
        
    });
  
});

SPEC_END
