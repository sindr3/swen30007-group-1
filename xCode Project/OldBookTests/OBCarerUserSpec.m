//
//  OBCarerUserSpec.m
//  OldBook
//
//  Created by Mat on 7/10/2013.
//  Copyright 2013 Mat. All rights reserved.
//

#import "Kiwi.h"
#import "OBCarerUser.h"

SPEC_BEGIN(OBCarerUserSpec)

describe(@"An OBCarerUser", ^{
    
    __block OBParseQueryManager *testQM;
    __block OBCarerUser *assistedUser;
    __block PFUser *user = nil;
    __block OBContact *contact = nil;
    __block NSMutableDictionary *inbox = nil;
    __block NSMutableArray *friendList = nil;
    __block OBCarer *carer = nil;
    __block NSMutableArray *requestsList = nil;
    
    beforeAll(^{
        testQM = [OBParseQueryManager nullMock];
        assistedUser = [OBCarerUser nullMock];
        //Initialise OBUser with mock objects
        user = [PFUser nullMock];
        contact = [OBContact nullMock];
        //Create a mocked inbox
        inbox = [NSMutableDictionary new];
        [inbox setObject:[OBMessageInbox nullMock] forKey:@"test"];
        //Mock friendlist
        friendList = [NSMutableArray nullMock];
        //Mock carer
        carer = [OBCarer nullMock];
        //Mock requests lists
        requestsList = [NSMutableArray nullMock];
    });
    
    context(@"when trying to initialise", ^{
      
        it(@"should call the appropriate functions to initialise", ^{
            //Make our allocated OBCarerUser;
            OBCarerUser *carer = [OBCarerUser alloc];
            
            // Set up our expectations
            [[carer should] receive:@selector(init)];
            
            //Make the call
            [carer init];
            
            
        });
        
    });
    

});

SPEC_END
