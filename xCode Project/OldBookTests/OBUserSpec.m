//
// OBUserSpec.m
// OldBook
//
// Created by Mat on 1/10/2013.
// Copyright 2013 Mat. All rights reserved.
//

#import "Kiwi.h"
#import "OBUser.h"

SPEC_BEGIN(OBUserSpec)

describe(@"An OBUser", ^{
	
	__block OBUser *testUser = nil;
	__block OBParseQueryManager *queryManager= nil;
	__block PFUser *user = nil;
	__block OBContact *contact = nil;
	__block NSMutableDictionary *inbox = nil;
	__block NSMutableArray *friendList = nil;
	
	
	//Create the mock OBUser which we will test against
	beforeAll(^{
		//Initialise OBUser with mock objects
		queryManager = [OBParseQueryManager nullMock];
		user = [PFUser nullMock];
		contact = [OBContact nullMock];
		//Create a mocked inbox
		inbox = [NSMutableDictionary new];
		[inbox setObject:[OBMessageInbox nullMock] forKey:@"test"];
		//Mock friendlist
		friendList = [NSMutableArray nullMock];
	});
	
	afterAll(^{
		testUser = nil;
		queryManager = nil;
		user = nil;
		contact = nil;
		inbox = nil;
		friendList = nil;
	});
	
	context(@"when logging in and out", ^{
		
		it(@"it should get current user type, returning nil if no user", ^{
			//Tell it to return nil
			[PFUser stub:@selector(currentUser) andReturn:nil];
			//Call the class method and check that it returns @"NA"
			NSString *result = [OBUser getCurrentUserType];
			[[result should] equal:@"NA"];
			
			//Now try with user
			[PFUser stub:@selector(currentUser) andReturn:user];
			//Set up our expectations
			[[OBUser should] receive:@selector(getCurrentUser) withCount:1];
			//Call the class method and check the method gets called on OBUse
			[OBUser getCurrentUserType];
			
		});
		
		it(@"it should allow for log in", ^{
			//Set up the method stubs
			KWCaptureSpy *spy = [PFUser captureArgument:@selector(logInWithUsernameInBackground:password:block:) atIndex:2];
			[PFUser stub:@selector(logInWithUsernameInBackground:password:block:) withArguments:any(),any(),any()];
			[[OBUser should] receive:@selector(registerForPushNotificationsFor:) withArguments:any()];
			
			//Set up our expectations
			[[PFUser should] receive:@selector(logInWithUsernameInBackground:password:block:) withArguments:@"TESTONE",@"TESTPASSWORD",any()];
			
			//Call the argument
			[OBUser logInWithUserNameInBackground:@"TESTONE" andPassword:@"TESTPASSWORD" onCompletion:nil];
			
			//We then test that the completion block fires without an error
			void (^block)(PFUser *user, NSError *error) = spy.argument;
			block(user,nil);
			
		});
		
		it(@"it should allow for log out", ^{
			//Set up expectations
			[[[PFInstallation currentInstallation] should] receive:@selector(setObject:forKey:)];
			[[PFUser should] receive:@selector(logOut)];
			
			//Log out
			[OBUser logOut];
		});
		
	});
	
	context(@"when dealing with friend requests", ^{
		it(@"Should send a friendRequest", ^{
			//Initiate with nil friendlist and then check that the query manager is called
			OBUser *testUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			
			//Set up our expectations
			[[testUser should] receive:@selector(newSocialInteractionWithType:) withCount:1];
			[[PFObject should] receive:@selector(alloc) withCount:1];
			
			//Make the call.
			[testUser sendFriendRequest:contact];
		});
		
		it(@"it should add friends", ^{
			//Set up our expectations
			OBUser *testUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			
			[[PFObject should] receive:@selector(objectWithClassName:) withArguments:@"friendLink"];
			[[contact should] receive:@selector(objectsUser)];
			[[testUser should] receive:@selector(currentParseUser)];
			
			//Call the method
			[testUser addFriend:contact];
		});
		
		it(@"it should remove friends", ^{
			//Create our test user
			OBUser *testUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			//Set up our expecations
			KWCaptureSpy *spy = [testUser.queryManager captureArgument:@selector(findFriendLink:andUser:withCompletion:) atIndex:2];
			[[testUser.queryManager should] receive:@selector(findFriendLink:andUser:withCompletion:)];
			
			//Call the method
			[testUser removeFriend:contact];
			
			//Capture the block
			void (^block)(NSArray *friendLinks, NSError *error) = spy.argument;
			[[block should] beNonNil];
			
		});
		
		it(@"it should accept friend requests", ^{
			//Create our test user
			OBUser *testUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			
			//Set up expectations
			[[OBParseQueryManager should] receive:@selector(findAndDeleteFriendRequestFrom:to:) withArguments:contact.objectsUser,any()];
			[[testUser should] receive:@selector(addFriend:) withArguments:contact];
			[[testUser should] receive:@selector(newSocialInteractionWithType:) withCount:1];
			
			//Make the call
			[testUser acceptFriendRequestFrom:contact];
		});
		
		it(@"it should deny friend requests", ^{
			//Create our test user
			OBUser *testUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			
			//Set up expectations
			[[OBParseQueryManager should] receive:@selector(findAndDeleteFriendRequestFrom:to:) withArguments:contact.objectsUser,any()];
			[[testUser shouldNot] receive:@selector(addFriend:) withArguments:contact];
			[[testUser should] receive:@selector(newSocialInteractionWithType:) withCount:1];
			
			//Make the call
			[testUser denyFriendRequestFrom:contact];
		});
		
		it(@"it should check if a user has friend requests", ^{
			//Create our test user
			OBUser *testUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			
			//Set up our expectations
			[[PFQuery should] receive:@selector(queryWithClassName:) withArguments:@"friendRequest"];
			
			//Make the call
			BOOL response = [testUser userHasFriendRequests];
			[[theValue(response) should] equal:theValue(NO)];
			
		});
		
		it(@"it should retrieve friend requests", ^{
			//Create our test user
			OBUser *testUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			testUser.friendRequests = nil;
			//Set up our expectations
			[[testUser.queryManager should] receive:@selector(retrieveFriendRequestsWithCallback:) withArguments:any()];
			
			//Make the call
			[testUser retrieveFriendRequestsWithCallback:nil];
		});
		
	});
	
	
	context(@"when accessing friend details", ^{
		
		it(@"Should return friendlists", ^{
			//Initiate with nil friendlist and then check that the query manager is called
			OBUser *testUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
            
			//Outline what should happen
			[[testUser.queryManager should] receive:@selector(queryForFriendlistForUser:onCompletion:) withCount:1];
			//Make the call
			[testUser retrieveFriendlistWithCallback:nil];
		});
		
	});
	
	context(@"when dealing with messages", ^{
		
		it(@"it should send text messages if mailbox exists", ^{
			//Initiate with nil friendlist and then check that the query manager is called
			OBUser *testUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			
			//Set our expectations
			OBMessageInbox *inbox = [OBMessageInbox nullMock];
			[testUser.messageInboxes stub:@selector(objectForKey:) andReturn:inbox];
			[[inbox should] receive:@selector(createAndSendTextMessageWith:withProgress:) withArguments:@"TESTMESSAGE",nil];
			[[testUser should] receive:@selector(newSocialInteractionWithType:) withCount:1];
			
			//Make the call
			[testUser createAndSendTextMessageWith:@"TESTMESSAGE" andRecipient:contact withProgress:nil];
		});
		
		it(@"it should send text messages if mailbox doesn't exist", ^{
			//Initiate with nil friendlist and then check that the query manager is called
			OBUser *testUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			
			//Set our expectations
			OBMessageInbox *inbox = [OBMessageInbox nullMock];
			[testUser.messageInboxes stub:@selector(objectForKey:) andReturn:nil];
			[[inbox should] receive:@selector(createAndSendTextMessageWith:withProgress:) withCount:1 arguments:@"TESTMESSAGE",nil];
			[[testUser.messageInboxes should] receive:@selector(setValue:forKey:) withArguments:inbox,contact.objectsUser.objectId];
			[[testUser should] receive:@selector(newSocialInteractionWithType:) withCount:1];
			[OBMessageInbox stub:@selector(createInboxFor:andContact:) andReturn:inbox];
			
			//Make the call
			[testUser createAndSendTextMessageWith:@"TESTMESSAGE" andRecipient:contact withProgress:nil];
			
			//Now run the block
		});
		
		
		it(@"it should send picture messages if mailbox exists", ^{
			//Initiate with nil friendlist and then check that the query manager is called
			OBUser *testUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			
			//Set our expectations
			OBMessageInbox *inbox = [OBMessageInbox nullMock];
			[testUser.messageInboxes stub:@selector(objectForKey:) andReturn:inbox];
			[[inbox should] receive:@selector(createAndSendPictureMessageWith:withProgress:) withArguments:nil,nil];
			
			//Make the call
			[testUser createAndSendPictureMessageWith:nil andRecipient:contact withProgress:nil];
		});
		
		
		it(@"it should send picture messages if mailbox doesn't exist", ^{
			//Initiate with nil friendlist and then check that the query manager is called
			OBUser *testUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			
			//Set our expectations
			OBMessageInbox *inbox = [OBMessageInbox nullMock];
			[testUser.messageInboxes stub:@selector(objectForKey:) andReturn:nil];
			[[inbox should] receive:@selector(createAndSendPictureMessageWith:withProgress:) withArguments:nil,nil];
			[[testUser.messageInboxes should] receive:@selector(setValue:forKey:) withArguments:inbox,contact.objectsUser.objectId];
			[[testUser should] receive:@selector(newSocialInteractionWithType:) withCount:1];
			[OBMessageInbox stub:@selector(createInboxFor:andContact:) andReturn:inbox];
			
			//Make the call
			[testUser createAndSendPictureMessageWith:nil andRecipient:contact withProgress:nil];
						
		});
		
		
		it(@"it should mark messages as read", ^{
			//Initiate with nil friendlist and then check that the query manager is called
			OBUser *testUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			//Make a mock message
			OBMessage *mockMessage = [OBMessage nullMock];
			//Set our expectations
			OBMessageInbox *inbox = [OBMessageInbox nullMock];
			[testUser.messageInboxes stub:@selector(objectForKey:) andReturn:inbox];
			[[inbox should] receive:@selector(markAsRead:) withArguments:mockMessage];
			[[testUser should] receive:@selector(newSocialInteractionWithType:) withCount:1];
			
			//Make the call
			[testUser markMessageAsRead:mockMessage];
			
		});
		
		pending_(@"it delete messages", ^{
			//Initiate with nil friendlist and then check that the query manager is called
			OBUser *testUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			//Make a mock message
			OBMessage *mockMessage = [OBMessage nullMock];
			//Set our expectations
			OBMessageInbox *inbox = [OBMessageInbox nullMock];
			[testUser.messageInboxes stub:@selector(objectForKey:) andReturn:inbox];
			[[inbox should] receive:@selector(deleteMessage:) withArguments:mockMessage];
			[[testUser should] receive:@selector(newSocialInteractionWithType:) withCount:1];
			
			//Make the call
			[testUser deleteMessage:mockMessage];
			
		});
		
		it(@"it should return message inboxes for contacts if mailbox exists", ^{
			//Initiate with nil friendlist and then check that the query manager is called
			OBUser *testUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			
			//Set our expectations
			OBMessageInbox *inbox = [OBMessageInbox nullMock];
			[testUser.messageInboxes stub:@selector(objectForKey:) andReturn:inbox];
			[[testUser.messageInboxes should] receive:@selector(objectForKey:) withArguments:contact.objectsUser.objectId];
			
			//Make the call
			[testUser getMessageinboxForContact:contact withCallback:nil];
		});
		
		it(@"it should return message inboxes for contacts if mailbox doesn't exist", ^{
			//Initiate with nil friendlist and then check that the query manager is called
			OBUser *testUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			
			//Set our expectations
			OBMessageInbox *inbox = [OBMessageInbox nullMock];
			[testUser.messageInboxes stub:@selector(objectForKey:) andReturn:nil];
			[[testUser.messageInboxes should] receive:@selector(setValue:forKey:) withArguments:inbox,contact.objectsUser.objectId];
			[OBMessageInbox stub:@selector(createInboxFor:andContact:) andReturn:inbox];
			
			//Make the call
			[testUser getMessageinboxForContact:contact withCallback:nil];
					
		});
		
		it(@"it should check for new messages", ^{
			//Initiate with nil friendlist and then check that the query manager is called
			OBUser *testUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			
			//Set up our expectations
			OBMessageInbox *inbox = [OBMessageInbox nullMock];
			[testUser.messageInboxes stub:@selector(objectForKey:) andReturn:inbox];
			[[inbox should] receive:@selector(checkForNewMessagesWithUpdate:)];
			[[testUser should] receive:@selector(newSocialInteractionWithType:) withCount:1];
			
			//Make our call
			[testUser checkForNewMessagesFrom:contact withUpdate:nil];
			
		});
		
	});
	
	context(@"when signing up new user", ^{
		
		it(@"it should sign up new users in background", ^{
			//Make a fake user
			OBUser *fakeUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			//Set up our expectation
			[[fakeUser.currentParseUser should] receive:@selector(signUpInBackgroundWithBlock:) withArguments:any()];
			
			//Call the method
			[fakeUser signUpUserInBackgroundWithBlock:nil];
		});
		
		it(@"it should create new user with profile picture", ^{
			//Make a fake user
			OBUser *fakeUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			
			//Set up our expectations
			[[fakeUser should] receive:@selector(signUpUserInBackgroundWithBlock:)];
			KWCaptureSpy *spy = [fakeUser captureArgument:@selector(signUpUserInBackgroundWithBlock:) atIndex:0];
			[[fakeUser should] receive:@selector(setProfilePicture:showProgress:)];
			[OBUser stub:@selector(registerForPushNotificationsFor:)];
			[[OBUser should] receive:@selector(registerForPushNotificationsFor:)];
			//Make our call
			[fakeUser createNewUserWithProfilePicture:[UIImage mock] andCompletion:nil];
			
			void (^block)(BOOL succeeded, NSError *error) = spy.argument;
			//Run the block and check that things were called.
			block(YES, nil);
			
		});
		
		it(@"it should create a temporary user with details", ^{
			//Stub some methods so we don't crash with fake values
			[PFUser stub:@selector(user) andReturn:[PFUser nullMock]];
			
			//Create our fake user
			OBUser *fakedUser = [OBUser createTempUserFor:@"USERTEST" andPassword:@"TESTPASS" andEmail:@"EMAIL" andBirthday:[NSDate date] andFirstName:@"NAME" andLastName:@"LASTNAME"];
			
			//Now check the values is not nil
			[[fakedUser.currentParseUser shouldNot] beNil];
		});
		
	});
	
	context(@"when manipulating details", ^{
		
		it(@"it should update profile picture", ^{
			//Make a fake user
			OBUser *fakeUser = [[OBUser alloc] initWithMockObjectsMailbox:queryManager andUser:user andDetails:contact andFriendlist:nil andMessages:inbox];
			
			//Set up our stubs for control flow
			PFFile *fakePic = [PFFile nullMock];
			[fakePic stub:@selector(saveInBackgroundWithBlock:progressBlock:)];
			KWCaptureSpy *spy = [fakePic captureArgument:@selector(saveInBackgroundWithBlock:progressBlock:) atIndex:0];
			[PFFile stub:@selector(fileWithData:) andReturn:fakePic];
			[fakeUser.currentParseUser stub:@selector(saveEventually)];
			
			//Set up our expectations
			[[fakeUser.currentParseUser should] receive:@selector(setObject:forKey:) withArguments:fakePic,@"profilePicture"];
			[[fakeUser.currentParseUser should] receive:@selector(saveEventually)];
			
			[[fakeUser should] receive:@selector(newSocialInteractionWithType:)];
			
			//Call the setting
			[fakeUser setProfilePicture:[UIImage nullMock] showProgress:NO];
			
			void (^block)(BOOL succeeded, NSError *error) = spy.argument;
			block(YES,nil);
		});
		
	});
	
	
	
});

SPEC_END
