//
//  OBLocationManagerTest.m
//  OldBook
//
//  Created by James Fitzsimmons on 18/10/2013.
//  Copyright (c) 2013 Mat. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "Kiwi.h"
#import "OBLocationManager.h"

SPEC_BEGIN(OBLocationManagerSpec)

describe(@"The OBLocationManager", ^{
	__block PFUser *pfUser = nil;
	__block PFGeoPoint *pfGeoPoint = nil;
	__block OBContact *contact = nil;
	__block CLLocationCoordinate2D coord;
	__block MKPlacemark *placemark = nil;
	__block MKMapItem *item = nil;
	
	beforeAll(^{
		pfUser = [PFUser nullMock];
		pfGeoPoint = [PFGeoPoint nullMock];
		contact = [OBContact nullMock];
		coord = CLLocationCoordinate2DMake(1, 1);
		placemark = [MKPlacemark nullMock];
		item = [MKMapItem nullMock];
	});
	
	afterAll(^{
		pfUser = nil;
		pfGeoPoint = nil;
		contact = nil;
		placemark = nil;
		item = nil;
	});
	
	context(@"when dealing with the current user", ^{
		it(@"should save the current location on the server", ^{
			// Stub methods
			KWCaptureSpy *geospy = [PFGeoPoint captureArgument:@selector(geoPointForCurrentLocationInBackground:) atIndex:0];
			[PFGeoPoint stub:@selector(geoPointForCurrentLocationInBackground:) withArguments:any()];
			
			[PFUser stub:@selector(currentUser) andReturn:pfUser];
			[[pfUser should] receive:@selector(saveInBackgroundWithBlock:) withArguments:any()];
			[[pfUser should] receive:@selector(setObject:forKey:) withArguments:any(), @"location"];
			
			// Call
			[OBLocationManager updateServerLocationForLoggedInUserInBackgroundWithBlock:nil];
			
			// Run block
			void (^geoblock)(PFGeoPoint *point, NSError *error) = geospy.argument;
			geoblock(pfGeoPoint, nil);
		});
		
		it(@"should save a specific location on the server", ^{
			// Stub methods
			[PFGeoPoint stub:@selector(geoPointWithLocation:) andReturn:pfGeoPoint];
			[PFUser stub:@selector(currentUser) andReturn:pfUser];
			
			// Expectations
			[[pfUser should] receive:@selector(saveInBackgroundWithBlock:) withArguments:any()];
			[[pfUser should] receive:@selector(setObject:forKey:) withArguments:pfGeoPoint, @"location"];
			
			// Call
			[OBLocationManager updateServerLocationForLoggedInUserWithLocation:[CLLocation nullMock]
														 inBackgroundWithBlock:nil];
		});
	});
	
	context(@"when dealing with contacts", ^{
		it(@"should create a MapItem", ^{
			// Stub
			KWCaptureSpy *spy = [OBLocationManager captureArgument:@selector(clLocationForContact:withBlock:) atIndex:1];
			[OBLocationManager stub:@selector(clLocationForContact:withBlock:) withArguments:contact, any()];
			
			// Expectations
			[MKPlacemark stub:@selector(alloc) andReturn:placemark];
			[[placemark should] receive:@selector(initWithCoordinate:addressDictionary:) withArguments:any(), any()];
			[placemark stub:@selector(initWithCoordinate:addressDictionary:) andReturn:placemark];
			[MKMapItem stub:@selector(alloc) andReturn:item];
			[[item should] receive:@selector(initWithPlacemark:) withArguments:any()];
			
			// Call
			[OBLocationManager mapItemForContact:contact withBlock:nil];
			
			// Check
			void (^block)(CLLocationCoordinate2D *location, NSError *error) = spy.argument;
			block(&coord, nil);
		});
	});
});

SPEC_END