//
// OBMessageInboxTest.m
// OldBook
//
// Created by Mat on 24/09/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "OBContact.h"
#import "OBMessageInbox.h"
#import "OBUser.h"
#import "SVProgressHUD.h"

@interface OBMessageInboxTest : XCTestCase

@end

@implementation OBMessageInboxTest

- (void)setUp
{
  [super setUp];
  [OBUser logOut];
  // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
  // Put teardown code here; it will be run once, after the last test case.
  [super tearDown];
}

- (void)testCreateInbox
{
  //Create and inbox for testuser
  //Get the user from the server.
  PFQuery *findTest = [PFUser query];
  PFUser *testUser =(PFUser *) [findTest getObjectWithId:@"zMK772Rgro"];
  OBContact *contact = [[OBContact alloc] initWithUser:testUser];
  
  //Get the user from the server.
  PFUser *testUser2 = (PFUser *) [findTest getObjectWithId:@"7mIOhGRuPI"];
  OBContact *contact2 = [[OBContact alloc] initWithUser:testUser2];
  
  OBMessageInboxSYNCH *inbox = [OBMessageInboxSYNCH createInboxForSYNCH:contact andContact:contact2];
  
  XCTAssert(inbox.messages.count==1, @"Message inbox count is incorrect");
  XCTAssert([inbox.toUserName isEqualToString:(contact.firstName)], @"To user is incorrect");
  XCTAssert([inbox.fromUserName isEqualToString:(contact2.firstName)], @"To user is incorrect");
  XCTAssert([inbox.messagesTo.objectsUser isEqual:(contact.objectsUser)], @"user is incorrect");
}

- (void) testCreateAndSendTextMessage {
  //Create and inbox for testuser
  //Get the user from the server.
  PFQuery *findTest = [PFUser query];
  PFUser *testUser =(PFUser *) [findTest getObjectWithId:@"zMK772Rgro"];
  OBContact *contact = [[OBContact alloc] initWithUser:testUser];
  
  //Get the user from the server.
  PFUser *testUser2 = (PFUser *) [findTest getObjectWithId:@"7mIOhGRuPI"];
  OBContact *contact2 = [[OBContact alloc] initWithUser:testUser2];
  
  OBMessageInboxSYNCH *inbox = [OBMessageInboxSYNCH createInboxForSYNCH:contact andContact:contact2];
  
  //Send the message
  [inbox createAndSendTextMessageWith:@"Test message wooho!" withProgress:nil];
  
  //Get the latest message
  OBMessage *msg = [inbox.messages firstObject];
  NSData *localData = msg.messageContents;
  NSString *localString = [[NSString alloc] initWithData:localData encoding:NSStringEncodingConversionAllowLossy];
  XCTAssert([localString isEqualToString:@"Test message wooho!"], @"Files aren't equal");

  //Delete message
  [inbox deleteMessageSYNCH:msg];
  XCTAssert(inbox.messages.count==1, @"Message inbox count is incorrect");
}

- (void) testCreateAndPictureTextMessage {
  //Create and inbox for testuser
  //Get the user from the server.
  PFQuery *findTest = [PFUser query];
  PFUser *testUser =(PFUser *) [findTest getObjectWithId:@"zMK772Rgro"];
  OBContact *contact = [[OBContact alloc] initWithUser:testUser];
  
  //Get the user from the server.
  PFUser *testUser2 = (PFUser *) [findTest getObjectWithId:@"7mIOhGRuPI"];
  OBContact *contact2 = [[OBContact alloc] initWithUser:testUser2];
  
  OBMessageInboxSYNCH *inbox = [OBMessageInboxSYNCH createInboxForSYNCH:contact andContact:contact2];
  
  //Send the message
  [inbox createAndSendPictureMessageWith:[UIImage imageNamed:@"defaultimage"] withProgress:nil];
  
  //Get the latest message
  OBMessage *msg = [inbox.messages firstObject];
  NSData *localData = msg.messageContents;
  NSData *data = UIImageJPEGRepresentation([UIImage imageNamed:@"defaultimage"],1.0f);
  XCTAssert([localData isEqual:data], @"Files aren't equal");
  
  //Delete message
  [inbox deleteMessageSYNCH:msg];
  XCTAssert(inbox.messages.count==1, @"Message inbox count is incorrect");
}

- (void) testReadAndUnread {
  //Create and inbox for testuser
  //Get the user from the server.
  PFQuery *findTest = [PFUser query];
  PFUser *testUser =(PFUser *) [findTest getObjectWithId:@"zMK772Rgro"];
  OBContact *contact = [[OBContact alloc] initWithUser:testUser];
  
  //Get the user from the server.
  PFUser *testUser2 = (PFUser *) [findTest getObjectWithId:@"7mIOhGRuPI"];
  OBContact *contact2 = [[OBContact alloc] initWithUser:testUser2];
  
  OBMessageInboxSYNCH *inbox = [OBMessageInboxSYNCH createInboxForSYNCH:contact andContact:contact2];
  
  //Get the message
  OBMessage *msg = [inbox getMessageAtIndex:0];
  
  //Check its current read status
  BOOL status = msg.read;
  BOOL localStatus = msg.read;
  XCTAssert(!status, @"Message status was incorrect to start");
  
  //Mark as read
  [inbox markAsReadSYNCH:msg];
  [msg.serverObject fetch];
  status = [[msg.serverObject objectForKey:@"hasBeenRead"] boolValue];
  localStatus = msg.read;
  XCTAssert(status, @"Message status was incorrect after mark read");

  //Mark as unread
  [inbox markAsUnreadSYNCH:msg];
  status = [[msg.serverObject objectForKey:@"hasBeenRead"] boolValue];
  localStatus = msg.read;
  XCTAssert(!status, @"Message status was incorrect after mark unread");

  
}

@end
