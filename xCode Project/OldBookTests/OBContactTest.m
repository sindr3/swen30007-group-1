//
// OBContactTest.m
// OldBook
//
// Created by Mat on 22/09/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <Parse/Parse.h>
#import "OBContact.h"

@interface OBContactTest : XCTestCase

@property (strong,nonatomic) OBContact *testContact;

@end

@implementation OBContactTest 
- (void) testContactFromUser {
  //Get the user from the server.
  PFQuery *findTest = [PFUser query];
  PFUser *testUser = (PFUser *)[findTest getObjectWithId:@"zMK772Rgro"];
  OBContact *contact = [[OBContact alloc] initWithUser:testUser];
  
  //Test against expected values on server
  XCTAssert([contact.firstName isEqualToString:@"James"], @"First Name inCorrect");
  XCTAssert([contact.lastName isEqualToString:@"Fitzsimmons"], @"Last name incorrect");
  XCTAssert([contact.emailAddress isEqualToString: @"fjr@student.unimelb.edu.au"], @"Email adress INcorrect");
  XCTAssert([contact.objectsUser.objectId isEqualToString:@"zMK772Rgro"], @"Incorrect user id");
}

- (void) testGetFullName {
  //Get the user from the server.
  PFQuery *findTest = [PFUser query];
  PFUser *testUser = (PFUser *)[findTest getObjectWithId:@"zMK772Rgro"];
  OBContact *contact = [[OBContact alloc] initWithUser:testUser];
  //Test getting full name against expected erver values
  XCTAssert([[contact getFullName] isEqualToString:@"James Fitzsimmons"], @"Full name is incorrect");
}

@end
