//
// OBMessageTest.m
// OldBook
//
// Created by Mat on 24/09/2013.
// Copyright (c) 2013 Mat. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <Parse/Parse.h>
#import "OBMessage.h"

@interface OBMessageTest : XCTestCase

@end

@implementation OBMessageTest

- (void)setUp
{
  [super setUp];
  // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
  // Put teardown code here; it will be run once, after the last test case.
  [super tearDown];
}

- (void)testSendTextMessage
{
  //Get two users
  PFQuery *findTest = [PFUser query];
  PFUser *testUser = (PFUser *)[findTest getObjectWithId:@"zMK772Rgro"];
  PFUser *testUser2 = (PFUser *)[findTest getObjectWithId:@"7mIOhGRuPI"];

  //Send the message
  OBMessage *message = [OBMessage createAndSendTextMessageWithSYNCH:@"Test message" toUser:testUser fromUser:testUser2 withProgress:^(int percentdone) {
  }];
  
  //Check the message is on the server
  PFQuery *query = [PFQuery queryWithClassName:@"Message"];
  PFObject *serverObject = [query getObjectWithId:message.serverObject.objectId];
  PFUser *onServer = [serverObject objectForKey:@"fromUser"];
  [onServer fetch];
  XCTAssert([message.fromUser.username isEqual:onServer.username], @"User was incorrect");
  
  //Get the file contents and check equality
  PFFile *server = [serverObject objectForKey:@"messageContents"];
  NSData *localData = message.messageContents;
  NSData *serverData = [server getData];
  NSString *localString = [[NSString alloc] initWithData:localData encoding:NSStringEncodingConversionAllowLossy];
  NSString *severString = [[NSString alloc] initWithData:serverData encoding:NSStringEncodingConversionAllowLossy];

  //Now delete the message
  [message deleteMessageFromServer];
  
  XCTAssert([severString isEqualToString:localString], @"Files aren't equal");
}


- (void)testSendPicMessage
{
  //Get two users
  PFQuery *findTest = [PFUser query];
  PFUser *testUser = (PFUser *)[findTest getObjectWithId:@"zMK772Rgro"];
  PFUser *testUser2 = (PFUser *) [findTest getObjectWithId:@"7mIOhGRuPI"];
  
  //Send the message
  OBMessage *message = [OBMessage createAndSendPictureMessageWithSYNCH:[UIImage imageNamed:@"defaultimage"] andToUser:testUser andFromUser:testUser2 withProgress:^(int percentdone) {
  }];
  
  //Check the message is on the server
  PFQuery *query = [PFQuery queryWithClassName:@"Message"];
  PFObject *serverObject = [query getObjectWithId:message.serverObject.objectId];
  PFUser *onServer = [serverObject objectForKey:@"fromUser"];
  [onServer fetch];
  XCTAssert([message.fromUser.username isEqual:onServer.username], @"User was incorrect");
  
  //Get the file contents and check equality
  PFFile *server = [serverObject objectForKey:@"messageContents"];
  NSData *localData = message.messageContents;
  NSData *serverData = [server getData];  
  XCTAssert([serverData isEqual :localData], @"Files aren't equal");
  
  //Now delete the server object
  [message deleteMessageFromServer];

}

- (void)testDownload
{
  //Get the server object
  PFQuery *query = [PFQuery queryWithClassName:@"Message"];
  PFObject *serverObject = [query getObjectWithId:@"T4ozUONsrO"];
  [serverObject fetch];
  
  //Now get the message
  OBMessage *message = [OBMessage saveMessageFromServerSYNCH:serverObject];
  
  
  //Now test the data is what it should be.
  //Get the file contents and check equality
  NSData *localData = message.messageContents;
  NSString *localString = [[NSString alloc] initWithData:localData encoding:NSStringEncodingConversionAllowLossy];
  XCTAssert([localString isEqualToString:@"Test message wooho!"], @"Files aren't equal");
  
  //Check the usernames
  XCTAssert([message.toUser.username isEqualToString:@"testuser"], @"To username is wrong");
  XCTAssert([message.fromUser.username isEqualToString:@"testsocial"], @"To username is wrong");
}



@end
